<?php

    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');

    $id = $_SESSION['id'];

    //SELECT QUERIES FOR 5 TABS 

    $sql1= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish,
    `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone, `customer`.email
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='pending'" ;

    $sql2= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription, `customer_order`.price,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish, `customer_order`.paymentType, `customer_order`.paymentStatus,
     `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone, `customer`.email
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='upcoming'" ;

    $sql3= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription, `customer_order`.price,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish,`customer_order`.paymentType, `customer_order`.paymentStatus,
     `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone, `customer`.email
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='ongoing'" ;

    $sql4= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription, `customer_order`.price,
    `customer_order`.orderStartDate, `customer_order`.orderEndDate, `customer_order`.paymentType, `customer_order`.paymentStatus,
    `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `rating`.freelancerRating, `rating`.customerRating,`rating`.customerFeedback, `customer`.phone, `customer`.email
    FROM ((customer_order
    INNER JOIN customer ON `customer_order`.customerID=`customer`.customerID)
    INNER JOIN rating ON `customer_order`.orderID = `rating`.orderID)
    WHERE `customer_order`.freelancerID=84 AND `customer_order`.orderStatus='finished'" ;

    $sql5= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderReceivalDate, `customer_order`.customerID, `customer_order`.rejectedReason, `customer_order`.rejectedBy,
    `customer`.fName, `customer`.lName, `customer`.phone, `customer`.email
    FROM customer_order
    INNER JOIN customer ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='rejected'" ;

    $sql6= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription, `customer_order`.price,
    `customer_order`.orderReceivalDate, `customer_order`.customerID, `customer_order`.orderStartDate,
    `customer`.fName, `customer`.lName, `customer`.phone, `customer_order`.orderExpectedFinish, `customer`.email
    FROM customer_order
    INNER JOIN customer ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='reviewing'" ; 


    $result1 = mysqli_query($conn, $sql1);
    $result2 = mysqli_query($conn, $sql2);
    $result3 = mysqli_query($conn, $sql3);
    $result4 = mysqli_query($conn, $sql4); 
    $result5 = mysqli_query($conn, $sql5);
    $result6 = mysqli_query($conn, $sql6);


    // SELECT QUERIES OVER 

    mysqli_close($conn);

?>