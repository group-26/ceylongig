<?php
session_start();

require_once ("C:\wamp64\www\ceylongig\app\model\config.php");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";

$orderID = $_GET["orderID"];
$orderStatus = $_GET['orderStatus'];
$paymentType = $_GET['paymentType'];
$paymentStatus = $_GET['paymentStatus'];
$phone = $_GET['phone'];
$freelancerEmail = $_GET['email'];

if($orderStatus === 'upcoming'){
    if(isset($paymentStatus)){
        $sql = "UPDATE customer_order
                SET customer_order.orderStatus='$orderStatus',customer_order.paymentType='$paymentType', customer_order.paymentStatus='$paymentStatus'
                WHERE customer_order.orderID='$orderID'";

        if ($conn->query($sql) === TRUE) {
            echo '<script language="javascript">';
            echo 'alert("Payment Successful. Now please await until your freelancer starts the order.");';
            echo 'window.location.href="/ceylongig/app/view/pages/customer/customerdashboard.php";';
            echo '</script>';

            $url = 'http://send.ozonedesk.com/api/v2/send.php';
            $data=array("user_id"=>103673,
            "api_key"=>"i0zcdwgn8uyes80sw",
            "sender_id"=>"ozoneDEMO",
            "to"=>$phone,
            "message"=>"From CeylonGig: Online payment for your order $orderID has been completed by the customer. Start working on your order.");

            // use key 'http' even if you send the request to https://...

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { /* Handle error */ }

        } 
        
        else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    else{
        $sql = "UPDATE customer_order
                SET customer_order.orderStatus='$orderStatus',customer_order.paymentType='$paymentType'
                WHERE customer_order.orderID='$orderID'";
        if ($conn->query($sql) === TRUE) {
            echo '<script language="javascript">';
            echo 'alert("You have successfully accepted the order. Now please await until your freelancer starts the order.");';
            echo 'window.location.href="/ceylongig/app/view/pages/customer/customerdashboard.php";';
            echo '</script>';

            $url = 'http://send.ozonedesk.com/api/v2/send.php';
            $data=array("user_id"=>103673,
            "api_key"=>"i0zcdwgn8uyes80sw",
            "sender_id"=>"ozoneDEMO",
            "to"=>$phone,
            "message"=>"From CeylonGig: Your order $orderID has been accepted by the customer. Start working on your order.");

            // use key 'http' even if you send the request to https://...

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { /* Handle error */ }

            }
        
        else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}
else {
    $sql = "UPDATE customer_order
            SET customer_order.orderStatus='$orderStatus',customer_order.paymentType='$paymentType',customer_order.rejectedBy='customer'
            WHERE customer_order.orderID='$orderID'";

    if ($conn->query($sql) === TRUE) {
        echo '<script language="javascript">';
        echo 'alert("Order rejected successfully.");';
        echo 'window.location.href="/ceylongig/app/view/pages/customer/customerdashboard.php";';
        echo '</script>';

        $url = 'http://send.ozonedesk.com/api/v2/send.php';
        $data=array("user_id"=>103673,
        "api_key"=>"i0zcdwgn8uyes80sw",
        "sender_id"=>"ozoneDEMO",
        "to"=>$phone,
        "message"=>"From CeylonGig: Your order $orderID has been accepted by the customer. Start working on your order.");

        // use key 'http' even if you send the request to https://...

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }
    } 
    
    else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}


$conn->close();

?>