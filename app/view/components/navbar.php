<!DOCTYPE html>

<head>
    <title>NavBar</title>
    <link rel="stylesheet" href="../assets/css/navbar.css">  <!--Copy and Paste this within Head Tag-->
</head>

<body>

    <!--copy this whole thing (nav and script) on top of your body-->
    <nav>
        <div>
            <a href="#"><img src="../assets/img/CeylonGig_logo.svg" alt="CeylonGig Logo"></a>
        </div>
        <ul class="nav-links">
           
            <li><a href="#" class="sign-up">Sign Up</a></li>
          
        </ul>
        <div class="burger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
        </div>
    </nav>

    <script src="../JS/navbar.js"></script>

</body>

</html>