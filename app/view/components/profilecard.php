<?php
    session_start(); 

    if (!isset($_SESSION['role'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /ceylongig/app/view/login.php');
    }
    if (($_SESSION['role']) !== "customer"){
      session_destroy();
      $_SESSION['msg'] = "You must log in as customer first";
      header('location: /ceylongig/app/view/login.php');
    }

    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');

    $query = "SELECT * FROM customer WHERE email = '$email'";
    $result = mysqli_query($conn, $query);

    while ($row = mysqli_fetch_assoc($result)){
        $fName = $row["fName"];
        $lName = $row["lName"];
        
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link rel="stylesheet" href="/ceylongig/app/view/assets/css/customerprofile.css">
    <title>Customer Profile - CeylonGig</title>
    <script> 
        $(function(){
          $("#includedContent").load("customernavbar.php"); 
        });
    </script>
</head>
<body>
    <div id="includedContent"></div>
    <div class="modal">
        <img src="../../assets/img/profile.jpg" alt="">
        <div class="close"></div>
    </div>
    
    <div class="container">
        <div class="card">
            <div class="header">
                <div class="hamburger-menu">
                    <div class="center"></div>
                </div>
                <a href="#" class="mail">
                    <i class="far fa-envelope"></i>
                </a>
                <div class="main">
                    <div class="image">
                        <div class="hover">
                            <i class="fas fa-camera fa-2x"></i>
                        </div>
                    </div>
                    <h3 class="name">Nilukshan Krishnaram</h3>
                    <h3 class="job-description">Plumber</h3>
                </div>
            </div>

            <div class="content">
                <div class="left">
                    <div class="about-container">
                        <h3 class="title">About</h3>
                        <p class="text">Lorem Ipsum is simply text of the printing and types industry.</p>
                    </div>
                    <div class="icons-container">
                        <a href="#" class="icon">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>

                    
                    <div class="buttons-wrap">
                        <div class="follow-wrap">
                            <a href="#" class="follow">Hire</a>
                        </div>
                        <div class="share-wrap">
                            <a href="#" class="share">Share</a>
                        </div>
                    </div>
                </div>

                <div class="right">
                    <div>
                        <h3 class="number">91</h3>
                        <h3 class="number-title">Completed Jobs</h3>
                    </div>
                    <div>
                        <h3 class="number">2020</h3>
                        <h3 class="number-title">Joined</h3>
                    </div>
                    <div>
                        <h3 class="number">4.7</h3>
                        <h3 class="number-title">Rating</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/customerprofile.js"></script>
</body>
</html>
