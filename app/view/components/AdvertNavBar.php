<?php

    echo '
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, intial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    
            <link rel="stylesheet" href="./css/navigation.css">
    
            <title>Admin panel</title>
        </head>
    
        <body>
            
            
           
          
           <!-- side bar menus-->
           <input type="checkbox" id="check">
                   <label for="check">
                       <i class="fas fa-bars" id="btn"></i>
                       <i class="fas fa-times" id="cancel"></i>
    
                   </label>
                
           <div class="admin-wrapper">
                <!--left side bar-->
               
                <div class="left-sidebar">
                   
                  
                    <p>
                        Admin panel
                    </p>
                    <!--using i classes-->
                    <ul>
                        <li><a href="#"><i class="fas fa-qrcode"></i>Dashboard</a></li>
                        <li><a href="#"><i class="fas fa-link"></i>Shortcuts</a></li>
                        <li><a href="#"><i class="fas fa-stream"></i>Overview</a></li>
                        <li><a href="#"><i class="fas fa-calendar-week"></i>Events</a></li>
                        <li><a href="#"><i class="fas fa-question-circle"></i>About</a></li>
                        <li><a href="#"><i class="fas fa-sliders-h"></i>Services</a></li>
                        <li><a href="#"><i class="fas fa-envelope"></i>Contact Us</a></li>
                    </ul>
                    
                </div>
           
            </div>   
    
            
           
    
    
        </body>
    
            
    
    
    
    
     </html>';
?>
