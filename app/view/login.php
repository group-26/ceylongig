
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');


?>


<!DOCTYPE html>

<head>
    <?php //echo $_SESSION['msg']; ?>
    <title>Login - CeylonGig</title>
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/admin/header.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/css/navbar.css">
    <link rel="icon" href="assets/img/icon_circle.png" type="image/png">

    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>

    <script> 
        $(function(){
          $("#includedContent").load("components/navbar.html"); 
        });
    </script>


</head>


<body>


    <div id="includedContent"></div>
   
    <form class="loginform" method="POST" action="loginFunctions.php">
        <h1>Login</h1>

        <div class="textbox">
            <input type="email" name="email" required>
            <span data-placeholder="email"></span>
        </div>
       
        <div class="textbox">
            <input type="password" name="password" required>
            <span data-placeholder="Password"></span>
        </div>
        
        <input class="loginbutton" type="submit" value="Login" name="login">

        <div class="bottomtext">
            <a href="email_verify.php">Forgot Password?</a><br><br>
            Don't have an account? <a href="#chooseSignUp">Create one!</a>
        </div>
       
    </form>

    <script type="text/javascript">
        $(".textbox input").on("focus",function(){
            $(this).addClass("focus");
        });

        $(".textbox input").on("blur",function(){
            if($(this).val() == "")
            $(this).removeClass("focus");
        });
    </script>

</body>

</html>
