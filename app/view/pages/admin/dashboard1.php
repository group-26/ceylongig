<?php

session_start();

?>

<?php

if(!isset($_SESSION['userid'])) {
    header('Location: ../login.php');
}

?>

<?php 
//include model file

include ('../../../model/dashboard.php');

?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../../assets/css/admin/admindash.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/admin/pure-min.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/admin/pure-responsive-min.css" />

        <link rel="stylesheet" href="../../assets/css/navbar.css" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

            <script src="https://kit.fontawesome.com/a076d05399.js"></script>


        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
            <script> 
                $(function(){
                $("#includedContent").load("header.php"); 
                });
            </script> 

        
        <title>Document</title>
        

    </head>
    <body>

        <!-- <div id="includedContent1"></div> -->
        <div id="includedContent"></div>

        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items">
                    <h1 class="subhead"> Admin Dashboard</h1>
                </div>

                <div class="pure-g">

                    <div class="pure-u-1 pure-u-md-1-3">
                        <div class="column-block">
                            <div class="column-block-header column-success">
                                <h2>Number of adimin</h2>
                                <span class="column-block-info"><?php  echo $numb1; ?> </span>
                            </div>
                            <ul class="column-block-list">
                                <li>Male <span class="buble-success button-small pull-right"><?php  echo $numb2; ?></span></li>
                                <li>Female <span class="buble-secondary button-small pull-right"><?php  echo $numb3; ?></span></li>
                            
                            </ul>
                        </div>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <div class="column-block">
                            <div class="column-block-header column-warning">
                                <h2>Number of staff</h2>
                                <span class="column-block-info"><?php  echo $numb4; ?></span>
                            </div>
                            <ul class="column-block-list">
                                <li>Male <span class="buble-success button-small pull-right"><?php  echo $numb5; ?></span></li>
                                <li>Female <span class="buble-secondary button-small pull-right"><?php  echo $numb6; ?></span></li>
                            
                            </ul>
                        </div>
                    </div>

                    <div class="pure-u-1 pure-u-md-1-3">
                        <div class="column-block">
                            <div class="column-block-header">
                                <h2>Total number of users</h2>
                                <span class="column-block-info"><?php  echo $numb7; ?></span>
                            </div>
                            <ul class="column-block-list">
                                <li>Male <span class="buble-success button-small pull-right"><?php  echo $numb8; ?></span></li>
                                <li>Female <span class="buble-secondary button-small pull-right"><?php  echo $numb9; ?></span></li>
                            
                            </ul>
                        </div>
                    </div>


                </div>

                <div class="pure-g">
        
            
                    <div class="pure-u-1 pure-u-md-2-3">
                        <div class="column-block">
                            <table class="pure-table pure-table-horizontal">

                            
                                    <thead>
                                    <tr>
                                        <th>UserId</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Gender</th>
                                        <th>Role</th>
                                    </tr>
                                    </thead>

                        <?php
                            while($row=mysqli_fetch_assoc($result)){

                        ?>
                        
                                    <tbody>
                                    <tr>
                                        <td><?php echo $row['userid'] ?></td>
                                        <td><?php echo $row['name'] ?></td>
                                        <td><?php echo $row['email'] ?></td>
                                        <td><?php echo $row['address'] ?></td>
                                        <td><?php echo $row['gender'] ?></td>
                                        <td><?php echo $row['role'] ?></td>

                                    </tr>
                                
                                    </tbody>

                                <?php
            }
            
        
            ?>
                            </table>
                        </div>
                    </div>
                </div>

                <script src="../../assets/js/navbar.js"></script>

    
    </body>
</html>