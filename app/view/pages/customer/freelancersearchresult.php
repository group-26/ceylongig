<?php
    session_start(); 

    if (!isset($_SESSION['role'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /ceylongig/app/view/login.php');
    }
    if (($_SESSION['role']) !== "customer"){
      session_destroy();
      $_SESSION['msg'] = "You must log in as customer first";
      header('location: /ceylongig/app/view/login.php');
    }

    $freelancerCategory = $_GET['freelancerCategory'];
    $customerLocation = $_GET['customerLocation'];
    $dateOfBooking = strtotime($_GET["dateOfBooking"]);
    $dateOfBooking = date('Y-m-d', $dateOfBooking);
    $day = $_GET['btnClickedValue'];
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Freelancer Search Results - CeylonGig</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/freelancersearchresult.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/navbar.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/footer.css">
        <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/table.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/freelancercard.css">
        <script src="/app/view/assets/js/progressbar.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script src="https://kit.fontawesome.com/a81368914c.js"></script>
        <script> 
            $(function(){
              $("#includedContent").load("/ceylongig/app/view/pages/customer/customernavbar.php"); 
            });
            $(function(){
              $("#includedContent2").load("/ceylongig/app/view/components/footer.html"); 
            });
        </script>
    </head>

    <body>
        <div id="includedContent" style="top:0;"></div>
        <br><br>
            <div class="rateFreelancer" style="height: 100vh">
                <h3>Search results for <?php echo $freelancerCategory; ?>s near <?php echo $customerLocation; ?> available on <?php echo $dateOfBooking; ?></h3><br>
                <div class="table">
                    <!--Header-->
                    <div class="row header">
                        <div class="cell" style="padding-left: 40px;">
                            <p></p>
                        </div>
                        <div class="cell" >
                            <p>Name</p>
                        </div>
                        <div class="cell">
                            <p>Location</p>
                        </div>
                        <div class="cell">
                            <p></p>
                        </div>
                        <div class="cell">
                            <p></p>
                        </div>
                    </div>

                    <?php
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                        $query =    "SELECT DISTINCT freelancer.freelancerID, freelancer.fName, freelancer.lName, freelancer.profilePicture, freelancer.address, freelancer_category.categoryID
                                    FROM freelancer, freelancer_category, freelancer_has_category, freelancer_working_days
                                    WHERE freelancer_category.categoryName = '$freelancerCategory' 
                                    AND freelancer_category.categoryID = freelancer_has_category.categoryID
                                    AND freelancer_working_days.freelancerID = freelancer_has_category.freelancerID
                                    AND freelancer.freelancerID = freelancer_has_category.freelancerID
                                    AND freelancer_working_days.workingDays = '$day'
                                    AND freelancer.address LIKE '%$customerLocation%'
                                    AND freelancer.status = 'active'";
                        $result = mysqli_query($conn, $query);

                        if(mysqli_num_rows($result) > 0){
                            while ($row = mysqli_fetch_assoc($result)){
                                echo '
                                <div class="row">
                                    <div class="cell title" style="padding-left: 40px;">
                                        <img src="data:image/jpeg;base64,'.$row["profilePicture"].'" class="dp">
                                    </div>
                                    <div class="cell" style="padding-right: 10px;">
                                        <p>'.$row["fName"].' '.$row["lName"].'</p>
                                    </div>
                                    <div class="cell">
                                        <p>'.$row["address"].'</p>
                                    </div>
                                    <div class="cell">
                                        <a class="button1" href="#openProfile'.$row["freelancerID"].'">View&nbsp;Profile</a>
                                    </div>
                                    <div class="cell">
                                        <a class="button1" href="/ceylongig/app/view/pages/customer/orderplacement.php?freelancerID='.$row["freelancerID"].'&categoryID='.$row["categoryID"].'">Hire</a>
                                    </div>
                                </div>';
                            }
                        }
                        else{
        
         
                                echo"<script> alert('No freelancers were found. Try a different search query.') </script>";
                                echo"<script> location.href='bookfreelancer.php' </script>";
                                
                        }
                    ?>
                </div>
            </div>
            <?php
            include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
            $query =    "SELECT DISTINCT *
                        FROM freelancer, freelancer_category, freelancer_has_category, freelancer_working_days
                        WHERE freelancer_category.categoryName = '$freelancerCategory' 
                        AND freelancer_category.categoryID = freelancer_has_category.categoryID
                        AND freelancer_working_days.freelancerID = freelancer_has_category.freelancerID
                        AND freelancer.freelancerID = freelancer_has_category.freelancerID
                        AND freelancer_working_days.workingDays = '$day'
                        AND freelancer.address LIKE '%$customerLocation%'
                        AND freelancer.status = 'active'";
            $result = mysqli_query($conn, $query);

            if(mysqli_num_rows($result) > 0){
                while ($row = mysqli_fetch_assoc($result)){
                    $ID = $row['freelancerID'];
                    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                    $query2 =   "SELECT * FROM customer_order
                                WHERE customer_order.freelancerID = '$ID'
                                AND customer_order.orderStatus = 'finished'";
                    $result2 = mysqli_query($conn, $query2);
                    $completed_jobs = mysqli_num_rows($result2);

                    $query3=    "SELECT AVG(freelancerRating) + 0.00 AS average , COUNT(freelancerRating) AS total
                                FROM rating
                                WHERE freelancerID='$ID' AND freelancerRating!=0";
                    $result3 = mysqli_query($conn, $query3);
                    $row3 = mysqli_fetch_assoc($result3);

                    echo '
                        <div class="modalDialog" id="openProfile'.$ID.'">
                            
                                <a href="#close" title="Close" class="close"><h1>×</h1></a>
                                <div class="card">
                                    <div class="heading">
                                        <div class="hamburger-menu">
                                            <div class="center"></div>
                                        </div>
                                        <a href="#" class="mail">
                                            <i class="far fa-envelope"></i>
                                        </a>
                                        <div class="main">
                                        <!--<div class="image" style="background: url('; echo "'data:image/jpeg;base64," . $row['profilePicture'] . "'"; echo ') no-repeat center / cover;">-->
                                        <div class="image" style="background: url(data:image/jpeg;base64,' . $row['profilePicture'] . ') no-repeat center / cover;">        
                                                
                                            </div>
                                            <h3 class="name">'.$row["fName"].' '.$row["lName"].'</h3>
                                            <h3 class="job-description" style="margin: 0;">'.$row["categoryName"].'</h3>
                                        </div>
                                    </div>

                                    <div class="content">
                                        <div class="left">
                                            <div class="about-container">
                                                <h3 class="topic" style="margin: 0;">Details</h3>
                                                <p class="text">Gender: '.$row["gender"].'</p>
                                                <p class="text">Phone: '.$row["phone"].'</p>
                                                <p class="text">Email: '.$row["freelancerEmail"].'</p>
                                                <p class="text">Address: '.$row["address"].'</p>
                                                <p class="text">Working Days: '.$row["workingDays"].'</p>
                                            </div>
                                            <div class="icons-container">
                                                <a href="#" class="icon">
                                                    <i class="fab fa-facebook"></i>
                                                </a>
                                                <a href="#" class="icon">
                                                    <i class="fab fa-instagram"></i>
                                                </a>
                                                <a href="#" class="icon">
                                                    <i class="fab fa-linkedin"></i>
                                                </a>
                                                <a href="#" class="icon">
                                                    <i class="fab fa-twitter"></i>
                                                </a>
                                            </div>

                                            
                                            <div class="buttons-wrap">
                                                <div class="follow-wrap">
                                                    <a href="/ceylongig/app/view/pages/customer/orderplacement.php?freelancerID='.$ID.'&categoryID='.$row["categoryID"].'" class="follow">Hire</a>
                                                </div>
                                                <div class="share-wrap">
                                                    <a href="/ceylongig/app/view/pages/customer/freelancerprofile.php?freelancerID='.$ID.'&categoryID='.$row["categoryID"].'" class="share">View More</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="right">
                                            <div>
                                                <h3 class="number">'.$completed_jobs.'</h3>
                                                <h3 class="number-title">Completed Jobs</h3>
                                            </div>
                                            <div>
                                                <h3 class="number">'.$row["experience"].'</h3>
                                                <h3 class="number-title">Years of Experience</h3>
                                            </div>
                                            <div>
                                                <h3 class="number">'.$row3['average'].'</h3>
                                                <h3 class="number-title">Rating</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>    
                        <script src="/ceylongig/app/view/assets/js/customerprofile.js"></script>
                    ';
                }
            }
            ?>
            
        <div id="includedContent2"></div>
    </body>
</html>