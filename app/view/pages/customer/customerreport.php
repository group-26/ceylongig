<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app/view/login.php');
  }
  if (($_SESSION['role']) !== "customer"){
    session_destroy();
    $_SESSION['msg'] = "You must log in as customer first";
    header('location: /ceylongig/app/view/login.php');
  }

  $orderID = $_GET['orderID'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/customerreport.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/navbar.css">
        <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/popupcard.css">
        <script src="/ceylongig/app/view/assets/js/progressbar.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <title>Report a Freelancer - CeylonGig</title>
        <script> 
            $(function(){
              $("#includedContent").load("customernavbar.php"); 
            });
        </script>
    </head>

    <body>
        <div id="includedContent" style="position:fixed; top:0;"></div>
        <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
                <h3>
                <?php 
                    echo $_SESSION['success']; 
                    unset($_SESSION['success']);
                ?>
                </h3>
            </div>
        <?php endif ?>

        <?php

            include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
            $query =   "SELECT DISTINCT *
                        FROM freelancer, customer_order
                        WHERE customer_order.orderStatus = 'finished'
                        AND customer_order.freelancerID = freelancer.freelancerID
                        AND customer_order.orderID = '$orderID'";
            $result = mysqli_query($conn, $query);

            if(mysqli_num_rows($result) > 0){
                while ($row = mysqli_fetch_assoc($result)){
                    echo '
                    <form class="reportform" method="post" action="/ceylongig/app/controller/customer/report.php">
                        <input type="hidden" name="orderID" value="'.$orderID.'">
                        <input type="hidden" name="complaintEmail" value="'.$_SESSION['email'].'">
                        <input type="hidden" name="complaintPhone" value="'.$_SESSION['phone'].'">
                        <input type="hidden" name="complaintActionStatus" value="pending">
                        <input type="hidden" name="freelancerID" value="'.$row["freelancerID"].'">
                        <input type="hidden" name="customerID" value="'.$_SESSION['phone'].'">
                        <h2>What went wrong?</h2>
                        <h4>Give us an appropriate subject for your complaint in one sentence</h4><br>
                        <input type="text" name="complaintName"><br><br>
                        <h4>Let us know more on your experience with '.$row["fName"].':</h4><br>
                        <textarea rows="5" cols="80" name="badExperience"></textarea><br><br>
                        <input type="submit" value="Submit" class="submitbutton">
                    </form>
                    ';
                }
            }

        ?>
        
    </body>
</html> 