<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app/view/login.php');
  }
  if (($_SESSION['role']) !== "customer"){
    session_destroy();
    $_SESSION['msg'] = "You must log in as customer first";
    header('location: /ceylongig/app/view/login.php');
  }
  $customerID = $_SESSION['customerID'];
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Dashboard - CeylonGig</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/customerdashboard.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/navbar.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/footer.css">
        <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/table.css">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/popupcard.css">
        <script src="/ceylongig/app/view/assets/js/progressbar.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script> 
            $(function(){
              $("#includedContent").load("customernavbar.php"); 
            });
            $(function(){
              $("#includedContent2").load("../../components/footer.html"); 
            });
        </script>
    </head>

    <body>
        <div id="includedContent" style="position:fixed; top:0;"></div>
        <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
                <h3>
                    <?php 
                        echo "<script> alert('".$_SESSION['success']."') </script>"; 
                        unset($_SESSION['success']);
                    ?>
                </h3>
            </div>
        <?php endif ?>


        <!--NOTIFICATIONS PANEL-->

        <div class="notifications">
            <h2>Notifications</h2>
            <br>
            <p> No New Notifications. </p>
            <!--
            <div class="table">
                <div class="row">
                    <div class="cell">
                        <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                    </div>
                    <div class="cell title" style="padding-left: 40px;">
                        <p>Repair Tap</p>
                    </div>
                    <div class="cell">
                        <p>Nilukshan Krishnaram Has completed your work.</p>
                    </div>
                    <div class="cell">
                        <p>4 mon</p>
                    </div>
                </div>
    
                <div class="row">
                    <div class="cell">
                        <img src="/ceylongig/app/view/assets/img/chana.jpg" class="dp">
                    </div>
                    <div class="cell title" style="padding-left: 40px;">
                        <p>Birthday Cake</p>
                    </div>
                    <div class="cell">
                        <p>Please Pay your final amount to Chanaka.</p>
                    </div>
                    <div class="cell">
                        <p>3 min</p>
                    </div>
                </div>
            </div><br>
            -->
            <div class="align-right"><a class="button1" href="./bookfreelancer.php">Book&nbsp;a&nbsp;Freelancer</a></div>
        </div>
        
        <!--PENDING ORDERS -->

        <div class="jobPending">
            <h3>Pending Orders</h3><br>
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'pending'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'";
                $result = mysqli_query($conn, $query);
        
                if(mysqli_num_rows($result) > 0){
                    echo '
                        <div class="table">
                            <!--Header-->
                            <div class="row header">
                                <div class="cell" style="padding-left: 40px;">
                                    <p>Order Title</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <div class="cell">
                                    <p>Done By</p>
                                </div>
                                <div class="cell">
                                    <p>Order Description</p>
                                </div>
                                <div class="cell">
                                    <p>Start Date</p>
                                </div>
                                <div class="cell">
                                    <p>Expected Finish Date</p>
                                </div>
                                <!--
                                <div class="cell">
                                    <p>Progress</p>
                                </div>
                                -->
                            </div>';
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div class="row">
                                <div class="cell title" style="padding-left: 40px;">
                                    <p>'.$row["orderName"].'</p>
                                </div>
                                <div class="cell" style="padding-right: 10px;">
                                    <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                                </div>
                                <div class="cell">
                                    <p>'.$row["fName"].' '.$row["lName"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderDescription"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderStartDate"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderExpectedFinish"].'</p>
                                </div>
                                <!--
                                <div class="cell progress">
                                    <div id="progress">
                                        <div id="bar" style="width: 15%;"></div>
                                    </div>
                                </div>
                                -->
                            </div>';
                    }
                    echo '</div>';
                }
                else {
                    echo '<p>No orders are under review as of now. Reload the page or <a href="./bookfreelancer.php">Book a Freelancer.</a></p>';
                }
            ?>    
        </div>

        <!--ORDERS UNDER REVIEW -->

        <div class="jobReview">
            <h3>Jobs Under Review</h3><br>
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'reviewing'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'";
                $result = mysqli_query($conn, $query);
        
                if(mysqli_num_rows($result) > 0){
                    echo '
                        <div class="table">
                            <!--Header-->
                            <div class="row header">
                                <div class="cell" style="padding-left: 40px;">
                                    <p>Order Title</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <div class="cell">
                                    <p>Done By</p>
                                </div>
                                <div class="cell">
                                    <p>Expected Finish Date</p>
                                </div>
                                <div class="cell">
                                    <p>Quoted Price</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <!--
                                <div class="cell">
                                    <p>Progress</p>
                                </div>
                                -->
                            </div>';
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div class="row">
                                <div class="cell title" style="padding-left: 40px;">
                                    <p>'.$row["orderName"].'</p>
                                </div>
                                <div class="cell" style="padding-right: 10px;">
                                    <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                                </div>
                                <div class="cell">
                                    <p>'.$row["fName"].' '.$row["lName"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderExpectedFinish"].'</p>
                                </div>
                                <div class="cell">
                                    <p style="font-weight: bold;">LKR '.$row["price"].'</p>
                                </div>
                                <div class="cell">
                                    <a class="link" href="/ceylongig/app/view/pages/customer/payment.php?orderID='.$row["orderID"].'">Accept</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="linkred" href="/ceylongig/app/controller/customer/changeOrderReviewStatus.php?orderID='.$row["orderID"].'&orderStatus=rejected&rejectedBy=customer&phone='.$row["phone"].'&email='.$row["freelancerEmail"].'">Reject</a>
                                </div>
                                <!--
                                <div class="cell progress">
                                    <div id="progress">
                                        <div id="bar" style="width: 15%;"></div>
                                    </div>
                                </div>
                                -->
                            </div>';
                    }
                    echo '</div>';
                }
                else {
                    echo '<p>No orders are under review as of now. Reload the page or <a href="./bookfreelancer.php">Book a Freelancer.</a></p>';
                }
            ?>    
        </div>

        <!--UPCOMING ORDERS -->

        <div class="upcomingOrders">
            <h3>Upcoming Orders</h3><br>
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'upcoming'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'";
                $result = mysqli_query($conn, $query);
        
                if(mysqli_num_rows($result) > 0){
                    echo '
                        <div class="table">
                            <!--Header-->
                            <div class="row header">
                                <div class="cell" style="padding-left: 40px;">
                                    <p>Order Title</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <div class="cell">
                                    <p>Done By</p>
                                </div>
                                <div class="cell">
                                    <p>Expected Start Date</p>
                                </div>
                                <div class="cell">
                                    <p>Expected Finish Date</p>
                                </div>
                                <div class="cell">
                                    <p>Quoted Price</p>
                                </div>
                                <!--
                                <div class="cell">
                                    <p>Progress</p>
                                </div>
                                -->
                            </div>';
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div class="row">
                                <div class="cell title" style="padding-left: 40px;">
                                    <p>'.$row["orderName"].'</p>
                                </div>
                                <div class="cell" style="padding-right: 10px;">
                                <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                                </div>
                                <div class="cell">
                                    <p>'.$row["fName"].' '.$row["lName"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderStartDate"].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row["orderExpectedFinish"].'</p>
                                </div>
                                <div class="cell">
                                    <p style="font-weight: bold;">LKR '.$row["price"].'</p>
                                </div>
                                <!--
                                <div class="cell progress">
                                    <div id="progress">
                                        <div id="bar" style="width: 15%;"></div>
                                    </div>
                                </div>
                                -->
                            </div>';
                    }
                    echo '</div>';
                }
                else {
                    echo '<p>You do not have any upcoming order as of now. Reload the page or <a href="./bookfreelancer.php">Book a Freelancer.</a></p>';
                }
            ?>    
        </div>

        <!--ONGOING ORDERS/JOBS-->

        <div class="jobProgress">
            <h3>Jobs in Progress</h3><br>
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'ongoing'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'";
                $result = mysqli_query($conn, $query);
        
                if(mysqli_num_rows($result) > 0){
                    echo '
                        <div class="table">
                            <!--Header-->
                            <div class="row header">
                                <div class="cell" style="padding-left: 40px;">
                                    <p>Order Title</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <div class="cell">
                                    <p>Done By</p>
                                </div>
                                <div class="cell">
                                    <p>Expected Finish Date</p>
                                </div>
                                <div class="cell">
                                    <p></p>
                                </div>
                                <!--
                                <div class="cell">
                                    <p>Progress</p>
                                </div>
                                -->
                            </div>';
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div class="row">
                                <div class="cell title" style="padding-left: 40px;">
                                    <p>'.$row["orderName"].'</p>
                                </div>
                                <div class="cell" style="padding-right: 10px;">
                                    <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                                </div>
                                <div class="cell">
                                    <p>'.$row['fName'].' '.$row['lName'].'</p>
                                </div>
                                <div class="cell">
                                    <p>'.$row['orderExpectedFinish'].'</p>
                                </div>
                                <div class="cell">
                                    <a class="link" href="#openDetails'.$row["orderID"].'">View Details...</a>
                                </div>
                                <!--
                                <div class="cell progress">
                                    <div id="progress">
                                        <div id="bar" style="width: 15%;"></div>
                                    </div>
                                </div>
                                -->
                            </div>';
                    }
                    echo '</div>';
                }
                else {
                    echo '<p>No orders ongoing as of now. Reload the page or <a href="./bookfreelancer.php">Book a Freelancer.</a></p>';
                }
            ?>    
        </div>
        
        <!--ONGOING ORDER - PRINT-->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
        $query =   "SELECT DISTINCT *
                    FROM freelancer, customer_order
                    WHERE customer_order.orderStatus = 'ongoing'
                    AND customer_order.freelancerID = freelancer.freelancerID
                    AND customer_order.customerID = '$customerID'";
        $result = mysqli_query($conn, $query);
        if(mysqli_num_rows($result) > 0){
            while ($row = mysqli_fetch_assoc($result)){
                $orderID = $row["orderID"];
                echo '
                    <div id="openDetails'.$orderID.'" class="modalDialog">
                        <div>	
                            <a href="#close" title="Close" class="close"><h1>×</h1></a>
                            <center>
                                <br><br>
                                <h1>Order Details</h1><br>
                                <p></p><br>
                            </center>
                                <div class="reporttable">
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Order ID</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$orderID.'</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Order Title</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$row["orderName"].'</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Order Description</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$row["orderDescription"].'</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Handled by</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$row["fName"].' '.$row["lName"].'</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Freelancer Address</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$row["address"].'</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="reportcell">
                                            <p style="font-weight:bold;">Contact Number</p>
                                        </div>
                                        <div class="reportcell">
                                            <p>'.$row["phone"].'</p>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <center>
                                    <div class="">
                                        <a class="button1" href="/ceylongig/app/controller/customer/printCustomerOrder.php?orderID='.$orderID.'" target="_blank">
                                            Print&nbsp;Order&nbsp;Details
                                        </a>
                                    </div>
                                </center>
                                <br><br>
                        </div>
                    </div>';
            }
        }
        ?>


        <!--RATING A FREELANCER-->

        <div class="rateFreelancer">
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'finished'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'";
                $result = mysqli_query($conn, $query);

                if(mysqli_num_rows($result) > 0){
                    echo '<h3>Feel free to rate the freelancers</h3><br>';
                    echo '
                    <div class="table">
                        <!--Header-->
                        <div class="row header">
                            <div class="cell" style="padding-left: 40px;">
                                <p>Job Title</p>
                            </div>
                            <div class="cell">
                                <p></p>
                            </div>
                            <div class="cell">
                                <p>Done By</p>
                            </div>
                            <div class="cell">
                                <p>Rating</p>
                            </div>
                            <div class="cell">
                                <p></p>
                            </div>
                            <div class="cell">
                                <p></p>
                            </div>
                        </div>';
                        while ($row = mysqli_fetch_assoc($result)){
                            echo '
                            <div class="row">
                                <div class="cell title" style="padding-left: 40px;">
                                    <p>'.$row["orderName"].'</p>
                                </div>
                                <div class="cell" style="padding-right: 10px;">
                                    <div class="dp" style="background: url(data:image/jpeg;base64,'.$row["profilePicture"].') no-repeat center / cover;"></div>
                                </div>
                                <div class="cell">
                                    <p>'.$row['fName'].' '.$row['lName'].'</p>
                                </div>
                                <div class="cell">
                                    <div class="rate">
                                        <input type="radio" id="star5" name="rate" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" id="star4" name="rate" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" name="rate" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" name="rate" value="2" />
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" name="rate" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>
                                    <input type="button" onclick="rateFunction({$row["orderID"]})" id="rate" class="button1">
                                </div>
                                <div class="cell">
                                    <a class="button1" href="#openRating'.$row["orderID"].'">Add&nbsp;Rating</a>
                                </div>
                                <div class="cell">
                                    <a class="link" href="./customerreport.php?orderID='.$row["orderID"].'">Report...</a>
                                </div>
                            </div>';
                        }
                }
                else{
                    echo '<p>No orders pending to be rated as of now.</p>';
                }
            
            ?>
        </div>
        
        <br><br><br><br>

        <?php
            include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
            $query =   "SELECT DISTINCT *
                        FROM freelancer, customer_order
                        WHERE customer_order.orderStatus = 'finished'
                        AND customer_order.freelancerID = freelancer.freelancerID
                        AND customer_order.customerID = '$customerID'";
            $result = mysqli_query($conn, $query);

            if(mysqli_num_rows($result) > 0){
                while ($row = mysqli_fetch_assoc($result)){
                    echo '
                    <div id="openRating'.$row["orderID"].'" class="modalDialog">
                        <div>	
                            <a href="#close" title="Close" class="close"><h1>×</h1></a>
                            <center>
                                <br><br>
                                <h1>Rate your experience!</h1><br><br>
                                <p>How was your experience with '.$row["fName"].'?</p><br>

                                <form action="/ceylongig/app/controller/customer/rateFreelancer.php?orderID='.$row["orderID"].'" method="POST">
                                    <div class="rate">
                                        <input type="radio" id="star5" name="rate" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" id="star4" name="rate" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" name="rate" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" name="rate" value="2" />
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" name="rate" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>

                                    <!--<span class="">
                                        <input id="rating5" type="radio" name="rating" value="5">
                                        <label for="rating5">5</label>
                                        <input id="rating4" type="radio" name="rating" value="4">
                                        <label for="rating4">4</label>
                                        <input id="rating3" type="radio" name="rating" value="3">
                                        <label for="rating3">3</label>
                                        <input id="rating2" type="radio" name="rating" value="2">
                                        <label for="rating2">2</label>
                                        <input id="rating1" type="radio" name="rating" value="1">
                                        <label for="rating1">1</label>
                                    </span>--><br><br>
                                    <label for="experience">Tell us more about your experience...</label>
                                        <textarea rows="5" cols="50" name="experience"></textarea><br><br>

                                    <input type="submit" onclick="rateFunction({'.$row["orderID"].'})" id="rate" class="button1">

                                </form>
                                <br>
                            </center>
                        </div>
                    </div>';
                }
            }
        ?>
        
    </div>
            
            <script>
                function rateFunction(orderID){

                customerRating = $("input[type='radio'][name='rate']:checked").val();

                alert(customerRating);
                alert(orderID);

                // if(customerRating!=undefined){

                /*$.ajax({   

                    url: '../../../controller/customer/rateFreelancer.php',
                    data: {  
                    customerRating:customerRating,
                    orderID:orderID
                    },

                    type: 'POST'
                    }).done(function(resp) {

                    if (resp) {

                    alert ("Order " +orderID+ " been rated successfully!");
                    window.location.href = '../../pages/freelancer/freelancerOrderManagement.php';

                    } else {
                    alert("Error in order rating");
                    }
                    }

                }*/

                //alert(customerRating);
            }
            </script>

            <div id="includedContent2"></div>
    </body>
</html>