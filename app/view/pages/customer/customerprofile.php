<?php
    session_start(); 

    if (!isset($_SESSION['role'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /ceylongig/app/view/login.php');
    }
    if (($_SESSION['role']) !== "customer"){
      session_destroy();
      $_SESSION['msg'] = "You must log in as customer first";
      header('location: /ceylongig/app/view/login.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link rel="stylesheet" href="/ceylongig/app/view/assets/css/customerprofile.css">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="/ceylongig/app/view/assets/css/popupcard.css">
    <?php echo "<title>".$_SESSION['fName']." ".$_SESSION['lName']." - CeylonGig</title>"; ?>
    <script> 
        $(function(){
          $("#includedContent").load("customernavbar.php"); 
        });
    </script>
</head>
<body>
    <div id="includedContent"></div>
    <div class="modal">
        <?php echo "<img src='data:image/jpeg;base64," . base64_encode($_SESSION['profilePicture']) . "' alt=''>" ?>
        <div class="close"></div>
    </div>
    
    <div class="container">
        <div class="card">
            <div class="header">
                <!--
                <div class="hamburger-menu">
                    <div class="center"></div>
                </div>
                
                <a href="#" class="mail">
                    <i class="far fa-envelope"></i>
                </a>-->
                <div class="main">
                    <div class="image" style="background: url(<?php echo "'data:image/jpeg;base64," . base64_encode($_SESSION["profilePicture"]) . "'"; ?>) no-repeat center / cover;">
                        <div class="hover">
                            <i class="fas fa-camera fa-2x"></i>
                        </div>
                    </div>
                    <?php echo "<h3 class='name'>".$_SESSION['fName']." ".$_SESSION['lName']."</h3>"; ?>
                    <h3 class="location"><?php echo $_SESSION['address'] ?></h3>
                </div>
            </div>

            <div class="content">
                <div class="left">
                    <div class="about-container">
                        <h3 class="title">Personal Details</h3>
                        <p class="text">Born: <?php echo $_SESSION['dOB'] ?></p>
                        <p class="text">Gender: <?php echo $_SESSION['gender'] ?></p>
                        <p class="text">Phone: <?php echo $_SESSION['phone'] ?></p>
                        <p class="text">Email: <?php echo $_SESSION['email'] ?></p>
                    </div>
                    <!--
                    <div class="icons-container">
                        <a href="#" class="icon">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a href="#" class="icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>
                    -->
                    <br>
                    <div class="buttons-wrap">
                        <div class="follow-wrap">
                            <a href="#updateProfile" class="follow">Update</a>
                        </div>
                        <div class="share-wrap">
                            <a href="#" class="share">Share</a>
                        </div>
                    </div>
                </div>

                <div class="right">
                    <div>
                        <h3 class="number">91</h3>
                        <h3 class="number-title">Orders</h3>
                    </div>
                    <div>
                        <h3 class="number">2020</h3>
                        <h3 class="number-title">Joined</h3>
                    </div>
                    <div>
                        <h3 class="number">5.0</h3>
                        <h3 class="number-title">Rating</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/customerprofile.js"></script>

    <div id="updateProfile" class="modalDialog">
                    <div>	
                        <a href="#close" title="Close" class="close"><h1>×</h1></a>
                        <center>
                            <br><br>
                            <h1>Update your Profile</h1><br><br>
                        </center>
                            <form style="width:80%; margin:auto;" method = "post" action="/ceylongig/app/controller/customer/updateprofile.php" enctype="multipart/form-data">
                                <?php
                                    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                                    $email = $_SESSION['email'];
                                    $query1 = "SELECT * FROM customer WHERE email = '$email'";
                            
                                    $result1 = mysqli_query($conn, $query1);  
                                    $row1 = mysqli_fetch_assoc($result1);
                    
                                    $phone = $row1["phone"];
                                    $fName = $row1["fName"];
                                    $lName = $row1["lName"];
                                    $NIC = $row1["NIC"];
                                    $address = $row1["address"];
                                    $dOB = $row1["dOB"];
                                    $profilePicture = $row1["profilePicture"];
                                    $gender = $row1["gender"];
                                ?>
                                <table>
                                    <tr>
                                        <td>First&nbsp;Name: </td>
                                        <td colspan="2"><input type="text" name="fName" value="<?php echo $fName; ?>" required></td>
                                        <td>Last&nbsp;Name: </td>
                                        <td colspan="2"><input type="text" name="lName" value="<?php echo $lName; ?>" required></td>
                                    </tr>
                                    <tr>
                                        <td>Date&nbsp;of&nbsp;Birth: </td>
                                        <td colspan="2"><input type="date" name="dOB" value="<?php echo $dOB; ?>"></td>
                                        <td>NIC&nbsp;Number: </td>
                                        <td colspan="2"><input type="text" name="NIC" value="<?php echo $NIC; ?>" required></td>
                                    </tr>
                                    <tr>
                                        <td>Phone: </td>
                                        <td colspan="2"><input type="text" name="phone" value="<?php echo $phone; ?>" required></td>
                                        <td>Email: </td>
                                        <td colspan="2"><input type="email" name="email" value="<?php echo $email; ?>" required></td>
                                    </tr>
                                    <tr>
                                        <td>Gender: </td>
                                        <?php 
                                            if($gender == 'male'){
                                                echo '<td><input type="radio" name="gender" value="female"> Female</td>
                                                <td><input type="radio" name="gender" value="male" checked> Male</td>
                                                <td><input type="radio" name="gender" value="other"> Other</td>
                                                <td><input type="radio" name="gender" value="prefer not to say"> Prefer Not to Say</td>';
                                            } else if($gender == 'female'){
                                                echo '<td><input type="radio" name="gender" value="female" checked> Female</td>
                                                <td><input type="radio" name="gender" value="male" > Male</td>
                                                <td><input type="radio" name="gender" value="other"> Other</td>
                                                <td><input type="radio" name="gender" value="prefer not to say"> Prefer Not to Say</td>';
                                            } else if($gender == 'other'){
                                                echo '<td><input type="radio" name="gender" value="female"> Female</td>
                                                <td><input type="radio" name="gender" value="male" > Male</td>
                                                <td><input type="radio" name="gender" value="other" checked> Other</td>
                                                <td><input type="radio" name="gender" value="prefer not to say"> Prefer Not to Say</td>';
                                            } else {
                                                echo '<td><input type="radio" name="gender" value="female"> Female</td>
                                                <td><input type="radio" name="gender" value="male" > Male</td>
                                                <td><input type="radio" name="gender" value="other" > Other</td>
                                                <td><input type="radio" name="gender" value="prefer not to say" checked> Prefer Not to Say</td>';
                                            }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td>Address: </td>
                                        <td colspan="5" style="width: 100px;"><textarea rows="3" cols="50" name="address" value="<?php echo $address; ?>"></textarea></td>
                                    </tr>
                                    <tr>   
                                        <td>Profile&nbsp;Picture: </td>
                                        <td colspan="2">
                                            <img src=<?php echo "'data:image/jpeg;base64," . base64_encode($_SESSION['profilePicture']) . "'"; ?> alt='Upload 1:1 ratio' width='auto' height='150px'>
                                            <input type="file" name="profilePic" id="profilePicture" accept="image/*" onchange="loadFile(event)" style="display: none;"><br>
                                        </td>
                                        <td>
                                            <p><label for="profilePicture" style="cursor: pointer;" class="share">Upload&nbsp;Image</label></p>
                                        </td>
                                        <td colspan="2">
                                            <img id="output" width="auto" height="150px" />
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <div class="follow-wrap">
                                    <input type="submit" class="follow">
                                </div>
                            </form>
                            <script>
                                var loadFile = function(event) {
                                    var image = document.getElementById('output');
                                    image.src = URL.createObjectURL(event.target.files[0]);
                                };
                            </script>
                    </div>
                </div>
</body>
</html>
