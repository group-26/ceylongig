<?php
    session_start(); 

    if (!isset($_SESSION['role'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /ceylongig/app/view/login.php');
    }
    if (($_SESSION['role']) !== "customer"){
      session_destroy();
      $_SESSION['msg'] = "You must log in as customer first";
      header('location: /ceylongig/app/view/login.php');
    }

    $freelancerID = $_GET['freelancerID'];
    $categoryID = $_GET['categoryID'];

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://kit.fontawesome.com/a81368914c.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/customerprofile.css">
        <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app/view/assets/css/popupcard.css">
        <?php 
            include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
            $query =    "SELECT DISTINCT *
                                    FROM freelancer
                                    WHERE freelancer.freelancerID = '$freelancerID'
                                    AND freelancer.status = 'active'";
            $result = mysqli_query($conn, $query);
        
            if(mysqli_num_rows($result) > 0){
                while ($row = mysqli_fetch_assoc($result)){
                    echo "<title>".$row['fName']." ".$row['lName']." - CeylonGig</title>"; 
                }
            }
        ?>
        <script> 
            $(function(){
            $("#includedContent").load("customernavbar.php"); 
            });
        </script>
    </head>
    <body>
        <div id="includedContent"></div>
        <br><br><br>
        <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                $query =    "SELECT DISTINCT *
                            FROM freelancer, freelancer_category, freelancer_has_category
                            WHERE freelancer_category.categoryID = '$categoryID'
                            AND freelancer.freelancerID = '$freelancerID'
                            AND freelancer_category.categoryID = freelancer_has_category.categoryID
                            AND freelancer.freelancerID = freelancer_has_category.freelancerID
                            AND freelancer.status = 'active'";
                $result = mysqli_query($conn, $query);
                if(mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){
                        $ID = $row['freelancerID'];
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                        $query2 =   "SELECT * FROM customer_order
                                    WHERE customer_order.freelancerID = '$ID'
                                    AND customer_order.orderStatus = 'finished'";
                        $result2 = mysqli_query($conn, $query2);
                        $completed_jobs = mysqli_num_rows($result2);

                        $query3=    "SELECT AVG(freelancerRating) AS average , COUNT(freelancerRating) AS total
                                    FROM rating
                                    WHERE freelancerID='$ID' AND freelancerRating!=0";
                        $result3 = mysqli_query($conn, $query3);
                        $row3 = mysqli_fetch_assoc($result3);

                        echo '  <div class="modal">';
                                    echo "<img src='data:image/jpeg;base64," . $row['profilePicture'] . "' alt=''>";
                        echo '   <div class="close"></div>
                                </div>
                                
                                <div class="container">
                                    <div class="card">
                                        <div class="header">
                                            <!--
                                            <div class="hamburger-menu">
                                                <div class="center"></div>
                                            </div>
                                            
                                            <a href="#" class="mail">
                                                <i class="far fa-envelope"></i>
                                            </a>-->
                                            <div class="main">
                                                <div class="image" style="background: url('; echo "'data:image/jpeg;base64," . $row['profilePicture'] . "'"; echo ') no-repeat center / cover;">
                                                    <div class="hover">
                                                        <i class="fas fa-camera fa-2x"></i>
                                                    </div>
                                                </div>';
                                                echo "<h3 class='name'>".$row['fName']." ".$row['lName']."</h3>";
                                                echo '
                                                <h3 class="location">'; echo $row['categoryName']; echo '</h3>
                                            </div>
                                        </div>

                                        <div class="content">
                                            <div class="left">
                                                <div class="about-container">
                                                    <h3 class="title">Personal Details</h3>
                                                    <p class="text">Born: '.$row['doB'].'</p>
                                                    <p class="text">Gender: '.$row['gender'].'</p>
                                                    <p class="text">Phone: '.$row['phone'].'</p>
                                                    <p class="text">Email: '.$row['freelancerEmail'].'</p>
                                                
                                                <br>';
                                                

                                                //  BEAUTICIAN

                                                if($categoryID === "1"){
                                                    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                                                    $query2 =   "SELECT *
                                                                FROM beautician_services
                                                                WHERE beautician_services.freelancerID = '$ID'";
                                                    $result2 = mysqli_query($conn, $query2);

                                                    $query4 =   "SELECT *
                                                                FROM beautician
                                                                WHERE beautician.freelancerID = '$ID'";
                                                    $result4 = mysqli_query($conn, $query4);

                                                    if(mysqli_num_rows($result4) > 0){
                                                        while ($row4 = mysqli_fetch_assoc($result4)){
                                                            echo '
                                                                <h3 class="title">Beautician Career Profile</h3>
                                                                <p class="text">Beautician Service Type: '.$row4['serviceType'].'</p>
                                                                <p class="text">Gender Type: '.$row4['genderType'].'</p>
                                                                <p class="text">Qualifications: '.$row4['qualifications'].'</p>
                                                                <p class="text">Additional Services: '.$row4['additionalServices'].'</p>
                                                                <br>
                                                                <h3 class="title">Services</h3>
                                                            ';
                                                        }
                                                    }
                                                    if(mysqli_num_rows($result2) > 0){
                                                        while ($row2 = mysqli_fetch_assoc($result2)){
                                                            echo '
                                                            <p class="text">'.$row2['service'].'</p>
                                                            ';
                                                        }
                                                    }
                                                }

                                                //  PLUMBER

                                                if($categoryID === "2"){
                                                    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                                                    $query2 =   "SELECT *
                                                                FROM plumber_services
                                                                WHERE plumber_services.freelancerID = '$ID'";
                                                    $result2 = mysqli_query($conn, $query2);

                                                    $query4 =   "SELECT *
                                                                FROM plumber
                                                                WHERE plumber.freelancerID = '$ID'";
                                                    $result4 = mysqli_query($conn, $query4);

                                                    if(mysqli_num_rows($result4) > 0){
                                                        while ($row4 = mysqli_fetch_assoc($result4)){
                                                            echo '
                                                                <h3 class="title">Plumber Career Profile</h3>
                                                                <p class="text">Additional Services: '.$row4['additionalServices'].'</p>
                                                                <br>
                                                                <h3 class="title">Services</h3>
                                                            ';
                                                        }
                                                    }
                                                    if(mysqli_num_rows($result2) > 0){
                                                        while ($row2 = mysqli_fetch_assoc($result2)){
                                                            echo '
                                                            <p class="text">'.$row2['service'].'</p>
                                                            ';
                                                        }
                                                    }
                                                }

                                                //  PLUMBER

                                                if($categoryID === "2"){
                                                    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');
                                                    $query2 =   "SELECT *
                                                                FROM plumber_services
                                                                WHERE plumber_services.freelancerID = '$ID'";
                                                    $result2 = mysqli_query($conn, $query2);

                                                    $query4 =   "SELECT *
                                                                FROM plumber
                                                                WHERE plumber.freelancerID = '$ID'";
                                                    $result4 = mysqli_query($conn, $query4);

                                                    if(mysqli_num_rows($result4) > 0){
                                                        while ($row4 = mysqli_fetch_assoc($result4)){
                                                            echo '
                                                                <h3 class="title">Plumber Career Profile</h3>
                                                                <p class="text">Additional Services: '.$row4['additionalServices'].'</p>
                                                                <br>
                                                                <h3 class="title">Services</h3>
                                                            ';
                                                        }
                                                    }
                                                    if(mysqli_num_rows($result2) > 0){
                                                        while ($row2 = mysqli_fetch_assoc($result2)){
                                                            echo '
                                                            <p class="text">'.$row2['service'].'</p>
                                                            ';
                                                        }
                                                    }
                                                }

                                                echo '
                                                </div>
                                                <!--
                                                <div class="icons-container">
                                                    <a href="#" class="icon">
                                                        <i class="fab fa-facebook"></i>
                                                    </a>
                                                    <a href="#" class="icon">
                                                        <i class="fab fa-instagram"></i>
                                                    </a>
                                                    <a href="#" class="icon">
                                                        <i class="fab fa-linkedin"></i>
                                                    </a>
                                                    <a href="#" class="icon">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </div>
                                                -->
                                                <br>
                                                <div class="buttons-wrap">
                                                    <div class="follow-wrap">
                                                        <a href="/ceylongig/app/view/pages/customer/orderplacement.php?freelancerID='.$ID.'&categoryID='.$row["categoryID"].'" class="follow">Hire</a>
                                                    </div>
                                                    <!--
                                                    <div class="share-wrap">
                                                        <a href="#" class="share">Share</a>
                                                    </div>
                                                    -->
                                                </div>
                                            </div>

                                            <div class="rightFreelancer">
                                                <div>
                                                <h3 class="numberfreelancer">'.$completed_jobs.'</h3>
                                                <h3 class="number-title">Completed Jobs</h3>
                                                </div>
                                                <div>
                                                    <h3 class="numberfreelancer">'.$row["experience"].'</h3>
                                                    <h3 class="number-title">Experience</h3>
                                                </div>
                                                <div>
                                                    <h3 class="numberfreelancer">'.$row3['average'].'</h3>
                                                    <h3 class="number-title">Rating</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script src="../../assets/js/customerprofile.js"></script>
                        ';
                    }
                }
        ?>
    </body>
</html>
