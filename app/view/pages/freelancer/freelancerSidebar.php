<div class="area"></div>

<nav class="main-menu">
  <ul>
    <li>
      <a href="./freelancerDash.php"
        ><img  class=" sidebar-image" src="../../assets/other/homeicon.svg" />
        <i class="fa fa-home fa-2x"></i>
        <span class="nav-text"> Dashboard </span>
      </a>
    </li>
    <li class="has-subnav">
      <a href="./freelancerOrderManagement.php">
        <img class=" sidebar-image" src="../../assets/other/order.svg" />
        <i class="fa fa-laptop fa-2x"></i>
        <span class="nav-text"> Manage Orders </span>
      </a>
    </li>
    <li class="has-subnav">
      <a href="./freelancerProfile.php">
        <img class=" sidebar-image" src="../../assets/other/user.svg" />
        <i class="fa fa-laptop fa-2x"></i>
        <span class="nav-text"> Profile Management </span>
      </a>
    </li>
    <li class="has-subnav">
      <a href="./freelancerServiceManagement.php">
        <img class=" sidebar-image" src="../../assets/other/services.svg" />
        <i class="fa fa-laptop fa-2x"></i>
        <span class="nav-text"> Services Management </span>
      </a>
    </li>
    <li class="has-subnav">
      <a href="./freelancerReport.php">
        <img class=" sidebar-image" src="../../assets/other/report.svg" />
        <i class="fa fa-list fa-2x"></i>
        <span class="nav-text"> Report Generation </span>
      </a>
    </li>
    <li class="has-subnav">
      <a href="./freelancerComplaint.php">
        <img class=" sidebar-image" src="../../assets/other/complaint.svg" />
        <i class="fa fa-folder-open fa-2x"></i>
        <span class="nav-text"> Complaint Management </span>
      </a>
    </li>
  </ul>
</nav>
