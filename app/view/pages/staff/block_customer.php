
<?php

session_start();

?>

<?php

if(!isset($_SESSION['userid'])) {
    header('Location: ../login.php');
}

?>


<!DOCTYPE html>

<html>

  <head>


    <link rel="stylesheet" href="../../assets/css/staff/block_customer.css " type="text/css">

    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
      $(function(){
        $("#includedContent").load("header.php"); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent1").load('footer.php'); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent2").load('navigation.php'); 
      });
    </script> 



  </head>

  <body>
    <header>
        <div id="includedContent"></div>
    </header>

    <!-- Siddebar-->
    <div id="includedContent2"></div>


    <table>
    
    <tr>

      <th colspan=4><h2>Customer Details</h2></th>

    <tr>  
      <tr>
        <th>User ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Block</th>
      
          
          
      </tr>

    
      <tr>
        <td>ABC</td>
        <td>CM1</td>
        <td>abc@w</td>
        <td><a href="#">Block</a></td>
          
      
      </tr>

      <tr>
        <td>ABC</td>
        <td>CM1</td>
        <td>abc@w</td>
      
        <td><a href="#">Block</a></td>
          
      
      </tr>

      <tr>
        <td>ABC</td>
        <td>CM1</td>
        <td>abc@w</td>
        <td><a href="#">Block</a></td>
          
      
      </tr>
  

    </table>


    <footer>
        <div id="includedContent1"></div>
    </footer>
  

  </body>

</html>