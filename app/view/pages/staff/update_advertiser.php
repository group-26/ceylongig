
<?php

session_start();

?>

<?php

if(!isset($_SESSION['userid'])) {
    header('Location: ../login.php');
}

?>

<!DOCTYPE html>

<html>

  <head>

    <link rel="stylesheet" type="text/css" href="../../assets/css/staff/update_advertiser.css">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
      $(function(){
        $("#includedContent").load("header.php"); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent1").load('footer.php'); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent2").load('navigation.php'); 
      });
    </script> 



  </head>

  <body>

    <header>
        <div id="includedContent"></div>
      
    </header>

    <!-- Siddebar-->

    <div id="includedContent2"></div>


      <table>
                      
         <tr>

            <th colspan=5><h2>Customer Details</h2></th>

        </tr>  
        <tr>
          <th>Company Name</th>
          <th>User Id</th>
          <th>Email</th>
          <th>Phone Number</th>
          <th>Update</th>
          
            
        </tr>

        
        <tr>
          <td>ABC</td>
          <td>CM1</td>
          <td>abc@w</td>
          <td>077</td>
          <td><a href="update_advertiser_form.php">Update</a></td>
              
          
        </tr>

        <tr>
          <td>ABC</td>
          <td>CM1</td>
          <td>abc@w</td>
          <td>077</td>
          <td><a href="update_advertiser_form.php">Update</a></td>
            
        
        </tr>

        <tr>
          <td>ABC</td>
          <td>CM1</td>
          <td>abc@w</td>
          <td>077</td>
          <td><a href="update_advertiser_form.php">Update</a></td>
        
        
        </tr>
      
  
      </table>

        <footer>
          <div id="includedContent1"></div>
        
        </footer>

  </body>

</html>