
<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php 
include ('../../../model/staff/change_status_ad.php');

?>




<html>
    <heaad>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" href="../../assets/css/admin/user_add.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script> 
    $(function(){
    $("#includedContent").load("header.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent1").load("footer.php"); 
    });
</script> 



    </head>
    <body>
    <header>

<div id="includedContent"></div>


</header>


        <div class="container">
            <div class="header">
                <h2>Change advertisement status</h2>
            </div>

            
            <form id="form" class="form" action="change_status_ad.php" method="POST">

          
                <div class="form-control">
                    <label for="username">advertisement ID</label>
                    <input type="text" value="<?php echo $row['advertisementID']; ?>"  name="adid" readonly/>
                   
                </div>
                <div class="form-control">
                    <label for="username">Advertisement Name</label>
                    <input type="text" value="<?php echo $row['advertisementName']; ?>"   name="adname" readonly/>
                  
                </div>
              

                  <div class="form-control">
                    <label for="username">Start Date</label>
                    <input type="text" value="<?php echo $row['startDate']; ?>" name="startdate" readonly/>
                   
                </div>

                <div class="form-control">
                    <label for="username">End Date</label>
                    <input type="text" value="<?php echo $row['endDate']; ?>" name="enddate" readonly/>
                   
                </div>

              

                <div class="form-control">
                    <label for = "username" id="role">U-Role</label>
                   
                    <label class="radio">
                    <input type="radio"  name="status" value="approved"  class="username " <?php if ($row['status']== 'approved') { echo "checked";} ?>>Approved
                        
                        <br>
                    </label>    
                       
                    <label class="radio">
                        <input type="radio"  name="status" value="ongoing" class="username"  <?php if ($row['status']== 'ongoing') { echo "checked";} ?> >Ongoing
                        
                    <br>
                    </label>

                    <label class="radio">
                        <input type="radio"  name="status" value="finished" class="username"  <?php if ($row['status']== 'finished') { echo "checked";} ?> >Finished
                        
                    <br>
                    </label>
                
                
                </div>

          

            <button type="submit" name="update" >Check and submit</button><br> 
            
            </form>
        </div>
     
      
        <footer>

<div id="includedContent1"></div>

</footer>


    </body>
</html>
