<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php

include ('../../../model/staff/view_current_freelancer.php');

?>




<!DOCTYPE html>

<html>

  <head>


    <link rel="stylesheet" href="../../assets/css/staff/block_freelancer.css " type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>

    <script src="https://code.jquery.com/jquery-3.5.0.js">
    </script>
        <script> 
          $(function(){
            $("#includedContent").load("header.php"); 
          });
        </script> 

        <script> 
          $(function(){
            $("#includedContent1").load('footer.php'); 
          });
        </script> 

        <script> 
          $(function(){
            $("#includedContent2").load('navigation.php'); 
          });
        </script> 

  </head>

  <body>

    <header>
        <div id="includedContent"></div>
      
    </header>

   <!-- Siddebar-->

    <div id="includedContent2"></div>

      <table>
                   
      <tr>

      <th colspan=7><h2>Current Freelancer Details</h2></th>

    <tr> 

        <tr>
            <th>Freelancer Name</th>
            <th>NIC</th>
            <th>Email</th>
            <th>phone</th>
            <th>Status</th>
            <th>View All details</th>
            <th>Block</th>
          
            
            
        </tr>

        <?php
				while($row=mysqli_fetch_assoc($result)){

			?>
      
      <tr>
            <td><?php echo $row['fName'] ?></td>
            <td><?php echo $row['freelancerNIC'] ?></td>
            <td><?php echo $row['freelancerEmail'] ?></td>
            <td><?php echo $row['phone'] ?></td>
            <td><?php echo $row['status'] ?></td>
            <?php echo "<td><a href =current_freelancer_form.php?freelancerEmail='".$row['freelancerEmail']."'> View </a> </td>"?>
           

            <?php echo "<td><a href =block_current_freelancer_reason.php?freelancerEmail=".$row['freelancerEmail']." onclick='return checkdelete()' > Block </a> </td>"?>
                    
				
        </tr>

          
        <?php
    }
    
  
    ?>

    
        
      </table>

             
<script>

    function checkdelete(){
    return confirm('Are you sure delete this record');
    }

</script>


      <footer>
          <div id="includedContent1"></div>
      </footer>   
  </body>

</html>