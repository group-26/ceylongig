
<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php

include ('../../../model/staff/view_customer.php');

?>




<!DOCTYPE html>

<html>

  <head>


    <link rel="stylesheet" href="../../assets/css/staff/block_customer.css " type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
      $(function(){
        $("#includedContent").load("header.php"); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent1").load('footer.php'); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent2").load('navigation.php'); 
      });
    </script> 



  </head>

  <body>
    <header>
        <div id="includedContent"></div>
    </header>

    <!-- Siddebar-->
    <div id="includedContent2"></div>


    <table>
    
    <tr>

      <th colspan=11><h2>Customer Details</h2></th>

      <tr>    
            <th>CustomerId</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>NIC</th>
            <th>Status</th>
            <th>View More</th>
            <th>Block</th>
            
        </tr>

        <?php
				while($row=mysqli_fetch_assoc($result)){

			?>
      
      <tr>
        <td><?php echo $row['customerID'] ?></td>
				<td><?php echo $row['fName'] ?></td>
				<td><?php echo $row['lName'] ?></td>
        <td><?php echo $row['email'] ?></td>
        <td><?php echo $row['NIC'] ?></td>
        <td><?php echo $row['status'] ?></td>
        <?php echo "<td><a href =view_more_customer_details.php?email='".$row['email']."'> View </a> </td>"?> 
        <?php echo "<td><a href =block_customer_reason.php?email=".$row['email']." onclick='return checkdelete()' > Block </a> </td>"?>
                

                
				
        </tr>

          
        <?php
    }
    
  
    ?>

    </table>

       
<script>

function checkdelete(){
  return confirm('Are you sure delete this record');
}

</script>

    <footer>
        <div id="includedContent1"></div>
    </footer>
  

  </body>

</html>