
<?php

session_start();

?>

<?php

if(!isset($_SESSION['userid'])) {
    header('Location: ../login.php');
}

?>

<!DOCTYPE html>

<html>

  <head>

    <link rel="stylesheet" type="text/css" href="../../assets/css/staff/trach_order.css">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
    $(function(){
        $("#includedContent").load("header.php"); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent1").load('footer.php'); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent2").load('navigation.php'); 
      });
    </script> 

  </head>

  <body>

        <header>
        <div id="includedContent"></div>

        </header>

        <!-- Siddebar-->
        <div id="includedContent2"></div>
    <table>

       <tr>
        <th></th>

      </tr>    

    </table>

    <footer>
      <div id="includedContent1"></div>

    </footer>


  </body>

</html>