<?php
include('../../../model/Advertiser/Session.php');
include('../../../model/Advertiser/AdvertChngePwd.php');
?>

<!DOCTYPE html>

<head>
    <title>Change Password - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertChngPwd.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container">
        <nav>
                <?php include('AdvertNav.php') ?>
            </nav>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>Change Password</h1>
            </div>
                
            <form name="chngePwd" action="" method="POST">
                <div id="ChngePwdBlock">
                    <label for="crntPwd">Current Password: </label><br>
                    <input type="Password" id="crntPwd" name="crntPwd" value="" class="txtField" required><br><br><br>
                    <label for="newPwd">New Password: </label><br>
                    <input type="Password" id="newPwd" name="newPwd" value="" class="txtField" minlength="7" maxlength="20" required><br><br><br>
                    <label for="conPwd">Confirm New Password: </label><br>
                    <input type="Password" id="conPwd" name="conPwd" value="" class="txtField" required><br><br><br>
                    <div style="text-align: center;">
                        <input type="submit" value="Change Password" id="chngeBtn" name="chngeBtn">
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    <footer>
            <div class="main-content">
                <div class="left box">
                <h2>About us</h2>
                    <div class="content1">
                        <p>CeylonGig is established with a motive of creating a web-based platform for all self-employed workers, also known as freelancers,
                            to deliver their service efficiently to the customers who are seeking them.</p>
                        <div class="social">
                            <a href="#"><span class="fab fa-facebook-f"></span></a>
                            <a href="#"><span class="fab fa-twitter"></span></a>
                            <a href="#"><span class="fab fa-instagram"></span></a>
                            <a href="#"><span class="fab fa-youtube"></span></a>
                        </div>
                    </div>
                </div>
                <div class="center box">
                    <h2>Address</h2>
                    <div class="content1">
                        <div class="place">
                            <span class="fas fa-map-marker-alt"></span>
                            <span class="text">-----------------</span>
                        </div>
                        <div class="phone">
                            <span class="fas fa-phone-alt"></span>
                            <span class="text">+094-0000000</span>
                        </div>
                        <div class="email1">
                            <span class="fas fa-envelope"></span>
                            <span class="text">abc@example.com</span>
                        </div>
                    </div>
                </div>
                <div class="right box">
                    <h2>Contact us</h2>
                    <div class="content1">
                        <form action="#">
                            <div class="email1">
                                <div class="text">Email *</div>
                                <input type="email" required>
                            </div>
                            <div class="msg">
                                <div class="text">Message *</div>
                                <textarea id=".msgForm" rows="2" cols="25" required></textarea><br />
                                <div class="btn1">
                                    <button type="submit">Send</button1>
                                 </div>
                            <div class="bottom">
                                <center>
                                    <span class="far fa-copyright"></span> 2020 All rights reserved.
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </footer>
</body>