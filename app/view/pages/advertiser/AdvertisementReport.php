<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 include('../../../model/Advertiser/AdvertisementReport.php');
 
 ?>
<!DOCTYPE html>

<head>
    <title>Edit Profile-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertisementReport.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="heading">
            <h1>Advertisement Report</h1><br>
            <h2>Organisation Name: <?php echo $_SESSION['orgName']?></h2>
        </div>
        <div id="pgeHeading">
        </div>
        <div id="formCont">
            <div class="block" id="rightBlock" >
                <label name="adName">Advertisment Name:&nbsp&nbsp<b><?php echo $adName;?></b></br></label><br>
                <label name="adName">Advertisment ID:&nbsp&nbsp<b><?php  echo $advertisementID ;?></b></br></label><br>
                <label name="adName">Organisation Email:&nbsp&nbsp<b><?php echo $_SESSION['orgEmail']?></b></br></label><br>
                <label name="name">Requester Name:&nbsp&nbsp<b><?php echo $reqName;?></b></br></label><br>
                <label name="nic">NIC:&nbsp&nbsp<b><?php echo $nic;?></b></br></label><br>  
                <label name="contactNo">Contact No:&nbsp&nbsp<b><?php  echo $contactNo;?></b></br></label><br>
                <label name="startdate">Started date:&nbsp&nbsp<b><?php echo $startDate;?></b><br></label><br>
                <label name="EndDate">Ended date:&nbsp&nbsp<b><?php echo $endDate;?></b><br></label><br>
                <label name="Clicks">No of Clicks:&nbsp&nbsp<b><?php echo $clicks;?></b><br></label><br>
                <label name="Views:">No of Views:&nbsp&nbsp<b><?php echo $views;?></b><br></label><br>
                <label name="End">Paid amount:&nbsp&nbsp<b><?php echo $price;?></b><br></label><br>

            </div>
            <div class="block" id="leftBlock" >
                <label for="website">Click Destination:&nbsp&nbsp<b><?php echo $clickDest;?></b></label><br></br>
                <label for="advImg">Advertisment Image</label><br> 
                <img id="previewImg" src='data:image/jpeg;base64,<?php echo "$adImage"; ?>' />
            </div>
            <div id="btnBlock">
                <input type="submit" value="Download" id="submitBtn" name="submitBtn"  onclick="demoFromHTML()">
            </div>

        </div>

    </div>
    <div id="table12">
        <table hidden class="tg">
            <tr>
                <th class="tg-0pky">Organistation</th>
                <th class="tg-0pky"><?php echo $_SESSION['orgName'] ?></th>
            </tr>
            <tr>
                <td class="tg-0pky">Advertisement name:</td>
                <td class="tg-0pky"> <b><?php echo $adName;?></b></td>
            </tr>
            <tr>
                <td class="tg-0pky">Advertisement ID:</td>
                <td class="tg-0pky"><?php echo $referneceID ;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">Organisation Email: </td>
                <td class="tg-0pky"> <b><?php echo $_SESSION['orgEmail'] ?></b></td>
            </tr>
            <tr>
                <td class="tg-0pky">Requester Name:<br></td>
                <td class="tg-0pky"> <?php echo $reqName;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">NIC:</td>
                <td class="tg-0pky"><?php echo $nic;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">Contact :</td>
                <td class="tg-0pky"><?php echo $contactNo;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">Started Date:</td>
                <td class="tg-0pky"> <?php echo $startDate;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">Ended Date:</td>
                <td class="tg-0pky"><?php echo $endDate;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">No of Clicks: </td>
                <td class="tg-0pky"><?php echo $clicks;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">No of Views:</td>
                <td class="tg-0pky"><?php echo $views;?></td>
            </tr>
            <tr>
                <td class="tg-0pky">Paid Amount:<br></td>
                <td class="tg-0pky"><?php echo $price;?></td>
            </tr>
            <tr>
                <td class="tg-0lax">Click Destination:</td>
                <td class="tg-0lax"><?php echo $clickDest;?></td>
            </tr>
            <tr>
                <td class="tg-0lax">Advertisement image:</td>
                <td class="tg-0lax"> <img id="previewImg" src='data:image/jpeg;base64,<?php echo "$adImage"; ?>' /></td>
            </tr>
        </table>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
    <script>
    function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#table12')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function(element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 700
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, {// y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },
    function(dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('AdvertisementReport.pdf');
    }
    , margins);
}
    </script>
</body>