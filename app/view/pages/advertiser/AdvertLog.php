<!DOCTYPE html>

<head>
    <title>Log-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/AdvertLog.css">
    <link rel="stylesheet" href="../../assets/css/navbar.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <div>
                <a href="#"><img src="../../assets/other/CeylonGig_Logo.svg" alt="CeylonGig Logo"></a>
            </div>
            <ul class="nav-links">
                <li><a href="AdvertDashboard.php">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Profile</a></li>
            </ul>
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
        </nav>
        <div id="heading">
            <h1>Overall Statistics</h1>
        </div>
        <div id="blockCont">
            <div class="blocks" id="block1">
                <h1>1024</h1><br>
                <h1><span style="font-size:x-large;">Clicks</span></h1>
            </div>
            <div class="blocks" id="block2">
                <h1>9034</h1><br>
                <h1><span style="font-size:x-large;">Views</span></h1>
            </div>
            <div class="blocks" id="block3">
                <h1>15</h1><br>
                <h1><span style="font-size:x-large;">Days</span></h1>
            </div>

        </div>
        <div id="currentAdStat">
          <input type="submit" value="Statistics for the Progressing Advertisment" id="currentAdStatBtn">
    
        </div>
       
        <div id="logTable">
            <table class="tg">
                <thead>
                  <tr>
                    <th class="tg-frgj">Ad ID</th>
                    <th class="tg-frgj">Organisation</th>
                    <th class="tg-frgj">Ownwer<br></th>
                    <th class="tg-frgj">Owner NIC</th>
                    <th class="tg-frgj">Start Date</th>
                    <th class="tg-frgj">End Date</th>
                    <th class="tg-frgj">Status</th>
                    <th class="tg-frgj">Report</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="tg-vvj7">001</td>
                    <td class="tg-vvj7">CeylonGIG</td>
                    <td class="tg-vvj7">Ahamed<br></td>
                    <td class="tg-vvj7">973511872V<br></td>
                    <td class="tg-vvj7">12-12-2020<br></td>
                    <td class="tg-vvj7">19-12-2020</td>
                    <td class="tg-vvj7">Finished</td>
                    <td class="tg-vvj7"><input type="button" value="Download" id="downloadBtn"></td>
                  </tr>
                  <tr>
                    <td class="tg-vvj7">024</td>
                    <td class="tg-vvj7">CeylonGIG<br></td>
                    <td class="tg-vvj7">Dhanush</td>
                    <td class="tg-vvj7">123456789V</td>
                    <td class="tg-vvj7">21-12-2020<br></td>
                    <td class="tg-vvj7">21-01-2021</td>
                    <td class="tg-vvj7">Progressing</td>
                    <td class="tg-vvj7">Unavalible</td>
                  </tr>
                  <tr>
                    <td class="tg-vvj7">027</td>
                    <td class="tg-vvj7">CeyllonGIG</td>
                    <td class="tg-vvj7">Ahamed<br></td>
                    <td class="tg-vvj7">973511872V<br></td>
                    <td class="tg-vvj7">22-01-2021</td>
                    <td class="tg-vvj7">23-01-2021</td>
                    <td class="tg-vvj7">Approved</td>
                    <td class="tg-vvj7">Unavalible</td>
                  </tr>
                  <tr>
                    <td class="tg-vvj7">029</td>
                    <td class="tg-vvj7">CeyllonGIG</td>
                    <td class="tg-vvj7">Ahamed<br></td>
                    <td class="tg-vvj7">973511872V<br></td>
                    <td class="tg-vvj7">24-01-2021</td>
                    <td class="tg-vvj7">31-01-2021</td>
                    <td class="tg-vvj7">Pending <a href="#"> (edit)</a></td>
                    <td class="tg-vvj7">Unavalible</td>
                  </tr>
                </tbody>
            </table>
        </div>
    
    
    <footer>
       
    </footer>
    
    
</body>