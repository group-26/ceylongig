<?php
include('../../../model/Advertiser/Session.php');
include('../../../model/Advertiser/AdvertStat.php');
?>
<!DOCTYPE html>

<head>
    <title>Log-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertStat.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="heading">
            <h1><?php echo $heading ?></h1>
        </div>
        <div id="blockCont">
            <div class="blocks" id="block1">
                <h1><?php echo $views ?></h1><br>
                <h1><span style="font-size:x-large;">Views</span></h1>
            </div>
            <div class="blocks" id="block2">
                <h1><?php echo $clicks ?></h1><br>
                <h1><span style="font-size:x-large;">Clicks</span></h1>
            </div>
            <div class="blocks" id="block3">
                <h1><?php echo $days ?></h1><br>
                <h1><span style="font-size:x-large;">Days</span></h1>
            </div>

        </div>
        <div id="currentAdStat">
          <input type="submit" value="Statistics for the Progressing Advertisment" id="currentAdStatBtn">
        </div>
        <div id="logTable">
            <table class="tg">
                <thead>
                  <tr>
                    <th class="tg-frgj">Advertisement ID</th>
                    <th class="tg-frgj">Advertisement Name</th>
                    <th class="tg-frgj">Requested By<br></th>
                    <th class="tg-frgj">Requester NIC</th>
                    <th class="tg-frgj">Start Date</th>
                    <th class="tg-frgj">End Date</th>
                    <th class="tg-frgj">clickDestination</th>
                    <th class="tg-frgj">Price</th>
                    <th class="tg-frgj">View Stat</th>
                  </tr>
                </thead>
                <tbody>
                  <?php include('../../../model/Advertiser/AdvertStatTable.php');?>
                </tbody>
            </table>
        </div>

    </body>
</html>