<!DOCTYPE html>

<head>
    <title>Profile-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/advertProfile.css">
    <link rel="stylesheet" href="../../assets/css/navbar.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <div>
                <a href="#"><img src="../../assets/other/CeylonGig_Logo.svg" alt="CeylonGig Logo"></a>
            </div>
            <ul class="nav-links">
                <li><a href="AdvertDashboard.php">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Profile</a></li>
            </ul>
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
        </nav>
        <div id="content">
            <div id="heading">
                <h1>Profile</h1>
            </div>
            <div id="detailsleft">
                <label for="orgID">Organisation ID:</label><br>
                <label for="orgID">#</label><br><br>
                <label for="orgName">Organisation Name:</label><br>
                <label for="orgName">#</label><br><br>
                <label for="orgAddress">Address:</label><br>
                <label for="orgAddress">#</label><br><br>
                <label for="orgEmail">Email:</label><br>
                <label for="orgEmail">#</label><br><br>
                <label for="orgContact">Contact No:</label><br>
                <label for="orgContact">#</label><br><br>

                
            </div>
            <div id="detailsright">
                <img id="profileimg" name="prifileimg" src="#" alt=""><br>
                <label for="orgID">Organistation Profile Image</label><br><br>
                
            </div>
            <div id="editBtnCon">
                <input type="submit" value="Edit Details" id="editBtn" onclick="window.location.href='advertEditProfile.php'">
            </div>
        </div>

    </div>
    <footer>
       
    </footer>
    
    
</body>