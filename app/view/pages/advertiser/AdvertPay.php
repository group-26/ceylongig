<?php
include('../../../model/Advertiser/Session.php');
?>
<?php
 include('../../../model/Advertiser/connection.php');
 ?>

<!DOCTYPE html>

<head>
    <title>Payments - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertPay.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            
            <div id="pgeHeading">
                <h1>Online Payment</h1>
            </div>
            <form>
                <div id="payBlock" >
                    <h3>Please Enter your card details</h3>
                    <img class="crdImg" src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                    <label for="name">Name On Card</label><br>
                    <input type="text" id="crdName" name="crdName" value="" class="txtField" required><br><br>
                    <label for="name">Card Number:</label><br>
                    <input type="text" id="crdName" name="crdName" value="" class="txtField" required><br><br>
                    <label for="name">Expiray Date:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                    <label for="name">CVC:</label><br>
                    <input type="text" id="expDate" name="expDate" value="MM/YY" class="txtField" required>
                    <input type="password" id="cvc" name="cvc" value="" class="txtField" required><br><br>
                    <div style="text-align:center;"> 
                        <input type="submit" value="Proceed" id="payBtn" name="payBtn">
                    </div>
                </div>
            </form>
            
        </div>
    </div>
        <footer>
            <div class="main-content">
                <div class="left box">
                <h2>About us</h2>
                    <div class="content1">
                        <p>CeylonGig is established with a motive of creating a web-based platform for all self-employed workers, also known as freelancers,
                            to deliver their service efficiently to the customers who are seeking them.</p>
                        <div class="social">
                            <a href="#"><span class="fab fa-facebook-f"></span></a>
                            <a href="#"><span class="fab fa-twitter"></span></a>
                            <a href="#"><span class="fab fa-instagram"></span></a>
                            <a href="#"><span class="fab fa-youtube"></span></a>
                        </div>
                    </div>
                </div>
                <div class="center box">
                    <h2>Address</h2>
                    <div class="content1">
                        <div class="place">
                            <span class="fas fa-map-marker-alt"></span>
                            <span class="text">-----------------</span>
                        </div>
                        <div class="phone">
                            <span class="fas fa-phone-alt"></span>
                            <span class="text">+094-0000000</span>
                        </div>
                        <div class="email1">
                            <span class="fas fa-envelope"></span>
                            <span class="text">abc@example.com</span>
                        </div>
                    </div>
                </div>
                <div class="right box">
                    <h2>Contact us</h2>
                    <div class="content1">
                        <form action="#">
                            <div class="email1">
                                <div class="text">Email *</div>
                                <input type="email" required>
                            </div>
                            <div class="msg">
                                <div class="text">Message *</div>
                                <textarea id=".msgForm" rows="2" cols="25" required></textarea><br />
                                <div class="btn1">
                                    <button type="submit">Send</button1>
                                 </div>
                            <div class="bottom">
                                <center>
                                    <span class="far fa-copyright"></span> 2020 All rights reserved.
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
</body>