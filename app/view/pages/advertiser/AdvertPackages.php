<!DOCTYPE html>

<head>
    <title>Packages-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="../../assets/css/navbar.css">
    <link rel="stylesheet" href="../../assets/css/AdvertPackages.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <div>
                <a href="#"><img src="img/CeylonGig_logo.svg" alt="CeylonGig Logo"></a>
            </div>
            <ul class="nav-links">
                <li><a href="AdvertDashboard.php">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Profile</a></li>
            </ul>
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
        </nav>
       
        <div id="leftAd">
            <a href="#"><img id="AdImg" src="../../assets/img/FreelancerAd-02.png" alt="RightAd"></a>
            <p>Your ad may look like this</p>
        </div>
        <div id="rightAd">
            <a href="#"><img id="AdImg" src="../../assets/img/FreelancerAd-01.png" alt="RightAd"></a>
            <p>Your ad may look like this</p>
        </div>
        

        <div id="allPackages">
            <a href="#">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 15000.png" alt="price">
                    </div>
                    <h2>Monthly<br>Package</h2>
                    <p><br>This pacakage will be valid for 30 days</p>
                </div>
            </a>
            <a href="#">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 4000.png" alt="price">
                    </div>
                    <h2>Weekly<br>Package</h2>
                    <p><br>This pacakage will be valid for 07 days</p>
                </div>
            </a>
            <a href="#">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 650.png" alt="price">
                    </div>
                    <h2>Daily<br>Package</h2>
                    <p><br>This pacakage will be valid for 24 hours</p>
                </div>
            </a>

        </div>
        
    </div>
    
    
    <footer>
       
    </footer>
    
    
</body>