

const form = document.getElementById('form');
const username = document.getElementById('username');
const userid = document.getElementById('userid');
const email = document.getElementById('email');

let value=false;

form.addEventListener('submit', e => {


	if (!value){
		e.preventDefault();
		checkInputs();
	}
	else{
		checkInputs();
	}
	
	
	
});
if (value == false)
function checkInputs() {
	// trim to remove the whitespaces
	const usernameValue = username.value.trim();
	const useridValue = userid.value.trim();
    const emailValue = email.value.trim();

	
	{
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
		value = false;
		return	

	} else {
		setSuccessFor(username);
		value = true;
	}
	
	if(useridValue === '') {
		setErrorFor(userid, 'Userid cannot be blank');
		value = false;
		return
			

	} else {
		setSuccessFor(userid);
		
		value = true;
	}
	
	


	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
		value = false;
		return
			

	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
		value = false;
		return
			

	} else {
		setSuccessFor(email);
		
		value = true;
	}
	

	
	


	
}
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}