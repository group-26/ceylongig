

const form = document.getElementById('form');
const name = document.getElementById('name');
const cid= document.getElementById('cid');

$(document).ready(function(){

    $(function(){
        var dtToday = new Date();

        var month = dtToday.getMonth()+1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();

        if(month < 10)
            month = '0' + month.toString();
        
        if(day<10)
            day = '0' +day.toString();    

            var maxDate = year + '-' + month + '-' + day;

            $('#dob').attr('max', maxDate)
    })


})





let value=false;

form.addEventListener('submit', e => {


	if (!value){
		e.preventDefault();
		checkInputs();
	}
	else{
		checkInputs();
	}
	
	
	
});
if (value == false)
function checkInputs() {
	// trim to remove the whitespaces
	const nameValue = name.value.trim();
	const cidValue = cid.value.trim();

	
	{

	if(nameValue === '') {
		setErrorFor(name, 'Name cannot be blank');
		value = false;
		return	

	} else {
		setSuccessFor(name);
		value = true;
	}


	if(cidValue === '') {
		setErrorFor(cid, 'Category ID cannot be blank');
		value = false;
		return	

	} else {
		setSuccessFor(cid);
		value = true;
	}




	
}
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}