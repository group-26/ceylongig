var loadFile = function(event){
    var preview = document.getElementById('previewImg');
    preview.src = URL.createObjectURL(event.target.files[0]);
    preview.onload = function(){
        URL.revokeObjectURL(preview.src)
    }
};