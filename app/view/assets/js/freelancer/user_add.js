//take values from the form

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');
const userid = document.getElementById('userid');
const address = document.getElementById('address');
const dob = document.getElementById('dob');

//assign false to a variable

let value=false;

form.addEventListener('submit', e => {

	//create a statement to prevent the process and checkinputs

	if (!value){
		e.preventDefault();
		checkInputs();
	}
	else{
		checkInputs();
	}
	
	
	
});
if (value == false)
function checkInputs() {
	// trim to remove the whitespaces
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();
	const useridValue = userid.value.trim();
	const addressValue = address.value.trim();
	const dobValue = dob.value.trim();
	
	{
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
		value = false;
		return	

	} else {
		setSuccessFor(username);
		value = true;
	}
	
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
		value = false;
		return
			

	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
		value = false;
		return
			

	} else {
		setSuccessFor(email);
		
		value = true;
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
		value = false;
		return

	} else {
		setSuccessFor(password);
		
		value = true;
	}
	
	if(dobValue === '') {
		setErrorFor(dob, 'Date of birth cannot be blank');
		value = false;
		return

	} else {
		setSuccessFor(dob);
		
		value = true;
	}


	if(useridValue === '') {
		setErrorFor(userid, 'Password2 cannot be blank');
		value = false;
		return
			

	
	}  else{
		setSuccessFor(userid);
		
		value = true;
	}

	if(addressValue === '') {
		setErrorFor(address, 'Password2 cannot be blank');
		value = false;
		return

	}  else{
		setSuccessFor(address);
		
		value = true;
		
		
	}
	
	if(password2Value === '') {
		setErrorFor(password2, 'Password2 cannot be blank');
		value = false;
		return
			

	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
		value = false;
		return
			

	} else{
		setSuccessFor(password2);
		
		value = true;
	}

	
}
}
//error method

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}
//success method
function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	//check email validity
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}