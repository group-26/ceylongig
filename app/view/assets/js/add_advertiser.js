

const form = document.getElementById('form');
const username = document.getElementById('username');
const userid = document.getElementById('userid');
const password = document.getElementById('password');
const repassword = document.getElementById('repassword');
const email = document.getElementById('email');
const address = document.getElementById('address');
const phone = document.getElementById('phone');


let value=false;

form.addEventListener('submit', e => {


	if (!value){
		e.preventDefault();
		checkInputs();
	}
	else{
		checkInputs();
	}
	
	
	
});
if (value == false)
function checkInputs() {
	// trim to remove the whitespaces
	const usernameValue = username.value.trim();
	const useridValue = userid.value.trim();
    const passwordValue = password.value.trim();
    const repasswordValue = repassword.value.trim();
    const emailValue = email.value.trim();
    const addressValue = address.value.trim();
    const phoneValue = phone.value.trim();
    

	
	{
	if(usernameValue === '') {
		setErrorFor(username, 'name cannot be blank');
		value = false;
		return	

	} else {
		setSuccessFor(username);
		value = true;
	}
	
	if(useridValue === '') {
		setErrorFor(userid, 'Userid cannot be blank');
		value = false;
		return
			

	} else {
		setSuccessFor(userid);
		
		value = true;
	}
	
	


    if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
		value = false;
		return

	} else {
		setSuccessFor(password);
		
		value = true;
	}
	
	if(repasswordValue === '') {
		setErrorFor(repassword, 'Re entered password cannot be blank');
		value = false;
		return
			

	} else if(passwordValue !== repasswordValue) {
		setErrorFor(repassword, 'Passwords does not match');
		value = false;
		return
			

	} else{
		setSuccessFor(repassword);
		
		value = true;
	}


    if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
		value = false;
		return
			

	} else {
		setSuccessFor(email);
		
		value = true;
    }
    
    if(phoneValue === '') {
		setErrorFor(phone, 'Phone number cannot be blank');
		value = false;
		return
			

	} else {
		setSuccessFor(phone);
		
		value = true;
    }
    
    if(addressValue === '') {
		setErrorFor(address, 'Address cannot be blank');
		value = false;
		return
			

	} else {
		setSuccessFor(address);
		
		value = true;
    }



	
	


	
}
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}