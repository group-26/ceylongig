<?php
    session_start();
    
    // initializing variables
    $role = "";
    $fName= "";
    $errors = array(); 
    
    // connect to the database
    require_once ("C:\wamp64\www\ceylongig\app\model\config.php");
    
    // REGISTER USER
    if (isset($_POST['reg_customer'])) {
      // receive all input values from the form
      $fName = mysqli_real_escape_string($conn, $_POST['fName']);
      $lName = mysqli_real_escape_string($conn, $_POST['lName']);
      $NIC = mysqli_real_escape_string($conn, $_POST['NIC']);
      $phone = mysqli_real_escape_string($conn, $_POST['phone']);
      $gender = mysqli_real_escape_string($conn, $_POST['gender']);
      $email = mysqli_real_escape_string($conn, $_POST['email']);
      $password_1 = mysqli_real_escape_string($conn, $_POST['PasswordEnter1']);
      $password_2 = mysqli_real_escape_string($conn, $_POST['PasswordEnter2']);
    
      // form validation: ensure that the form is correctly filled ...
      // by adding (array_push()) corresponding error unto $errors array
      if (empty($fName)) { array_push($errors, "Firstname is required"); }
      if (empty($lName)) { array_push($errors, "Lastname is required"); }
      if (empty($NIC)) { array_push($errors, "NIC Number is required"); }
      if (empty($email)) { array_push($errors, "Email is required"); }
      if (empty($phone)) { array_push($errors, "Phone Number is required"); }
      if (empty($password_1)) { array_push($errors, "Password is required"); }
      if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
      }
    
      // first check the database to make sure 
      // a user does not already exist with the same username and/or email
      $customer_check_query = "SELECT * FROM customer WHERE NIC='$NIC' OR email='$email' LIMIT 1";
      $result = mysqli_query($conn, $customer_check_query);
      $customer = mysqli_fetch_assoc($result);
      
      if ($customer) { // if user exists
        if ($customer['NIC'] === $NIC) {
          array_push($errors, "An account with the same NIC already exists");
        }
    
        if ($customer['email'] === $email) {
          array_push($errors, "An account with the same Email already exists");
        }
      }
    
      // Finally, register user if there are no errors in the form
      if (count($errors) == 0) {
          $password = md5($password_1);//encrypt the password before saving in the database
    
          $query = "INSERT INTO customer (fName, lName, NIC, phone, gender, email, password) 
                    VALUES('$fName', '$lName', '$NIC', '$phone', '$gender', '$email', '$password');
                    INSERT INTO loginuser (email, password, role) 
                    VALUES('$email', '$password', 'customer')";
          mysqli_query($conn, $query);
          $_SESSION['fName'] = $fName;
          $_SESSION['role'] = customer;
          $_SESSION['success'] = "You are now logged in";
          header('location: /ceylongig/app/view/pages/customerdashboard.php');
      }
    }
    
?>