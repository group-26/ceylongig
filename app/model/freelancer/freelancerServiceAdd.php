<?php

    include_once('../../model/config.php');
    session_start();

    $add_category=$_POST['add_category'];

    $freelancerID=84;

    // echo $add_category;

    // **********************
    // BEAUTICIAN
    // **********************

    if ($add_category==1){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category1="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,1)";

        mysqli_query($conn, $sql_category1);
        
        //MANDATORY FIELDS 

        $beauticianServiceType=$_POST['beauticianServiceType'];
        $beauticianGenderType=$_POST['beauticianGenderType'];

        //OPTIONAL FIELDS

        $beauticanQualifications=" ";
        if(isset($_POST['beauticanQualifications'])){
            $beauticanQualifications=$_POST['beauticanQualifications'];
        }

        $beauticianAdditionalServices=" ";
        if(isset($_POST['beauticianAdditionalServices'])){
            $beauticianAdditionalServices=$_POST['beauticianAdditionalServices'];
        }

        //INSERT QUERY FOR BEAUTICIAN TABLE 

        $sql3="INSERT INTO `beautician`(`freelancerID`, `serviceType`, `genderType`,`additionalServices`,`qualifications`) 
        VALUES ($freelancerID,'$beauticianServiceType','$beauticianGenderType','$beauticianAdditionalServices','$beauticanQualifications')";

        if(!mysqli_multi_query($conn, $sql3)){
            echo "Error: " . $sql3 . "<br>" . mysqli_error($conn);
        }
            
    

        //INSERT QUERY FOR BEAUTICIAN SERVICES TABLE ------>>> FROM checkboxes
    

        $beauticianServices= json_decode($_POST['beauticianServices']);

        for ($x = 0; $x <= count($beauticianServices)-1; $x++){
            $sql4="INSERT INTO `beautician_services`(`freelancerID`, `service`) 
            VALUES ($freelancerID,'$beauticianServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql4)){
                echo "Error: " . $sql4 . "<br>" . mysqli_error($conn);
            }
        }
    }

    // **********************
    // PLUMBER
    // **********************


    else if($add_category==2){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category2="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,2)";

        mysqli_query($conn, $sql_category2);
        
        
        //OPTIONAL FIELDS

        $plumberAdditionalServices=" ";
        if(isset($_POST['plumberAdditionalServices'])){
            $plumberAdditionalServices=$_POST['plumberAdditionalServices'];
        }

        
        //INSERT QUERY FOR PLUMBER TABLE 

        $sql5="INSERT INTO `plumber`(`freelancerID`, `additionalServices`) 
        VALUES ($freelancerID,'$plumberAdditionalServices')";

        if(!mysqli_multi_query($conn, $sql5)){
            echo "Error: " . $sql5 . "<br>" . mysqli_error($conn);
        }

        //INSERT QUERY FOR PLUMBER SERVICES TABLE ------>>> FROM checkboxes 
        
        $plumberServices= json_decode($_POST['plumberServices']);

        for ($x = 0; $x <= count($plumberServices)-1; $x++){

            $sql6="INSERT INTO `plumber_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$plumberServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql6)){
                echo "Error: " . $sql6 . "<br>" . mysqli_error($conn);
            }
        }
    }



    // **********************
    // PAINTER
    // **********************


    else if($add_category==3){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category3="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,3)";

        mysqli_query($conn, $sql_category3);
        
        //OPTIONAL FIELDS

        $painterAdditionalServices=" ";
        if(isset($_POST['painterAdditionalServices'])){
            $painterAdditionalServices=$_POST['painterAdditionalServices'];
        }

        
        //INSERT QUERY FOR PAINTER TABLE 

        $sql7="INSERT INTO `painter`(`freelancerID`, `additionalServices`)
        VALUES ($freelancerID,'$painterAdditionalServices')";

        if(!mysqli_multi_query($conn, $sql7)){
            echo "Error: " . $sql7 . "<br>" . mysqli_error($conn);
        }

        //INSERT QUERY FOR PAINTER SERVICES TABLE ------>>> FROM checkboxes 
        
        $painterServices= json_decode($_POST['painterServices']);

        for ($x = 0; $x <= count($painterServices)-1; $x++){
            $sql8="INSERT INTO `painter_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$painterServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql8)){
                echo "Error: " . $sql8 . "<br>" . mysqli_error($conn);
            }
        }


    }


    // **********************
    // COOK
    // **********************


    else if($add_category==4){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category4="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,4)";

        mysqli_query($conn, $sql_category4);

        //MANDATORY FIELDS 

        $cookVisitType=$_POST['cookVisitType'];
    
        //OPTIONAL FIELDS

        $cookQualifications=" ";
        if(isset($_POST['cookQualifications'])){
            $cookQualifications=$_POST['cookQualifications'];
        }

        $cookAdditionalCuisines=" ";
        if(isset($_POST['cookAdditionalCuisines'])){
            $cookAdditionalCuisines=$_POST['cookAdditionalCuisines'];
        }

        //INSERT QUERY FOR COOK TABLE 

        $sql9="INSERT INTO `cook`(`freelancerID`, `visitType`, `additionalCuisines`, `qualifications`)
        VALUES ($freelancerID,'$cookVisitType','$cookAdditionalCuisines','$cookQualifications')";

        if(!mysqli_multi_query($conn, $sql9)){
            echo "Error: " . $sql9 . "<br>" . mysqli_error($conn);
        }
            
        //INSERT QUERY FOR COOK CUISINES TABLE ------>>> FROM checkboxes
    
        $cookCuisines= json_decode($_POST['cookCuisines']);

        for ($x = 0; $x <= count($cookCuisines)-1; $x++){
            $sql10="INSERT INTO `cook_cuisines`(`freelancerID`, `cuisine`) 
            VALUES ($freelancerID,'$cookCuisines[$x]')"; 

            if(!mysqli_multi_query($conn, $sql10)){
                echo "Error: " . $sql10 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // TAILOR
    // **********************


    else if($add_category==5){

         // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

         $sql_category5="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
         VALUES ($freelancerID,5)";

         mysqli_query($conn, $sql_category5);

         //MANDATORY FIELDS 

         $tailorGenderType=$_POST['tailorGenderType'];
     
         //OPTIONAL FIELDS
     
         $tailorAdditionalDressTypes=" ";
         if(isset($_POST['tailorAdditionalDressTypes'])){
             $tailorAdditionalDressTypes=$_POST['tailorAdditionalDressTypes'];
         }

         //INSERT QUERY FOR TAILOR TABLE 

         $sql11="INSERT INTO `tailor`(`freelancerID`, `genderType`, `additionalDressTypes`)
         VALUES ($freelancerID,'$tailorGenderType','$tailorAdditionalDressTypes')";

         if(!mysqli_multi_query($conn, $sql11)){
             echo "Error: " . $sql11 . "<br>" . mysqli_error($conn);
         }
             
         //INSERT QUERY FOR TAILOR DRESS TYPES TABLE ------>>> FROM checkboxes
     
         $tailorDressTypes= json_decode($_POST['tailorDressTypes']);

         for ($x = 0; $x <= count($tailorDressTypes)-1; $x++){
             $sql12="INSERT INTO `tailor_dress_types`(`freelancerID`, `dressType`)
             VALUES ($freelancerID,'$tailorDressTypes[$x]')"; 

             if(!mysqli_multi_query($conn, $sql12)){
                 echo "Error: " . $sql12 . "<br>" . mysqli_error($conn);
             }
         }


    }

    // **********************
    // EVENT ORGANIZER
    // **********************


    else if($add_category==6){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category6="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,6)";

        mysqli_query($conn, $sql_category6);
        
        
        //OPTIONAL FIELDS

        $eventOrganizerAdditionalEvents=" ";
        if(isset($_POST['eventOrganizerAdditionalEvents'])){
            $eventOrganizerAdditionalEvents=$_POST['eventOrganizerAdditionalEvents'];
        }

        $eventOrganizerAdditionalServices=" ";
        if(isset($_POST['eventOrganizerAdditionalServices'])){
            $eventOrganizerAdditionalServices=$_POST['eventOrganizerAdditionalServices'];
        }

        $eventOrganizerQualifications=" ";
        if(isset($_POST['eventOrganizerQualifications'])){
            $eventOrganizerQualifications=$_POST['eventOrganizerQualifications'];
        }

        //INSERT QUERY FOR EVENT ORGANIZER TABLE 

        $sql13="INSERT INTO `event_organizer`(`freelancerID`, `additionalEventTypes`, `additionalServices`, `qualifications`)
        VALUES ($freelancerID,'$eventOrganizerAdditionalEvents','$eventOrganizerAdditionalServices','$eventOrganizerQualifications')";

        if(!mysqli_multi_query($conn, $sql13)){
            echo "Error: " . $sql13 . "<br>" . mysqli_error($conn);
        }
            

        //INSERT QUERY FOR EVENT ORGANIZER EVENT TYPES TABLE ------>>> FROM checkboxes
    

        $eventOrganizerEventTypes= json_decode($_POST['eventOrganizerEventTypes']);

        for ($x = 0; $x <= count($eventOrganizerEventTypes)-1; $x++){
            $sql14="INSERT INTO `event_organizer_event_types`(`freelancerID`, `eventType`)
            VALUES ($freelancerID,'$eventOrganizerEventTypes[$x]')"; 

            if(!mysqli_multi_query($conn, $sql14)){
                echo "Error: " . $sql14 . "<br>" . mysqli_error($conn);
            }
        }


        //INSERT QUERY FOR EVENT ORGANIZER SERVICES TABLE ------>>> FROM checkboxes

        $eventOrganizerServices= json_decode($_POST['eventOrganizerServices']);

        for ($x = 0; $x <= count($eventOrganizerServices)-1; $x++){
            $sql15="INSERT INTO `event_organizer_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$eventOrganizerServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql15)){
                echo "Error: " . $sql15 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // HOMEFOOD MANUFACTURER
    // **********************


    else if($add_category==7){


        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category7="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,7)";

        mysqli_query($conn, $sql_category7);

        //OPTIONAL FIELDS

        $homefoodAdditionalCuisines=" ";
        if(isset($_POST['homefoodAdditionalCuisines'])){
            $homefoodAdditionalCuisines=$_POST['homefoodAdditionalCuisines'];
        }

        $homefoodQualifications=" ";
        if(isset($_POST['homefoodQualifications'])){
            $homefoodQualifications=$_POST['homefoodQualifications'];
        }

        //INSERT QUERY FOR HOMEFOOD MANUFACTURER TABLE 

        $sql16="INSERT INTO `homefood_manufacturer`(`freelancerID`, `additionalCuisines`, `qualifications`)
        VALUES ($freelancerID,'$homefoodAdditionalCuisines','$homefoodQualifications')";

        if(!mysqli_multi_query($conn, $sql16)){
            echo "Error: " . $sql16 . "<br>" . mysqli_error($conn);
        }

            
        //INSERT QUERY FOR HOMEFOOD MANUFACTURER CUISINES TABLE ------>>> FROM checkboxes
    
        $homefoodCuisines= json_decode($_POST['homefoodCuisines']);

        for ($x = 0; $x <= count($homefoodCuisines)-1; $x++){
            $sql17="INSERT INTO `homefood_manufacturer_cuisines`(`freelancerID`, `cuisine`)
            VALUES ($freelancerID,'$homefoodCuisines[$x]')"; 

            if(!mysqli_multi_query($conn, $sql17)){
                echo "Error: " . $sql17 . "<br>" . mysqli_error($conn);
            }
        }

    }

    // **********************
    // DRIVER
    // **********************


    else if($add_category==8){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category8="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,8)";

        mysqli_query($conn, $sql_category8);
        
        //MANDATORY FIELDS 

        $drivingLicense=$_POST['drivingLicense'];
        $driverVehicleType=$_POST['driverVehicleType'];

        //INSERT QUERY FOR DRIVER TABLE 

        $sql18="INSERT INTO `driver`(`freelancerID`, `drivingLicenseNo`, `vehicleType`)
        VALUES ($freelancerID,'$drivingLicense','$driverVehicleType')";

        if(!mysqli_multi_query($conn, $sql18)){
            echo "Error: " . $sql18 . "<br>" . mysqli_error($conn);
        }


    }

    // **********************
    // PROGRAMMER
    // **********************


    else if($add_category==9){

         // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

         $sql_category9="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
         VALUES ($freelancerID,9)";

         mysqli_query($conn, $sql_category9);
         
         //OPTIONAL FIELDS

         $programmerProjects=" ";
         if(isset($_POST['programmerProjects'])){
             $programmerProjects=$_POST['programmerProjects'];
         }

         $programmerAdditionalLanguages=" ";
         if(isset($_POST['programmerAdditionalLanguages'])){
             $programmerAdditionalLanguages=$_POST['programmerAdditionalLanguages'];
         }

         $programmerAdditionalFrameworks=" ";
         if(isset($_POST['programmerAdditionalFrameworks'])){
             $programmerAdditionalFrameworks=$_POST['programmerAdditionalFrameworks'];
         }

         $programmerQualifications=" ";
         if(isset($_POST['programmerQualifications'])){
             $programmerQualifications=$_POST['programmerQualifications'];
         }


         //INSERT QUERY FOR PROGRAMMER TABLE 

         $sql19="INSERT INTO `programmer`(`freelancerID`, `projects`, `additionalLanguages`, `additionalFrameworks`, `qualifications`) 
         VALUES ($freelancerID,'$programmerProjects','$programmerAdditionalLanguages','$programmerAdditionalFrameworks','$programmerQualifications')";

         if(!mysqli_multi_query($conn, $sql19)){
             echo "Error: " . $sql19 . "<br>" . mysqli_error($conn);
         }

         //INSERT QUERY FOR EVENT PROGRAMMER DEVELOPMENT TYPES TABLE ------>>> FROM checkboxes
     

         $programmerDevelopmentType= json_decode($_POST['programmerDevelopmentType']);

         for ($x = 0; $x <= count($programmerDevelopmentType)-1; $x++){
             $sql20="INSERT INTO `programmer_development_type`(`freelancerID`, `developmentType`)
             VALUES ($freelancerID,'$programmerDevelopmentType[$x]')"; 

             if(!mysqli_multi_query($conn, $sql20)){
                 echo "Error: " . $sql20 . "<br>" . mysqli_error($conn);
             }
         }
             

         //INSERT QUERY FOR PROGRAMMER LANGUAGES TABLE ------>>> FROM checkboxes
     

         $programmerLanguages= json_decode($_POST['programmerLanguages']);

         for ($x = 0; $x <= count($programmerLanguages)-1; $x++){
             $sql21="INSERT INTO `programmer_languages`(`freelancerID`, `language`)
             VALUES ($freelancerID,'$programmerLanguages[$x]')"; 

             if(!mysqli_multi_query($conn, $sql21)){
                 echo "Error: " . $sql21 . "<br>" . mysqli_error($conn);
             }
         }


         //INSERT QUERY FOR PROGRAMMER FRAMEWORKS TABLE ------>>> FROM checkboxes

         $programmerFrameworks= json_decode($_POST['programmerFrameworks']);

         for ($x = 0; $x <= count($programmerFrameworks)-1; $x++){
             $sql22="INSERT INTO `programmer_frameworks`(`freelancerID`, `framework`)
             VALUES ($freelancerID,'$programmerFrameworks[$x]')"; 
 
             if(!mysqli_multi_query($conn, $sql22)){
                 echo "Error: " . $sql22 . "<br>" . mysqli_error($conn);
             }
         }


    }

    // **********************
    // MASON
    // **********************


    else if($add_category==10){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category10="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,10)";

        mysqli_query($conn, $sql_category10);

        //OPTIONAL FIELDS
        
        $masonAdditionalServices=" ";
        if(isset($_POST['masonAdditionalServices'])){
            $masonAdditionalServices=$_POST['masonAdditionalServices'];
        }

        //INSERT QUERY FOR MASON TABLE 

        $sql23="INSERT INTO `mason`(`freelancerID`, `additionalServices`)
        VALUES ($freelancerID,'$masonAdditionalServices')";

        if(!mysqli_multi_query($conn, $sql23)){
            echo "Error: " . $sql23 . "<br>" . mysqli_error($conn);
        }
            
        //INSERT QUERY FOR MASON SERVICES TABLE ------>>> FROM checkboxes

        $masonServices= json_decode($_POST['masonServices']);

        for ($x = 0; $x <= count($masonServices)-1; $x++){
            $sql24="INSERT INTO `mason_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$masonServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql24)){
                echo "Error: " . $sql24 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // GRAPHIC DESIGNER
    // **********************


    else if($add_category==11){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category11="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,11)";

        mysqli_query($conn, $sql_category11);

        //OPTIONAL FIELDS

        $graphicDesignerAditionalTypes=" ";
        if(isset($_POST['graphicDesignerAditionalTypes'])){
            $graphicDesignerAditionalTypes=$_POST['graphicDesignerAditionalTypes'];
        }

        $graphicDesignerQualifications=" ";
        if(isset($_POST['graphicDesignerQualifications'])){
            $graphicDesignerQualifications=$_POST['graphicDesignerQualifications'];
        }

        //INSERT QUERY FOR GRAPHIC DESIGNER TABLE 

        $sql25="INSERT INTO `graphic_designer`(`freelancerID`, `additionalDesignTypes`, `qualifications`)
        VALUES ($freelancerID,'$graphicDesignerAditionalTypes','$graphicDesignerQualifications')";

        if(!mysqli_multi_query($conn, $sql25)){
            echo "Error: " . $sql25 . "<br>" . mysqli_error($conn);
        }
    

        //INSERT QUERY FOR DESIGNER DESIGN TYPES TABLE ------>>> FROM checkboxes
    

        $graphicDesignerDesignTypes= json_decode($_POST['graphicDesignerDesignTypes']);

        for ($x = 0; $x <= count($graphicDesignerDesignTypes)-1; $x++){
            $sql26="INSERT INTO `graphic_designer_design_type`(`freelancerID`, `designType`)
            VALUES ($freelancerID,'$graphicDesignerDesignTypes[$x]')"; 

            if(!mysqli_multi_query($conn, $sql26)){
                echo "Error: " . $sql26 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // MECHANIC
    // **********************


    else if($add_category==12){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category12="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,12)";

        mysqli_query($conn, $sql_category12);

        //OPTIONAL FIELDS

        $mechanicAdditionalServices=" ";
        if(isset($_POST['mechanicAdditionalServices'])){
            $mechanicAdditionalServices=$_POST['mechanicAdditionalServices'];
        }

        $mechanicAdditionalVehicleTypes=" ";
        if(isset($_POST['mechanicAdditionalVehicleTypes'])){
            $mechanicAdditionalVehicleTypes=$_POST['mechanicAdditionalVehicleTypes'];
        }

        $mechanicAdditionalVehicleBrands=" ";
        if(isset($_POST['mechanicAdditionalVehicleBrands'])){
            $mechanicAdditionalVehicleBrands=$_POST['mechanicAdditionalVehicleBrands'];
        }

        //INSERT QUERY FOR MECHANIC TABLE 

        $sql27="INSERT INTO `mechanic`(`freelancerID`, `additionalServices`, `additionalVehicleTypes`, `additionalVehicleBrands`)
        VALUES ($freelancerID,'$mechanicAdditionalServices','$mechanicAdditionalVehicleTypes','$mechanicAdditionalVehicleBrands')";

        if(!mysqli_multi_query($conn, $sql27)){
            echo "Error: " . $sql27 . "<br>" . mysqli_error($conn);
        }
            

        //INSERT QUERY FOR MECHANIC SERVICES TABLE ------>>> FROM checkboxes

        $mechanicServices= json_decode($_POST['mechanicServices']);

        for ($x = 0; $x <= count($mechanicServices)-1; $x++){
            $sql28="INSERT INTO `mechanic_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$mechanicServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql28)){
                echo "Error: " . $sql28 . "<br>" . mysqli_error($conn);
            }
        }

        //INSERT QUERY FOR MECHANIC VEHICLE TYPES TABLE ------>>> FROM checkboxes

        $mechanicVehicleType= json_decode($_POST['mechanicVehicleType']);

        for ($x = 0; $x <= count($mechanicVehicleType)-1; $x++){
            $sql29="INSERT INTO `mechanic_vehicle_type`(`freelancerID`, `vehicleType`)
            VALUES ($freelancerID,'$mechanicVehicleType[$x]')"; 

            if(!mysqli_multi_query($conn, $sql29)){
                echo "Error: " . $sql29 . "<br>" . mysqli_error($conn);
            }
        }

        //INSERT QUERY FOR MECHANIC VEHICLE BRANDS TABLE ------>>> FROM checkboxes

        $mechanicVehicleBrands= json_decode($_POST['mechanicVehicleBrands']);

        for ($x = 0; $x <= count($mechanicVehicleBrands)-1; $x++){
            $sql30="INSERT INTO `mechanic_vehicle_brand`(`freelancerID`, `brand`)
            VALUES ($freelancerID,'$mechanicVehicleBrands[$x]')"; 

            if(!mysqli_multi_query($conn, $sql30)){
                echo "Error: " . $sql30 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // CONTENT WRITER
    // **********************


    else if($add_category==13){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category13="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,13)";

        mysqli_query($conn, $sql_category13);

        //OPTIONAL FIELDS

        $contentWriterPreviousWork=" ";
        if(isset($_POST['contentWriterPreviousWork'])){
            $contentWriterPreviousWork=$_POST['contentWriterPreviousWork'];
        }

        $contentWriterAdditionalLanguages=" ";
        if(isset($_POST['contentWriterAdditionalLanguages'])){
            $contentWriterAdditionalLanguages=$_POST['contentWriterAdditionalLanguages'];
        }

        $contentWriterQualifications=" ";
        if(isset($_POST['contentWriterQualifications'])){
            $contentWriterQualifications=$_POST['contentWriterQualifications'];
        }

        //INSERT QUERY FOR CONTENT WRITER TABLE 

        $sql31="INSERT INTO `content_writer`(`freelancerID`, `previousWork`, `additionalLanguages`, `qualifications`)
        VALUES ($freelancerID,'$contentWriterPreviousWork','$contentWriterAdditionalLanguages','$contentWriterQualifications')";

        if(!mysqli_multi_query($conn, $sql31)){
            echo "Error: " . $sql31 . "<br>" . mysqli_error($conn);
        }

        //INSERT QUERY FOR CONTENT WRITER LANGUAGES TABLE ------>>> FROM checkboxes
    

        $contentWriterLanguages= json_decode($_POST['contentWriterLanguages']);

        for ($x = 0; $x <= count($contentWriterLanguages)-1; $x++){
            $sql32="INSERT INTO `content_writer_languages`(`freelancerID`, `language`)
            VALUES ($freelancerID,'$contentWriterLanguages[$x]')"; 

            if(!mysqli_multi_query($conn, $sql32)){
                echo "Error: " . $sql32 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // HOUSEKEEPER
    // **********************


    else if($add_category==14){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category14="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,14)";

        mysqli_query($conn, $sql_category14);
        
        //MANDATORY FIELDS 

        $housekeeperVisitType=$_POST['housekeeperVisitType'];

        //OPTIONAL FIELDS

        $housekeeperComment=" ";
        if(isset($_POST['housekeeperComment'])){
            $housekeeperComment=$_POST['housekeeperComment'];
        }
        
        //INSERT QUERY FOR HOUSEKEEPER TABLE 

        $sql33="INSERT INTO `housekeeper`(`freelancerID`, `visitType`, `comments`)
        VALUES ($freelancerID,'$housekeeperVisitType','$housekeeperComment')";

        if(!mysqli_multi_query($conn, $sql33)){
            echo "Error: " . $sql33 . "<br>" . mysqli_error($conn);
        }


    }

    // **********************
    // ELDERLY CARETAKER
    // **********************


    else if($add_category==15){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category15="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,15)";

        mysqli_query($conn, $sql_category15);
        
        //MANDATORY FIELDS 

        $elderlyCaretakerVisitType=$_POST['elderlyCaretakerVisitType'];
        $elderlyCaretakerGenderType=$_POST['elderlyCaretakerGenderType'];
        $elderlyCaretakerAgeFrom=$_POST['elderlyCaretakerAgeFrom'];
        $elderlyCaretakerAgeTo=$_POST['elderlyCaretakerAgeTo'];

        
        //INSERT QUERY FOR ELDERLY CARETAKER TABLE 

        $sql34="INSERT INTO `elderly_caretaker`(`freelancerID`, `visitType`, `genderType`, `age_from`, `age_to`)
        VALUES ($freelancerID,'$elderlyCaretakerVisitType','$elderlyCaretakerGenderType',$elderlyCaretakerAgeFrom,$elderlyCaretakerAgeTo)";

        if(!mysqli_multi_query($conn, $sql34)){
            echo "Error: " . $sql34 . "<br>" . mysqli_error($conn);
        }


    }

    // **********************
    // GARDENER
    // **********************


    else if($add_category==16){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category16="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,16)";

        mysqli_query($conn, $sql_category16);
        
        //MANDATORY FIELDS 

        $gardenerVisitType=$_POST['gardenerVisitType'];

        //OPTIONAL FIELDS

        $gardenerAdditionalServices=" ";
        if(isset($_POST['gardenerAdditionalServices'])){
            $gardenerAdditionalServices=$_POST['gardenerAdditionalServices'];
        }

        $gardenerQualifications=" ";
        if(isset($_POST['gardenerQualifications'])){
            $gardenerQualifications=$_POST['gardenerQualifications'];
        }

        //INSERT QUERY FOR GARDENER TABLE 

        $sql35="INSERT INTO `gardener`(`freelancerID`, `visitType`, `additionalServices`, `qualifications`)
        VALUES ($freelancerID,'$gardenerVisitType','$gardenerAdditionalServices','$gardenerQualifications')";

        if(!mysqli_multi_query($conn, $sql35)){
            echo "Error: " . $sql35 . "<br>" . mysqli_error($conn);
        }
        else{
            echo "done 1";
        }          
    

        //INSERT QUERY FOR GARDENER SERVICES TABLE ------>>> FROM checkboxes
    
        $gardenerServices= json_decode($_POST['gardenerServices']);

        for ($x = 0; $x <= count($gardenerServices)-1; $x++){
            $sql36="INSERT INTO `gardener_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$gardenerServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql36)){
                echo "Error: " . $sql36 . "<br>" . mysqli_error($conn);
            }
            else{
                echo "done 2";
            }  
        }


    }

    // **********************
    // PHOTOGRAPHER
    // **********************


    else if($add_category==17){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category17="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,17)";

        mysqli_query($conn, $sql_category17);
        
        //OPTIONAL FIELDS

        $cameraModel=" ";
        if(isset($_POST['cameraModel'])){
            $cameraModel=$_POST['cameraModel'];
        }

        $photographerAdditionalEvents=" ";
        if(isset($_POST['photographerAdditionalEvents'])){
            $photographerAdditionalEvents=$_POST['photographerAdditionalEvents'];
        }

        $photographerQualifications=" ";
        if(isset($_POST['photographerQualifications'])){
            $photographerQualifications=$_POST['photographerQualifications'];
        }

        //INSERT QUERY FOR PHOTOGRAPHER TABLE 

        $sql37="INSERT INTO `photographer`(`freelancerID`, `cameraModels`, `additionalEvents`, `qualifications`)
        VALUES ($freelancerID,'$cameraModel','$photographerAdditionalEvents','$photographerQualifications')";

        if(!mysqli_multi_query($conn, $sql37)){
            echo "Error: " . $sql37 . "<br>" . mysqli_error($conn);
        }

        //INSERT QUERY FOR PHOTOGRAPHER EVENT TYPES TABLE ------>>> FROM checkboxes
    

        $photographerEventTypes= json_decode($_POST['photographerEventTypes']);

        for ($x = 0; $x <= count($photographerEventTypes)-1; $x++){
            $sql38="INSERT INTO `photographer_event_types`(`freelancerID`, `event`)
            VALUES ($freelancerID,'$photographerEventTypes[$x]')"; 

            if(!mysqli_multi_query($conn, $sql38)){
                echo "Error: " . $sql38 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // CARPENTER
    // **********************


    else if($add_category==18){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category18="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,18)";

        mysqli_query($conn, $sql_category18);
        
        //OPTIONAL FIELDS
    
        $carpenterAdditionalServices=" ";
        if(isset($_POST['carpenterAdditionalServices'])){
            $carpenterAdditionalServices=$_POST['carpenterAdditionalServices'];
        }

        //INSERT QUERY FOR CARPENTER TABLE 

        $sql39="INSERT INTO `carpenter`(`freelancerID`, `additionalServices`)
        VALUES ($freelancerID,'$carpenterAdditionalServices')";

        if(!mysqli_multi_query($conn, $sql39)){
            echo "Error: " . $sql39 . "<br>" . mysqli_error($conn);
        }
            
    

        //INSERT QUERY FOR CARPENTER SERVICES TABLE ------>>> FROM checkboxes
    

        $carpenterServices= json_decode($_POST['carpenterServices']);

        for ($x = 0; $x <= count($carpenterServices)-1; $x++){
            $sql40="INSERT INTO `carpenter_services`(`freelancerID`, `service`) 
            VALUES ($freelancerID,'$carpenterServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql40)){
                echo "Error: " . $sql40 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // PET CARETAKER
    // **********************


    else if($add_category==19){

        // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

        $sql_category19="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
        VALUES ($freelancerID,19)";

        mysqli_query($conn, $sql_category19);
        
        
        //OPTIONAL FIELDS

        $petCaretakerAdditionalPetTypes=" ";
        if(isset($_POST['petCaretakerAdditionalPetTypes'])){
            $petCaretakerAdditionalPetTypes=$_POST['petCaretakerAdditionalPetTypes'];
        }

        $petCaretakerAdditionalServices=" ";
        if(isset($_POST['petCaretakerAdditionalServices'])){
            $petCaretakerAdditionalServices=$_POST['petCaretakerAdditionalServices'];
        }

        //INSERT QUERY FOR PET CARETAKER TABLE 

        $sql41="INSERT INTO `pet_caretaker`(`freelancerID`, `additionalPets`, `additionalServices`)
        VALUES ($freelancerID,'$petCaretakerAdditionalPetTypes','$petCaretakerAdditionalServices')";

        if(!mysqli_multi_query($conn, $sql41)){
            echo "Error: " . $sql41 . "<br>" . mysqli_error($conn);
        }
            

        //INSERT QUERY FOR PET CARETAKER PET TYPES  TABLE ------>>> FROM checkboxes
    
        $petCaretakerPetTypes= json_decode($_POST['petCaretakerPetTypes']);

        for ($x = 0; $x <= count($petCaretakerPetTypes)-1; $x++){
            $sql42="INSERT INTO `pet_caretaker_pet_types`(`freelancerID`, `petType`)
            VALUES ($freelancerID,'$petCaretakerPetTypes[$x]')"; 

            if(!mysqli_multi_query($conn, $sql42)){
                echo "Error: " . $sql42 . "<br>" . mysqli_error($conn);
            }
        }

        //INSERT QUERY FOR PET CARETAKER SERVICES  TABLE ------>>> FROM checkboxes
    
        $petCaretakerServices= json_decode($_POST['petCaretakerServices']);

        for ($x = 0; $x <= count($petCaretakerServices)-1; $x++){
            $sql43="INSERT INTO `pet_caretaker_services`(`freelancerID`, `service`)
            VALUES ($freelancerID,'$petCaretakerServices[$x]')"; 

            if(!mysqli_multi_query($conn, $sql43)){
                echo "Error: " . $sql43 . "<br>" . mysqli_error($conn);
            }
        }


    }

    // **********************
    // BABYSITTER
    // **********************


    else if($add_category==20){

         // TO INSERT THE FREELANCER ID AND RESPECTIVE CATEGORY ID TO FREELANCER_HAS_CATEGORY TABLE

         $sql_category20="INSERT INTO `freelancer_has_category` (`freelancerID`,`categoryID`)
         VALUES ($freelancerID,20)";

         mysqli_query($conn, $sql_category20);
         
         //MANDATORY FIELDS 

         $babysitterVisitType=$_POST['babysitterVisitType'];
         $babysitterAgeFrom=$_POST['babysitterAgeFrom'];
         $babysitterAgeTo=$_POST['babysitterAgeTo'];   

         //INSERT QUERY FOR BEAUTICIAN TABLE 

         $sql44="INSERT INTO `babysitter`(`freelancerID`, `visitType`, `age_from`, `age_to`)
         VALUES ($freelancerID,'$babysitterVisitType','$babysitterAgeFrom','$babysitterAgeTo')";

         if(!mysqli_multi_query($conn, $sql44)){
             echo "Error: " . $sql44 . "<br>" . mysqli_error($conn);
         }


    }
   
   echo true;


?>