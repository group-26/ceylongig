<?php

    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');

    $id = $_SESSION['id'];

    $date=date('Y-m-d');

    //SELECT QUERIES FOR 5 TABS 

    $sql1= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish,
    `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='pending'" ;

    $sql2= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish, 
     `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='upcoming'" ;

    $sql3= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderStartDate, `customer_order`.orderExpectedFinish,
     `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `customer`.phone
    FROM customer_order
    INNER JOIN customer
    ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='ongoing'" ;

    $sql4= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderStartDate, `customer_order`.orderEndDate,
    `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName, `rating`.freelancerRating, `rating`.customerRating,`rating`.customerFeedback
    FROM ((customer_order
    INNER JOIN customer ON `customer_order`.customerID=`customer`.customerID)
    INNER JOIN rating ON `customer_order`.orderID = `rating`.orderID)
    WHERE `customer_order`.freelancerID=84 AND `customer_order`.orderStatus='finished'" ;

    $sql5= "SELECT `customer_order`.orderID, `customer_order`.orderName, `customer_order`.orderDescription,
    `customer_order`.orderReceivalDate, `customer_order`.customerID,
    `customer`.fName, `customer`.lName
    FROM customer_order
    INNER JOIN customer ON `customer_order`.customerID=`customer`.customerID
    WHERE `freelancerID`=84 AND `orderStatus`='rejected'" ;


    $result1 = mysqli_query($conn, $sql1);
    $result2 = mysqli_query($conn, $sql2);
    $result3 = mysqli_query($conn, $sql3);
    $result4 = mysqli_query($conn, $sql4); 
    $result5 = mysqli_query($conn, $sql5);




    $sql6="SELECT COUNT(*) AS ongoing_now
    FROM customer_order
    WHERE `freelancerID`=84 AND `orderStatus`='ongoing'";

    $result6 = mysqli_query($conn, $sql6);

    $sql7="SELECT COUNT(*) AS finished_total
    FROM customer_order
    WHERE `freelancerID`=84 AND `orderStatus`='finished'";

    $result7 = mysqli_query($conn, $sql7);


    $sql8="SELECT AVG(`freelancerRating`) AS average , COUNT(`freelancerRating`) AS total
    FROM rating
    WHERE freelancerID=84 AND freelancerRating!=0;";

    $result8 = mysqli_query($conn, $sql8);

    $sql9="SELECT COUNT(*) AS pending_total
    FROM customer_order
    WHERE `freelancerID`=84 AND `orderStatus`='pending'";

    $result9 = mysqli_query($conn, $sql9);

    $sql10="SELECT COUNT(*) AS upcoming_total
    FROM customer_order
    WHERE `freelancerID`=84 AND `orderStatus`='upcoming'";

    $result10 = mysqli_query($conn, $sql10);



    $sql11="SELECT COUNT(*) AS not_reviewed
    FROM rating
    WHERE `freelancerID`=84 AND `customerRating`=0";

    $result11 = mysqli_query($conn, $sql11);



    // SELECT QUERIES OVER 

    mysqli_close($conn);

?>