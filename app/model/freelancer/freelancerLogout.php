<?php

    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');

    session_destroy();

    header("Location: ../../view/login.php");

?>