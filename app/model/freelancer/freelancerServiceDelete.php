<?php

    include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app/model/config.php');

    $categoryIDs=json_decode( $_POST['categoryIDs']);


    $freelancerID=84;

    if(in_array(1, $categoryIDs)){

        $sql1 = "DELETE FROM freelancer_has_category
        WHERE categoryID=1 AND freelancerID=$freelancerID";

        $sql2 = "DELETE FROM beautician
        WHERE freelancerID=$freelancerID";

        $sql3 = "DELETE FROM beautician_services
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql1);
        mysqli_query($conn, $sql2);
        mysqli_query($conn, $sql3);

        

    }
    if(in_array(2,$categoryIDs)){

        $sql4 = "DELETE FROM freelancer_has_category
        WHERE categoryID=2 AND freelancerID=$freelancerID";

        $sql5 = "DELETE FROM plumber
        WHERE freelancerID=$freelancerID";

        $sql6 = "DELETE FROM plumber_services
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql4);
        mysqli_query($conn, $sql5);
        mysqli_query($conn, $sql6);

       

    }
    if(in_array(3,$categoryIDs)){

        $sql7 = "DELETE FROM freelancer_has_category
        WHERE categoryID=3 AND freelancerID=$freelancerID";

        $sql8 = "DELETE FROM painter
        WHERE freelancerID=$freelancerID";

        $sql9 = "DELETE FROM painter_services
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql7);
        mysqli_query($conn, $sql8);
        mysqli_query($conn, $sql9);
    }
    if(in_array(4,$categoryIDs)){

        $sql10 = "DELETE FROM freelancer_has_category
        WHERE categoryID=4 AND freelancerID=$freelancerID";

        $sql11 = "DELETE FROM cook
        WHERE freelancerID=$freelancerID";

        $sql12 = "DELETE FROM cook_cuisines
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql10);
        mysqli_query($conn, $sql11);
        mysqli_query($conn, $sql12);
    }
    if(in_array(5,$categoryIDs)){

        $sql13 = "DELETE FROM freelancer_has_category
        WHERE categoryID=5 AND freelancerID=$freelancerID";

        $sql14 = "DELETE FROM tailor
        WHERE freelancerID=$freelancerID";

        $sql15 = "DELETE FROM tailor_dress_types
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql13);
        mysqli_query($conn, $sql14);
        mysqli_query($conn, $sql15);
    }
    if(in_array(6,$categoryIDs)){

        $sql16 = "DELETE FROM freelancer_has_category
        WHERE categoryID=6 AND freelancerID=$freelancerID";

        $sql17 = "DELETE FROM event_organizer
        WHERE freelancerID=$freelancerID";

        $sql18 = "DELETE FROM event_organizer_event_types
        WHERE freelancerID=$freelancerID";

        $sql19 = "DELETE FROM event_organizer_services
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql16);
        mysqli_query($conn, $sql17);
        mysqli_query($conn, $sql18);
        mysqli_query($conn, $sql19);
    }
    if(in_array(7,$categoryIDs)){

        $sql20 = "DELETE FROM freelancer_has_category
        WHERE categoryID=7 AND freelancerID=$freelancerID";

        $sql21 = "DELETE FROM homefood_manufacturer
        WHERE freelancerID=$freelancerID";

        $sql22 = "DELETE FROM homefood_manufacturer_cuisines
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql20);
        mysqli_query($conn, $sql21);
        mysqli_query($conn, $sql22);
    }
    if(in_array(8,$categoryIDs)){

        $sql23 = "DELETE FROM freelancer_has_category
        WHERE categoryID=8 AND freelancerID=$freelancerID";

        $sql24 = "DELETE FROM driver
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql23);
        mysqli_query($conn, $sql24);
    }
    if(in_array(9,$categoryIDs)){

        $sql25 = "DELETE FROM freelancer_has_category
        WHERE categoryID=9 AND freelancerID=$freelancerID";

        $sql26 = "DELETE FROM programmer
        WHERE freelancerID=$freelancerID";

        $sql27 = "DELETE FROM programmer_development_type
        WHERE freelancerID=$freelancerID";

        $sql28 = "DELETE FROM programmer_frameworks
        WHERE freelancerID=$freelancerID";

        $sql29 = "DELETE FROM programmer_languages
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql25);
        mysqli_query($conn, $sql26);
        mysqli_query($conn, $sql27);
        mysqli_query($conn, $sql28);
        mysqli_query($conn, $sql29);
    }
    if(in_array(10,$categoryIDs)){

        $sql30 = "DELETE FROM freelancer_has_category
        WHERE categoryID=10 AND freelancerID=$freelancerID";

        $sql31 = "DELETE FROM mason
        WHERE freelancerID=$freelancerID";

        $sql32 = "DELETE FROM mason_services
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql30);
        mysqli_query($conn, $sql31);
        mysqli_query($conn, $sql32);
    }
    if(in_array(11,$categoryIDs)){

        $sql33 = "DELETE FROM freelancer_has_category
        WHERE categoryID=11 AND freelancerID=$freelancerID";

        $sql34 = "DELETE FROM graphic_designer
        WHERE freelancerID=$freelancerID";

        $sql35 = "DELETE FROM graphic_designer_design_type
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql33);
        mysqli_query($conn, $sql34);
        mysqli_query($conn, $sql35);
    }
    if(in_array(12,$categoryIDs)){

        $sql36 = "DELETE FROM freelancer_has_category
        WHERE categoryID=12 AND freelancerID=$freelancerID";

        $sql37 = "DELETE FROM mechanic
        WHERE freelancerID=$freelancerID";

        $sql38 = "DELETE FROM mechanic_services
        WHERE freelancerID=$freelancerID";

        $sql39 = "DELETE FROM mechanic_vehicle_brand
        WHERE freelancerID=$freelancerID";

        $sql40 = "DELETE FROM mechanic_vehicle_type
        WHERE freelancerID=$freelancerID";
        
        mysqli_query($conn, $sql36);
        mysqli_query($conn, $sql37);
        mysqli_query($conn, $sql38);
        mysqli_query($conn, $sql39);
        mysqli_query($conn, $sql40);
    }
    if(in_array(13,$categoryIDs)){

        $sql41 = "DELETE FROM freelancer_has_category
        WHERE categoryID=13 AND freelancerID=$freelancerID";

        $sql42 = "DELETE FROM content_writer
        WHERE freelancerID=$freelancerID";

        $sql43 = "DELETE FROM content_writer_languages
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql41);
        mysqli_query($conn, $sql42);
        mysqli_query($conn, $sql43);

    }
    if(in_array(14,$categoryIDs)){

        $sql44 = "DELETE FROM freelancer_has_category
        WHERE categoryID=14 AND freelancerID=$freelancerID";

        $sql45 = "DELETE FROM housekeeper
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql44);
        mysqli_query($conn, $sql45);


    }
    if(in_array(15,$categoryIDs)){

        $sql46 = "DELETE FROM freelancer_has_category
        WHERE categoryID=15 AND freelancerID=$freelancerID";

        $sql47 = "DELETE FROM elderly_caretaker
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql46);
        mysqli_query($conn, $sql47);


    }
    if(in_array(16,$categoryIDs)){

        $sql48 = "DELETE FROM freelancer_has_category
        WHERE categoryID=16 AND freelancerID=$freelancerID";

        $sql49 = "DELETE FROM gardener
        WHERE freelancerID=$freelancerID";

        $sql50 = "DELETE FROM gardener_services
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql48);
        mysqli_query($conn, $sql49);
        mysqli_query($conn, $sql50);

    }
    if(in_array(17,$categoryIDs)){

        $sql51 = "DELETE FROM freelancer_has_category
        WHERE categoryID=17 AND freelancerID=$freelancerID";

        $sql52 = "DELETE FROM photographer
        WHERE freelancerID=$freelancerID";

        $sql53 = "DELETE FROM photographer_event_types
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql51);
        mysqli_query($conn, $sql52);
        mysqli_query($conn, $sql53);

    }
    if(in_array(18,$categoryIDs)){

        $sql54 = "DELETE FROM freelancer_has_category
        WHERE categoryID=18 AND freelancerID=$freelancerID";

        $sql55 = "DELETE FROM carpenter
        WHERE freelancerID=$freelancerID";

        $sql56 = "DELETE FROM carpenter_services
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql54);
        mysqli_query($conn, $sql55);
        mysqli_query($conn, $sql56);

    }
    if(in_array(19,$categoryIDs)){

        $sql57 = "DELETE FROM freelancer_has_category
        WHERE categoryID=19 AND freelancerID=$freelancerID";

        $sql58 = "DELETE FROM pet_caretaker
        WHERE freelancerID=$freelancerID";

        $sql59 = "DELETE FROM pet_caretaker_pet_types
        WHERE freelancerID=$freelancerID";

        $sql60 = "DELETE FROM pet_caretaker_services
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql57);
        mysqli_query($conn, $sql58);
        mysqli_query($conn, $sql59);
        mysqli_query($conn, $sql60);

    }
    if(in_array(20,$categoryIDs)){

        $sql61 = "DELETE FROM freelancer_has_category
        WHERE categoryID=20 AND freelancerID=$freelancerID";

        $sql62 = "DELETE FROM babysitter
        WHERE freelancerID=$freelancerID";

        mysqli_query($conn, $sql61);
        mysqli_query($conn, $sql62);

    }



    echo true;


    

    

    // print(json_encode($result));

    mysqli_close($conn);

?>