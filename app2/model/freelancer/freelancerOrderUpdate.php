<?php

    include_once('../config.php');

    include_once('freelancerSessions.php');

    $id= $_SESSION['freelancerID'];
   
    $orderID=$_POST['orderID'];
    $customerID=$_POST['customerID'];
    $type=$_POST['type'];
    $customerRating=$_POST['customerRating'];
    $phone = 94 . $_POST['phone'];
    $price=$_POST['price'];
    $rejectedReason=$_POST['rejectedReason'];
    $email=$_POST['email'];
    
    // UPDATING THE ORDER STATUS OF CUSTOMER ORDER TABLE  

    $sql;


    if($type=="ongoing"){
        $date=date('Y-m-d');
        // echo $date;
        $sql="UPDATE `customer_order` SET orderStatus='ongoing', orderStartDate='$date' WHERE orderID=$orderID";



        //SEND SMS

        $url = 'http://send.ozonedesk.com/api/v2/send.php';
        $data=array("user_id"=>103673,
        "api_key"=>"i0zcdwgn8uyes80sw",
        "sender_id"=>"ozoneDEMO",
        "to"=>$phone,
        "message"=>"Your order $orderID has been started on $date");

        // use key 'http' even if you send the request to https://...

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }


        //SEND EMAIL

        $to_email = $email;
        $subject = "Ceylongig: Order Started";
        $body = "Your Order has been started. 
        \n Order ID: $orderID \n Price: $price \n Start Date: $date";
        // $headers = "From: Ceylongig";

        mail($to_email, $subject, $body);

    }
    else if($type=="finished"){
        $date=date('Y-m-d');

        $sql="UPDATE `customer_order` SET orderStatus='finished', orderEndDate='$date', paymentStatus='paid' WHERE orderID=$orderID";


        $sql2="INSERT INTO `rating` (`freelancerID`, `customerID`, `orderID`)
        VALUES ($id,$customerID,$orderID)";

        mysqli_multi_query($conn, $sql2);

        $url = 'http://send.ozonedesk.com/api/v2/send.php';
        $data=array("user_id"=>103673,
        "api_key"=>"i0zcdwgn8uyes80sw",
        "sender_id"=>"ozoneDEMO",
        "to"=>$phone,
        "message"=>"Your order $orderID has been finished on $date. You can now rate the freelancer and give feedback");

        // use key 'http' even if you send the request to https://...

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }

        //SEND EMAIL

        $to_email = $email;
        $subject = "Ceylongig: Order Finished";
        $body = "Your order has been finished. You can now rate the freelancer and give feedback. 
        \n Order ID: $orderID \n Price: $price \n End Date: $date";
        // $headers = "From: Ceylongig";

        mail($to_email, $subject, $body);

    }

    else if($type=="reviewing"){

        $sql="UPDATE `customer_order` SET orderStatus='reviewing', price=$price WHERE orderID=$orderID";


        //SEND SMS
        
        $url = 'http://send.ozonedesk.com/api/v2/send.php';
        $data=array("user_id"=>103673,
        "api_key"=>"i0zcdwgn8uyes80sw",
        "sender_id"=>"ozoneDEMO",
        "to"=>$phone,
        "message"=>"Your order $orderID has been accepted. Please review the price to proceed");

        // use key 'http' even if you send the request to https://...

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }



        //SEND EMAIL

        $to_email = $email;
        $subject = "Ceylongig: Order Accepted";
        $body = "Your Order has been accepted. Please review the price to proceed. 
        \n Order ID: $orderID \n Price: $price";
        // $headers = "From: Ceylongig";

        mail($to_email, $subject, $body);


    }
    else if($type=="rejected"){

        $sql="UPDATE `customer_order` SET orderStatus='rejected', rejectedReason='$rejectedReason', orderStartDate='$date', rejectedBy='freelancer' WHERE orderID=$orderID";

        $url = 'http://send.ozonedesk.com/api/v2/send.php';
        $data=array("user_id"=>103673,
        "api_key"=>"i0zcdwgn8uyes80sw",
        "sender_id"=>"ozoneDEMO",
        "to"=>$phone,
        "message"=>"Your order $orderID has been rejected by the freelancer due to $rejectedReason. Please submit another order");

        // use key 'http' even if you send the request to https://...

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }


        //SEND EMAIL

        $to_email = $email;
        $subject = "Ceylongig: Order Rejected";
        $body = "Your Order has been rejected. Please submit another order. 
        \n Order ID: $orderID";
        // $headers = "From: Ceylongig";

        mail($to_email, $subject, $body);


    }
    else{
        $sql="UPDATE `customer_order` SET orderStatus='$type' WHERE orderID=$orderID";
    }

    if(!mysqli_multi_query($conn, $sql)){

        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        
    }
    else{
        echo true;
        // header("Location: ../../view/pages/freelancer/freelancerOrderManagement.php");
    }


    



?>