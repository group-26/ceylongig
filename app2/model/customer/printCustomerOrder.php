<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app2/view/login.php');
  }
  if (($_SESSION['role']) !== "customer"){
    session_destroy();
    $_SESSION['msg'] = "You must log in as customer first";
    header('location: /ceylongig/app2/view/login.php');
  }
  $customerID = $_SESSION['customerID'];
  $orderID = $_GET['orderID'];
?>
<html>

    <head>
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/table.css">
        <link rel="icon" href="/ceylongig/app2/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/printCustomerOrder.css">
    </head>

    <body>
        <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app2/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'ongoing'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'
                            AND customer_order.orderID = '$orderID'";
                $result = mysqli_query($conn, $query);
                if(mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div id="openDetails'.$row["orderID"].'" class="">
                                <div>	
                                    
                                    <center>
                                    <br><br><br>
                                    <img src="/ceylongig/app2/view/assets/other/CeylonGig_Logo.svg" alt="CeylonGig Logo" width="20%" height="auto"><br><br>
                                        <h1>Order Details</h1><br>
                                        <p>Customer: '.$_SESSION["fName"].' '.$_SESSION["lName"].'</p><br><br>
                                    </center>
                                        <div class="table">
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order ID</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderID"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order Title</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderName"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order Description</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderDescription"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Handled by</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["fName"].' '.$row["lName"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Freelancer Address</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["address"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Contact Number</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["phone"].'</p>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                    
                                </div>
                            </div>';
                    }
                }
        ?>
        <script type="text/javascript">
            window.print();
        </script>
    </body>

</html>
