<?php 
	session_start();
	if(isset($_POST['logout'])) {
		$_SESSION['login'] = false;
        unset($_SESSION['login']);
        unset($_SESSION['orgID']);
		session_destroy();
	}
	if(isset($_SESSION['login']) && $_SESSION['login'] == true && $_SESSION['userType'] == 5) {
	
	} else {
		header("Location:../../pages/");
	}
?>
