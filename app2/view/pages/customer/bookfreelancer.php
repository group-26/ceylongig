<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app2/view/pages/login.php');
  }
  if (($_SESSION['role']) !== "customer"){
    session_destroy();
    $_SESSION['msg'] = "You must log in as customer first";
    header('location: /ceylongig/app2/view/pages/login.php');
  }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Book a Freelancer - CeylonGig</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/bookfreelancer.css">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/navbar.css">
        <link rel="icon" href="/ceylongig/app2/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/table.css">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/popupcard.css">
        <link rel="stylesheet" type="text/css" href="/ceylongig/app2/view/assets/css/customer/card.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script> 
            $(function(){
              $("#includedContent").load("/ceylongig/app2/view/pages/customer/customernavbar.php"); 
            });
        </script>
    </head>

    <body>
        <div id="includedContent" style="top:0;"></div>
        <br><br>
        <div id="msform">
            <form method="get" action="freelancersearchresult.php">
                <fieldset id="form1" style="margin: auto; width: fit-content; padding: 0px; padding-left: 8mm; padding-top: 10mm;">
                    
                    <h2>I want to hire a</h2><br><br>
                        <!-- ----------------- -->
                        <!-- BEAUTICIAN -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <button type="button" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                            <img src="../../assets/other/beautician.svg" alt="Beautician">
                            </div>
                            <p>Beautician</p>
                            </button>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="beautician"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- PLUMBER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                            <img src="../../assets/other/plumber.svg" alt="Plumber">
                            </div>
                            <p>Plumber</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="plumber"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- -------------------------------------- -->
                        <!-- PAINTER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/painter.svg" alt="Painter">
                            </div>
                            <p>Painter</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="painter"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- COOK -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/cook.svg" alt="Cook">
                            </div>
                            <p>Cook</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="cook"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- TAILOR -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/tailor.svg" alt="Tailor">
                            </div>
                            <p>Tailor</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="tailor"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- EVENT ORGANIZER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/event-organizer.svg" alt="EventOrganizer">
                            </div>
                            <p>Event Organizer</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="even_organizer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- HOMEFOOD MANUFACTURER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/homefood-manufacturer.svg" alt="HomefoodManufacturer">
                            </div>
                            <p>Homefood</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="homefood_manufacturer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- DRIVER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                
                                <img src="../../assets/other/driver.svg" alt="Driver">
                            </div>
                            <p>Driver</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="driver"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- PROGRAMMER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/programmer.svg" alt="Programmer">
                            </div>
                            <p>Programmer</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="programmer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- MASON -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/mason.svg" alt="Mason">
                            </div>
                            <p>Mason</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="mason"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- GRAPHIC DESIGNER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/graphic-designer.svg" alt="GraphicDesigner">
                            </div>
                            <p>Graphic Designer</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="graphic_designer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                        
                        <!-- --------------------------------- -->
                        <!-- MECHANIC -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/mechanic.svg" alt="Mechanic">
                            </div>
                            <p>Mechanic</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="mechanic"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- CONTENT WRITER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/content-writer.svg" alt="ContentWriter">
                            </div>
                            <p>Content Writer</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="content_writer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- HOUSEKEEPER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/housekeeper.svg" alt="Housekeeper">
                            </div>
                            <p>Housekeeper</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="housekeeper"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- ELDERLY CARETAKER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/elderly-caretaker.svg" alt="ElderlyCaretaker">
                            </div>
                            <p>Elderly Caretaker</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="elderly_caretaker"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- GARDENER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/gardener.svg" alt="Gardener">
                            </div>
                            <p>Gardener</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="gardener"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- PHOTOGRAPHER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/photographer.svg" alt="Photographer">
                            </div>
                            <p>Photographer</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="photographer"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- CARPENTER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/carpenter.svg" alt="Carpenter">
                            </div>
                            <p>Carpenter</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="carpenter"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- PET CARETAKER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/pet-caretaker.svg" alt="PetCaretaker">
                            </div>
                            <p>Pet Caretaker</p>
                            </a>
                            <label class="container">
                            <input
                                name="freelancerCategory"
                                type="radio"
                                value="pet_caretaker"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                
                        <!-- --------------------------------- -->
                        <!-- BABYSITTER -->
                        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
                            <a href="#" class="card category">
                            <div class="overlay"></div>
                            <div class="circle">
                                <img src="../../assets/other/babysitter.svg" alt="Babysitter">
                            </div>
                            <p>Babysitter</p>
                            </a>
                            <label class="container">
                                <input
                                name="freelancerCategory"
                                type="radio"
                                value="babysitter"
                            />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                    <!--
                    <select>
                        <option class="default" value="" selected disabled>Select a Freelancer</option>
                        <option value="">Beautician</option>
                        <option value="">Plumber</option>
                        <option value="">Painter</option>
                        <option value="">Cook</option>
                        <option value="">Tailor</option>
                        <option value="">Event Organizer</option>
                        <option value="">Homemade Food Maker</option>
                        <option value="">Driver</option>
                        <option value="">Programmer</option>
                        <option value="">Mason</option>
                        <option value="">Graphic Designer</option>
                        <option value="">Mechanic</option>
                        <option value="">Content Writer</option>
                        <option value="">Housekeeper</option>
                        <option value="">Pet Caretaker</option>
                        <option value="">Elderly Caretaker</option>
                        <option value="">Babysitter</option>
                        <option value="">Gardener</option>
                        <option value="">Interior Designer</option>
                        <option value="">Photographer</option>
                        <option value="">Private Tutor</option>
                        <option value="">Carpenter</option>
                    </select>
                    -->
                    <input
                        type="button"
                        name="next1"
                        onmouseup="enableForm2()"
                        class="next action-button"
                        value="Next"
                    />
                   <br><br>
                </fieldset>

                <fieldset id="form2" style="margin: auto; width: fit-content;">
                    <div>
                        <h3>To:</h3>
                        <select name="customerLocation">
                            <option class="default" value="" selected disabled>Select your location</option>
                            <option value="Colombo 01">Colombo 01</option>
                            <option value="Colombo 02">Colombo 02</option>
                            <option value="Colombo 03">Colombo 03</option>
                            <option value="Colombo 04">Colombo 04</option>
                            <option value="Colombo 05">Colombo 05</option>
                            <option value="Colombo 06">Colombo 06</option>
                            <option value="Colombo 07">Colombo 07</option>
                            <option value="Colombo 08">Colombo 08</option>
                            <option value="Colombo 09">Colombo 09</option>
                            <option value="Colombo 10">Colombo 10</option>
                            <option value="Colombo 11">Colombo 11</option>
                            <option value="Colombo 12">Colombo 12</option>
                            <option value="Colombo 13">Colombo 13</option>
                            <option value="Colombo 14">Colombo 14</option>
                            <option value="Colombo 15">Colombo 15</option>
                        </select>
                        <input
                            type="button"
                            onmouseup="enableForm3()"
                            name="next2"
                            class="next action-button"
                            value="Next"
                        />
                    </div>
                </fieldset>
                <fieldset id="form3" style="display: none; margin: auto; width: fit-content;">
                    <div>
                        <h3>On</h3><br>
                        <input type="date" id="datefield" name="dateOfBooking" min=><br><br><br>
                        <input type="hidden" id="btnClickedValue" name="btnClickedValue" value="" />
                        <input type="submit" class="button2" value="Find Freelancers" onclick="sendDay()">
                    </div>
                </fieldset>
            </form>
        </div>
        
        <script>
            function enableForm2(){
                document.getElementById("form2").style.display = "block";
                document.getElementById("form1").style.display = "none";
            }
            function enableForm3(){
                document.getElementById("form3").style.display = "block";
                document.getElementById("form2").style.display = "none";
            }


            var today = new Date();
            var dd = today.getDate()+1;
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            tomorrow = yyyy+'-'+mm+'-'+dd;
            document.getElementById("datefield").setAttribute("min", tomorrow);
            
            function sendDay(){
                var date = new Date(document.getElementById("datefield").value);
                var days = ['sun','mon','tue','wed','thu','fri','sat'];
                var day = days[ date.getDay() ];
                document.getElementById("btnClickedValue").value = day;
            }
        </script>
            
    </body>
</html>