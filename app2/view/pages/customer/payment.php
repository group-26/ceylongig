<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app2/view/pages/login.php');
  }
  if (($_SESSION['role']) !== "customer"){
    session_destroy();
    $_SESSION['msg'] = "You must log in as customer first";
    header('location: /ceylongig/app2/view/pages/login.php');
  }
  $customerID = $_SESSION['customerID'];
  $orderID = $_GET['orderID'];
?>
<html>

    <head>
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/table.css">
        <link rel="icon" href="/ceylongig/app2/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/payment.css">
    </head>

    <body>
        <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app2/model/config.php');
                $query =   "SELECT DISTINCT *
                            FROM freelancer, customer_order
                            WHERE customer_order.orderStatus = 'reviewing'
                            AND customer_order.freelancerID = freelancer.freelancerID
                            AND customer_order.customerID = '$customerID'
                            AND customer_order.orderID = '$orderID'";
                $result = mysqli_query($conn, $query);
                if(mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                            <div id="openDetails'.$row["orderID"].'" class="">
                                <div>	
                                    
                                    <center>
                                    <br><br><br>
                                    <img src="/ceylongig/app2/view/assets/other/CeylonGig_Logo.svg" alt="CeylonGig Logo" width="20%" height="auto"><br><br>
                                        <h1>Order Summary</h1><br>
                                        <p>Customer: '.$_SESSION["fName"].' '.$_SESSION["lName"].'</p><br><br>
                                    </center>
                                        <div class="table">
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order ID</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderID"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order Title</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderName"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Order Description</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["orderDescription"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Handled by</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["fName"].' '.$row["lName"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Freelancer Address</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["address"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Contact Number</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p>'.$row["phone"].'</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="reportcell">
                                                    <p style="font-weight:bold;">Quoted Price</p>
                                                </div>
                                                <div class="reportcell">
                                                    <p style="font-weight: bold;">LKR '.$row["price"].'</p>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        
                                        <center>
                                            <form method="post" action="https://sandbox.payhere.lk/pay/checkout">   
                                                <input type="hidden" name="merchant_id" value="1217031">
                                                <input type="hidden" name="return_url" value="http://localhost/ceylongig/app2/model/customer/changeOrderReviewStatus.php?orderID='.$row["orderID"].'&orderStatus=upcoming&paymentType=online&paymentStatus=paid&phone='.$row["phone"].'&email='.$row["freelancerEmail"].'">
                                                <input type="hidden" name="cancel_url" value="http://sample.com/cancel">
                                                <input type="hidden" name="notify_url" value="http://sample.com/notify">  
                                                <!--Item Details-->
                                                <input type="hidden" name="order_id" value="'.$orderID.'">
                                                <input type="hidden" name="items" value="'.$row["orderName"].'"><br>
                                                <input type="hidden" name="currency" value="LKR">
                                                <input type="hidden" name="amount" value="'.$row["price"].'">  
                                                <!--Customer Details-->
                                                <input type="hidden" name="first_name" value="'.$_SESSION["fName"].'">
                                                <input type="hidden" name="last_name" value="'.$_SESSION["lName"].'"><br>
                                                <input type="hidden" name="email" value="'.$_SESSION["email"].'">
                                                <input type="hidden" name="phone" value="'.$_SESSION["phone"].'"><br>
                                                <input type="hidden" name="address" value="'.$_SESSION["address"].'">
                                                <input type="hidden" name="city" value="Colombo">
                                                <input type="hidden" name="country" value="Sri Lanka"> 
                                                <a class="button1" href="/ceylongig/app2/model/customer/changeOrderReviewStatus.php?orderID='.$row["orderID"].'&orderStatus=upcoming&paymentType=cash&phone='.$row["phone"].'&email='.$row["freelancerEmail"].'">
                                                    Pay&nbsp;By&nbsp;Cash&nbsp;After&nbsp;Completion
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="button1" type="submit" value="Pay&nbsp;Online&nbsp;Now">   
                                            </form>
                                            <!--
                                            <div class="">
                                                <a class="button1" href="/ceylongig/app2/model/customer/changeOrderReviewStatus.php?orderID='.$row["orderID"].'&orderStatus=upcoming&paymentType=cash">
                                                    Pay&nbsp;By&nbsp;Cash&nbsp;After&nbsp;Completion
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a class="button1" href="/ceylongig/app2/model/customer/printCustomerOrder.php?orderID='.$orderID.'" target="_blank">
                                                    Pay&nbsp;Online&nbsp;Now
                                                </a>
                                            </div>
                                            -->
                                        </center>
                                </div>
                            </div>';
                    }
                }
        ?>
        <script type="text/javascript">
            
        </script>
    </body>

</html>
