<?php 
  session_start(); 

    if (!isset($_SESSION['role'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /ceylongig/app2/view/pages/login.php');
    }
    $ID = $_GET['freelancerID'];
    $categoryID = $_GET['categoryID'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/orderplacement.css">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/navbar.css">
        <link rel="icon" href="/ceylongig/app2/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/popupcard.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <title>Place Order - CeylonGig</title>
        <script> 
            $(function(){
              $("#includedContent").load("customernavbar.php"); 
            });
        </script>
    </head>

    <body>
        <div id="includedContent" style="position:fixed; top:0;"></div>
        <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
                <h3>
                <?php 
                    echo $_SESSION['success']; 
                    unset($_SESSION['success']);
                ?>
                </h3>
            </div>
        <?php endif ?>

        <form class="orderform" method="post" action="/ceylongig/app2/model/customer/requestFreelancer.php?freelancerID=<?php echo $ID; ?>">
            <h2>Let's describe your order</h2>
            <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . '/ceylongig/app2/model/config.php');
                $query= "SELECT * FROM freelancer
                        WHERE freelancer.freelancerID = '$ID'";
                $result = mysqli_query($conn, $query);
                if (mysqli_num_rows($result) > 0) { 
                    while ($row = mysqli_fetch_assoc($result)) {
                        $freelancerName = $row['fName'];
                        $freelancerPhone = $row['phone'];
                        $freelancerEmail = $row['freelancerEmail'];
                    }
                } 
            ?>
            <h5>to help <?php echo $freelancerName; ?> get your work done.</h5><br>
            <h4>Give a title that best describes your order: </h4><br>
            <input type="text" name="orderName"><br><br>
            <h4>Describe your order: </h4><br>
            <textarea rows="5" cols="80" name="orderDescription"></textarea><br><br>
            <!--<h4>If you have any document to describe your order, please share them here: </h4><br>
            <input type="file"><br><br>-->
            <h4>When do you want the order to commence?</h4><br>
            <input type="date" id="startDate" name="startDate" onchange="expectedFinishDate(event)" min=><br><br>
            <h4>When would you like to get it done by latest?</h4><br>
            <input type="date" id="expectedFinish" name="expectedFinish" min=><br><br>
            <input type="hidden" name="freelancerEmail" value="<?php echo '$freelancerEmail'; ?>">
            <input type="hidden" name="freelancerPhone" value="<?php echo '$freelancerPhone'; ?>">
            <input type="submit" value="Place the Order" class="submitbutton">
        </form>
        <script>
            var today = new Date();
            var dd = today.getDate()+1;
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            tomorrow = yyyy+'-'+mm+'-'+dd;
            document.getElementById("startDate").setAttribute("min", tomorrow);
            document.getElementById("expectedFinish").setAttribute("min", tomorrow);
            function expectedFinishDate(event){

                document.getElementById("expectedFinish").setAttribute("min", event.target.value);
            }
            
        </script>
    </body>
</html> 