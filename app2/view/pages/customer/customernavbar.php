<?php 
  session_start(); 

  if (!isset($_SESSION['role'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: /ceylongig/app2/view/pages/login.php');
  }
  if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['email']);
      unset($_SESSION['phone']);
      unset($_SESSION['fName']);
      unset($_SESSION['lName']);
      unset($_SESSION['role']);
  	header("location: /ceylongig/app2/view/pages/login.php");
  }
?>
<!DOCTYPE html>

<head>
    <title>NavBar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/navbar.css">
    <link rel="icon" href="/ceylongig/app2/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/table.css">
    <link rel="stylesheet" href="/ceylongig/app2/view/assets/css/customer/popupcard.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link rel="stylesheet" href="/app2/view/assets/css/navbar.css">  <!--Copy and Paste this within Head Tag-->
</head>

<body>

    <!--copy this whole thing (nav and script) on top of your body-->
    <nav>
        <div>
            <a href="/ceylongig/app2/view/pages/customer/customerdashboard.php"><img src="/ceylongig/app2/view/assets/other/CeylonGig_Logo.svg" alt="CeylonGig Logo"></a>
        </div>
        <ul class="nav-links">
            <li><a href="#includedContent2">About</a></li>
            <li><a href="./bookfreelancer.php">Book&nbsp;a&nbsp;Freelancer</a>&nbsp;&nbsp;&nbsp;</li>
            <li>Welcome <a href="./customerprofile.php"><?php echo $_SESSION['fName']; ?>!</a></li>
            <li><a href="/ceylongig/app2/model/customer/logout.php" class="log-in" style="color: #fbfbfb;">Logout</a></li>
        </ul>
        <div class="burger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
        </div>
    </nav>

    <script src="/app2/view/assets/js/navbar.js"></script>

    <!--
    <div class="modalDialog" id="chooseSignUp">
        <div>	
            <a href="#close" title="Close" class="close"><h1>×</h1></a>
            <center>
                <br><br>
                <h1>Sign up as</h1><br><br>

                <div class="container">
                    <a class="card1" href="/ceylongig/app/view/pages/freelancer/freelancerSignup.php"><br>
                        <img class="off" src="/ceylongig/app/view/assets/img/freelancerLogo.svg" height="60px" width="auto">
                        <img class="on" src="/ceylongig/app/view/assets/img/freelancerLogoHover.svg" height="60px" width="auto">
                        <br><br>
                        <h3>Freelancer</h3>
                        <div class="go-corner" href="#">
                          <div class="go-arrow">
                            
                          </div>
                        </div>
                    </a>
                    <a class="card1" href="/ceylongig/app/view/pages/customer/customersignup.php"><br>
                        <img class="off" src="/ceylongig/app/view/assets/img/customerLogo.svg" height="60px" width="auto">
                        <img class="on" src="/ceylongig/app/view/assets/img/customerLogoHover.svg" height="60px" width="auto">
                        <br><br>
                        <h3>Customer</h3>
                        <div class="go-corner" href="#">
                          <div class="go-arrow">
                            
                          </div>
                        </div>
                    </a>
                </div>

                <br>
            </center>
            
        </div>
    </div>
    -->
</body>

</html>