<?php
    session_start();
    
    // initializing variables
    $fName= "";
    $lName= "";
    $NIC= "";
    $phone= "";
    $gender= isset( $_POST['gender'] )  ? $_POST['gender'] : FALSE;;
    $email= "";
    $errors = array(); 
    
    // connect to the database
    $conn = mysqli_connect('database-1.cqtvhyjnw4qq.us-east-2.rds.amazonaws.com', 'admin', 'admin123', 'ceylongig', '3306');
    
    // REGISTER USER
    if (isset($_POST['reg_customer'])) {
      // receive all input values from the form
      $fName = mysqli_real_escape_string($conn, $_POST['fName']);
      $lName = mysqli_real_escape_string($conn, $_POST['lName']);
      $NIC = mysqli_real_escape_string($conn, $_POST['NIC']);
      $phone = mysqli_real_escape_string($conn, $_POST['phone']);
      if(!empty($_POST['gender'])) { $gender = mysqli_real_escape_string($conn, $_POST['gender']); }
      $email = mysqli_real_escape_string($conn, $_POST['email']);
      $password_1 = mysqli_real_escape_string($conn, $_POST['PasswordEnter1']);
      $password_2 = mysqli_real_escape_string($conn, $_POST['PasswordEnter2']);

      if (empty($fName)) { array_push($errors, "Firstname is required"); }
      if (empty($lName)) { array_push($errors, "Lastname is required"); }
      if (strlen($NIC) > 12 ||  strlen($NIC) < 10) { 
        if (empty($NIC)) { array_push($errors, "NIC Number is required"); }
        else{array_push($errors, "NIC Number should consist of 10 to 12 characters"); }
        }
      if (empty($email)) { array_push($errors, "Email is required"); }
      if (empty($gender)) { array_push($errors, "Gender is required"); }
      if (empty($phone)) { array_push($errors, "Phone Number is required"); }
      if (empty($password_1)) { array_push($errors, "Password is required"); }
      if ($password_1 != $password_2) {
        array_push($errors, "The passwords do not match");
      }
    
      // first check the database to make sure 
      // a customer does not already exist with the same username and/or email
      $customer_check_query = "SELECT * FROM ceylongig.customer WHERE NIC='$NIC' OR email='$email' LIMIT 1";
      $result = mysqli_query($conn, $customer_check_query);
      $customer = mysqli_fetch_assoc($result);
      
      if ($customer) { // if user exists
        if ($customer['NIC'] === $NIC) {
          array_push($errors, "A customer account with the same NIC already exists");
        }
    
        if ($customer['email'] === $email) {
          array_push($errors, "A cuctomer account with the same Email already exists");
        }
      }

      // then, check the database to make sure 
      // a freelancer, admin, staff or an advertiser does not already exist with the same username and/or email
      $loginuser_check_query = "SELECT * FROM ceylongig.customer WHERE email='$email' LIMIT 1";
      $result2 = mysqli_query($conn, $loginuser_check_query);
      $customer2 = mysqli_fetch_assoc($result2);

      if ($customer2) { // if user exists
        if ($customer2['email'] === $email) {
          array_push($errors, "A CeylonGig account with the same Email already exists");
        }
      }
    
      // Finally, register user if there are no errors in the form
      if (count($errors) == 0) {
          $password = md5($password_1);//encrypt the password before saving in the database
    
          $query = "INSERT INTO ceylongig.customer (fName, lName, NIC, phone, gender, email, password, status) 
                    VALUES('$fName', '$lName', '$NIC', '$phone', '$gender', '$email', '$password', 'active')";
          mysqli_query($conn, $query);
          $query = "INSERT INTO ceylongig.loginuser (email, password, role) 
                    VALUES('$email', '$password', 'customer')";
          mysqli_query($conn, $query);

            $query =    "SELECT ceylongig.customer.customerID
                        FROM ceylongig.customer
                        WHERE customer.email = '$email'";
            $result5 = mysqli_query($conn, $query);
            
            if(mysqli_num_rows($result5) > 0){
                while ($row = mysqli_fetch_assoc($result5)){
                    $_SESSION['customerID'] = $row["customerID"];
                }
            }
            $_SESSION['email'] = $email;
            $_SESSION['phone'] = $phone;
            $_SESSION['fName'] = $fName;
            $_SESSION['lName'] = $lName;
            $_SESSION['NIC'] = $NIC;
            $_SESSION['address'] = null;
            $_SESSION['dOB'] = null;
            $_SESSION['profilePicture'] = null;
            $_SESSION['gender'] = $gender;
            $_SESSION['role'] = 'customer';
            $_SESSION['success'] = "You are now logged in";
            header('location: /ceylongig/app2/view/pages/customer/customerdashboard.php');
      }
    }
    
?>

<!DOCTYPE html>

<html>
<head>
    <title>Customer Sign Up - CeylonGig</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/customer/customersignup.css">
    <link rel="stylesheet" href="../../assets/css/navbar.css">
    <link rel="stylesheet" href="../../assets/css/customer/popupcard.css">
    <link rel="icon" href="../../assets/img/icon_circle.png" type="image/png">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
        $(function(){
          $("#includedContent").load("../../components/navbar.html"); 
        });
    </script>
</head>

<body>
    <div id="includedContent"></div>

    <form class="signupform" method="post" action="customersignup.php">
        
        <h2>Let us know about you!</h2>
        <div>
        <?php  if (count($errors) > 0) : ?>
            <div class="error">
                <?php foreach ($errors as $error) : ?>
                    <p><?php echo $error ?></p>
                <?php endforeach ?>
            </div><br><br>
        <?php  endif ?>
        </div>
        <table class="table">
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <input type="text" name="fName" value="<?php echo $fName; ?>">
                        <span data-placeholder="First Name"></span>
                    </div>
                </td>

                <td colspan="2">
                    <div class="textbox">
                        <input type="text" name="lName" value="<?php echo $lName; ?>">
                        <span data-placeholder="Last Name"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <input type="text" name="NIC" value="<?php echo $NIC; ?>">
                        <span data-placeholder="NIC Number"></span>
                    </div>
                </td>
                <td colspan="2">
                    <div class="textbox">
                    <input type="text" disabled>
                        <span data-placeholder="Gender (Choose from below)"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <input type="text" name="phone" pattern="[0]{1}[0-9]{9}" title="Valid Phone Number should contain 10 digits, and must start with zero" value="<?php echo $phone; ?>">
                        <span data-placeholder="phone"></span>
                    </div>
                </td>
                <td colspan="2" class="light">
                    <label>
                        <input class="radiobutton" type="radio" name="gender" value="Female">
                        <span class="design"></span>
                        <span class="text">Female</span>
                    </label>
                </td>
                
            </tr>
            <tr> 
                <td colspan="2">
                    <div class="textbox">
                        <input type="email" name="email" value="<?php echo $email; ?>">
                        <span data-placeholder="E-mail Address"></span>
                    </div>
                </td>
                <td colspan="2" class="light">  
                    <label>
                        <input class="radiobutton" type="radio" name="gender" value="Male">
                        <span class="design"></span>
                        <span class="text">Male</span>
                    </label>
                </td>
                
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <input type="password" name="PasswordEnter1" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                        <span data-placeholder="Password"></span>
                    </div>
                </td>
                <td colspan="2" class="light">  
                    <label>
                        <input class="radiobutton" type="radio" name="gender" value="Other">
                        <span class="design"></span>
                        <span class="text">Other</span>
                    </label>
                </td> 
                
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <input type="password" name="PasswordEnter2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                        <span data-placeholder="Confirm Password"></span>
                    </div>
                </td>
                <td colspan="2" class="light">
                    <label>
                        <input class="radiobutton" type="radio" name="gender" value="Prefer not to say">
                        <span class="design"></span>
                        <span class="text">Prefer not to say</span>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <input class="signupbutton" type="submit" name="reg_customer" value="Sign Up">
                </td>
            </tr>
        </table>    

        <div class="bottomtext">
            Already have an account? <a href="/ceylongig/app2/view/pages/login.php">Log in!</a>
        </div>
       
    </form>

    <script type="text/javascript">
        $(".textbox input").on("focus",function(){
            $(this).addClass("focus");
        });

        $(".textbox input").on("blur",function(){
            if($(this).val() == "")
            $(this).removeClass("focus");
        });
    </script>

</body>

</html>