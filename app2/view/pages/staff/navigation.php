
<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, intial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
     <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="../../assets/css/staff/navigation.css">

        <title>Admin panel</title>
    </head>

    <body>
        
        
       
      
       <!-- side bar menus-->
       <input type="checkbox" id="check">
            <label for="check">
                <i class="fas fa-bars" id="btn"></i>
                <i class="fas fa-times" id="cancel"></i>

            </label>
            
       <div class="admin-wrapper">
            <!--left side bar-->
           
            <div class="left-sidebar">
               
              
                <p>
                    Staff panel
                </p>
                <!--using i classes-->
                <ul>
                    <li><a href="customer_manage.php"><i class="fas fa-user-circle"></i>Customer Manage</a></li>
                    <li><a href="freelancer_manage.php"><i class="fas fa-id-badge"></i>Freaalancer Manage </a></li>
                    <li><a href="advertise_manage.php"><i class="fas fa-audio-description"></i>Advertiser Manage</a></li>
                    <li><a href="order_manage.php"><i class="fas fa-th-list"></i>Order Manage</a></li>
                    <li><a href="complaint_manage.php"><i class="fas fa-copy"></i>Complaint Manage</a></li>
                    <li><a href="category_manage.php"><i class="fas fa-sliders-h"></i>Category Manage</a></li>
                    
                </ul>
                
            </div>
       
        </div>   


    </body>

</html>
     