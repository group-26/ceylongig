<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php

include ('../../../model/staff/view_complaint.php');

?>


<html>
    <heaad>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" href="../../assets/css/staff/view_complaint.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script> 
    $(function(){
    $("#includedContent").load("header.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent1").load("footer.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent2").load("navigation.php"); 
    });
</script> 



    </head>
    <body>
    <header>

<div id="includedContent"></div>


</header>

<div id="includedContent2"></div>
        <div class="container">
            <div class="header">
                <h2>View Complaint</h2>
            </div>

            
            <form id="form" class="form" action="#" method="POST">

            <div class="form-control">
                    <label for="username">Complaint ID</label>
                    <input type="text" value="<?php echo $row['complaintID']; ?>" id="name" name="name" readonly/>
                  
                </div>

                <div class="form-control">
                    <label for="username">Order ID</label>
                    <input type="text" value="<?php echo $row['orderID']; ?>" id="name" name="name" readonly/>
                  
                </div>
          
                <div class="form-control">
                    <label for="username">Complaint Name</label>
                    <input type="text"  value="<?php echo $row['complaintName']; ?>" id="cid" name="categoryid" readonly/>
                   
                </div>

                <div class="form-control">
                    <label for="username">Complaint</label>
                    <input type="text"  value="<?php echo $row['complaintDescription']; ?>" id="cid" name="categoryid" readonly/>
                   
                </div>
                <div class = "links">
                    <div class="first">
                <?php echo "<a href =inform_final_decision.php?complaintID=".$row['complaintID']."> Inform Final Decision </a>"?> 
                    </div>
                </div>
                <div class= "links">
            <a href="complaint_details.php">View List</a><br>
            </div> 
            </form>
        </div>
     
        <script src="../../assets/js/view_complaint.js"></script>
      
        <footer>

<div id="includedContent1"></div>

</footer>


    </body>
</html>
