

<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php

include ('../../../model/staff/view_order.php');

?>




<!DOCTYPE html>

<html>

<head>

    <link rel="stylesheet" type="text/css" href="../../assets/css/staff/view_order.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
    $(function(){
      $("#includedContent").load("header.php"); 
    });
    </script> 

<script> 
    $(function(){
      $("#includedContent1").load('footer.php'); 
    });
    </script> 

<script> 
    $(function(){
      $("#includedContent2").load('navigation.php'); 
    });
    </script> 


</head>

<body>

    <header>
    <div id="includedContent"></div>

    </header>

    <!-- Siddebar-->
    <div id="includedContent2"></div>


    <table>
        
    <tr>

      <th colspan=11><h2>Order Details</h2></th>

    <tr> 
      
        <tr>
            <th>Order ID</th>
            <th>Order Name</th>
            <th>Freelancer ID</th>
            <th>Customer ID</th>
            <th>Order Description</th>
            <th>Price</th>
            <th>Order Status</th>
            <th>Payment Method</th>
            <th>View Full details</th>
           
            
            
        </tr>

      
        <?php
				while($row=mysqli_fetch_assoc($result)){

			?>
      
      <tr>
        <td><?php echo $row['orderID'] ?></td>
        <td><?php echo $row['freelancerID'] ?></td>
        <td><?php echo $row['customerID'] ?></td>
				<td><?php echo $row['orderName'] ?></td>
				<td><?php echo $row['orderDescription'] ?></td>
        <td><?php echo $row['price'] ?></td>
        <td><?php echo $row['orderStatus'] ?></td>
        <td><?php echo $row['paymentType'] ?></td>
   
       
        <?php echo "<td><a href =view_full_order_details.php?orderID='".$row['orderID']."' > View </a> </td>"?>
        

                

                
				
        </tr>
        
       
       
       
        <?php
    }
    
  
    ?>

         
     
 
    </table>

            
<footer>
<div id="includedContent1"></div>
</footer>



</body>

</html>