<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php 
include ('../../../model/staff/update_advertiser_form.php');

?>





<html>
    <heaad>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" href="../../assets/css/staff/update_advertiser_form.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script> 
    $(function(){
    $("#includedContent").load("header.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent1").load("footer.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent2").load("navigation.php"); 
    });
</script> 



    </head>
    <body>
    <header>

<div id="includedContent"></div>


</header>

<div id="includedContent2"></div>
        <div class="container">
            <div class="header">
                <h2>Advertiser Update</h2>
            </div>

            
            <form id="form" class="form" action="#" method="POST">

          
                <div class="form-control">
                    <label for="username">Company Name</label>
                    <input type="text"value="<?php echo $row['companyName']; ?>" id="companyName" name="companyName" />
                  
                </div>
                <div class="form-control">
                    <label for="username">Advertiser Id</label>
                    <input type="text" value="<?php echo $row['advertiserID']; ?>" id="advertiserID" name="advertiserID" readonly />
                   
                </div>
              

                <div class="form-control">
                    <label for="username">Email</label>
                    <input type="email" value="<?php echo $row['email']; ?>" id="email" name="email" readonly/>
                  
                </div>

                <div class="form-control">
                    <label for="username">Phone Number</label>
                    <input type="text"value="<?php echo $row['phone']; ?>" id="phone" name="phone" />
                   
                </div>

                <div class="form-control">
                    <label for="username">Register Number</label>
                    <input type="text" value="<?php echo $row['registrationNumber']; ?>" id="registrationNumber" name="registrationNumber" readonly/>
                   
                </div>

                <div class="form-control">
                    <label for="username">Profile Picture</label>
                    <input type="text" value="<?php echo $row['profilePicture']; ?>" id="profilePicture" name="profilePicture" />
                   
                </div>

              
              

              
          

            <button type="submit" name="register" >Check and Update</button><br> 
            
            </form>
        </div>
     
      
      
        <footer>

<div id="includedContent1"></div>

</footer>


    </body>
</html>
