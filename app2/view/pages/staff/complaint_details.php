
<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>

<?php

include ('../../../model/staff/complaint_details.php');

?>




<!DOCTYPE html>

<html>

<head>


<link rel="stylesheet" href="../../assets/css/staff/complaint_details.css" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
    $(function(){
      $("#includedContent").load("header.php"); 
    });
    </script> 

<script> 
    $(function(){
      $("#includedContent1").load('footer.php'); 
    });
    </script> 

<script> 
    $(function(){
      $("#includedContent2").load('navigation.php'); 
    });
    </script> 



</head>

<body>

    <header>
    <div id="includedContent"></div>
    </header>

     <!-- Siddebar-->
     <div id="includedContent2"></div>

    <table>
        
        <tr>

           <th colspan=8><h2>Complaint Details</h2></th>

        <tr> 
      
      <tr>
          <th>Complaint ID</th>
          <th>Complainer's Email</th>
          <th>Complaint Name</th>
          <th>Complaint Date</th>
          <th>Role of the complainer</th>
          <th>Complaint Status</th>
          <th>View Complaint</th>
          
          
          
      </tr>

      <?php
				while($row=mysqli_fetch_assoc($result)){

			?>
      
      <tr>
        <td><?php echo $row['complaintID'] ?></td>
				<td><?php echo $row['complaintEmail'] ?></td>
        <td><?php echo $row['complaintName'] ?></td>
        <td><?php echo $row['complaintDate'] ?></td>
        <td><?php echo $row['complaintBy'] ?></td>
        <td><?php echo $row['complaintActionStatus'] ?></td>
        
       

        <?php echo "<td><a href =view_complaint.php?complaintID='".$row['complaintID']."' > View Complaint </a> </td>"?>
        

                
				
        </tr>
        
       
       
       
        <?php
    }
    
  
    ?>

  
 
    </table>


    <footer>
    <div id="includedContent1"></div>
  
  </footer>
  

</body>

</html>