

<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>


<?php 
include ('../../../model/staff/current_freelancer_form.php');

?>



<html>
    <heaad>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" href="../../assets/css/staff/current_freelancer_form.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>

<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script> 
    $(function(){
    $("#includedContent").load("header.php"); 
    });
</script> 

<script> 
    $(function(){
    $("#includedContent1").load("footer.php"); 
    });
</script> 


    </head>
    <body>
    <header>

<div id="includedContent"></div>

</header>

        <div class="container">
            <div class="header">
                <h2>User Update</h2>
            </div>
            <form id="form" class="form" action="current_freelancer_form.php" method="POST">
                <div class="form-control">
                    <label for="username">Freelancer ID</label>
                    <input type="text" value="<?php echo $row['freelancerID']; ?>" readonly/>
                 
                </div>

                <div class="form-control">
                    <label for="username">Email</label>
                    <input type="email" value="<?php echo $row['freelancerEmail']; ?>" readonly/>
                  
                </div>
                <div class="form-control">
                    <label for="username">First Name</label>
                    <input type="text" value="<?php echo $row['fName']; ?>" readonly/>
                 
                </div>
                <div class="form-control">
                    <label for="username">Last name</label>
                    <input type="text"value="<?php echo $row['lName']; ?>" readonly/>
                   
                </div>

                  <div class="form-control">
                    <label for="username">NIC</label>
                    <input type="text" value="<?php echo $row['freelancerNIC']; ?>" id="userid" readonly />
                   
                </div>

              <div class="form-control">
                    <label for="username">Address</label>
                    <input type="text" value="<?php echo $row['address']; ?>"  readonly/>
                   
                </div>

                <div class="form-control">
                    <label for="username">Gender</label>
                    <input type="text" value="<?php echo $row['gender']; ?>"  readonly/>
                  
                </div>

                <div class="form-control">
                    <label for="username">Phone No</label>
                    <input type="text" value="<?php echo $row['phone']; ?>"  readonly/>
                
                </div>

                
                <div class="form-control">
                    <label for="username">Working Hours From</label>
                    <input type="time" value="<?php echo $row['workingHours_from']; ?>" readonly/>
                  
                </div>

                <div class="form-control">
                    <label for="username">Working Hours To</label>
                    <input type="time" value="<?php echo $row['workingHours_to']; ?>" readonly/>
                   
                </div>

                <div class="form-control">
                    <label for="username">Date Of Birth</label>
                    <input type="date" value="<?php echo $row['doB']; ?>" readonly/>
                   
                </div>


            <div class = "links">
            <a href="view_current_freelancer.php">View Freelancer List</a><br>
            </div>  
            </form>
        </div>
     
      
        <footer>

<div id="includedContent1"></div>

</footer>


    </body>
</html>