
<?php

session_start();

?>

<?php

if(!isset($_SESSION['email'])) {
    header('Location: ../login.php');
}

?>
<?php

include ('../../../model/staff/view_advertiser.php');

?>





<!DOCTYPE html>

<html>

  <head>

    <link rel="stylesheet" type="text/css" href="../../assets/css/staff/update_advertiser.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script> 
      $(function(){
        $("#includedContent").load("header.php"); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent1").load('footer.php'); 
      });
    </script> 

    <script> 
      $(function(){
        $("#includedContent2").load('navigation.php'); 
      });
    </script> 



  </head>

  <body>

    <header>
        <div id="includedContent"></div>
      
    </header>

    <!-- Siddebar-->

    <div id="includedContent2"></div>


      <table>
                      
         <tr>

            <th colspan=9><h2>Current Advertiser Details</h2></th>

        </tr>  
        <tr>
          <th>Advertiser ID</th>
          <th>Company Name</th>
          <th>Email</th>
         <th>Registerer Name</th>
         <th>View More</th>
        
         
        
            
        </tr>

        <?php
				while($row=mysqli_fetch_assoc($result)){

			?>
      
      <tr>
        <td><?php echo $row['advertiserID'] ?></td>
				<td><?php echo $row['companyName'] ?></td>
        <td><?php echo $row['email'] ?></td>
        <td><?php echo $row['registererName'] ?></td>
				

        <?php echo "<td><a href =view_full_advertiser.php?email='".$row['email']."' > View </a> </td>"?>
				
        </tr>
        
       
       
       
        <?php
    }
    
  
    ?>
       
      </table>

        <footer>
          <div id="includedContent1"></div>
        
        </footer>

  </body>

</html>