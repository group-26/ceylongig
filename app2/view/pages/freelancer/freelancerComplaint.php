<?php
  session_start();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      type="text/css"
      href="../../assets/css/freelancer/freelancerComplaintTable.css"
    />
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link
      rel="stylesheet"
      type="text/css"
      href="../../assets/css/freelancer/freelancerSidebar.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="../../assets/css/freelancer/newComplaint.css"
    /> 
    <link rel="stylesheet" href="../../assets/css/freelancer/freelancerNavbar.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <style>
      .column {
        float: left;
        width: 50%;
      }

      td, th{
        padding: 5px;
        font-size: 12px;
      }

      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }
    </style>

    <title>Complaint Management</title>

    <script>
      $(function () {
        $("#includedContent1").load("./freelancerSidebar.php");
      });
    </script>

    <script>
      $(function () {
        $("#includedContent").load("./freelancerNavbar.php");
      });
    </script>

    <!-- <script>
      $(function () {
        $("#includedContent2").load("./newComplaint.php");
      });
    </script> -->
  </head>
  <body>
    <div style="margin-left:-2mm; margin-top:-2mm">
      <nav class="nav1">
          <div>
          <a href="../index.html"><img class="img1" src="https://i.ibb.co/n0613Lr/Ceylon-Gig-Logo.jpg" alt="CeylonGig Logo"></a>
          </div>
          <ul class="nav-links">

              <div>
                  <div style="float:left; margin-left:13cm; margin-top: 5mm;">
                      <ul class="li1" style="font-weight: bold;"><a class="a1" style="font-size:16px; color: black; list-style: none; font-weight: bold; font-family: Montserrat; display: flex; text-decoration: none" href="freelancerAboutUs.php">About</a></ul>
                  </div>
                  
                  <div  style="float:left; margin-left:10mm; margin-top: 5mm;">
                      <ul class="li1"><a href="../../../model/freelancer/freelancerLogout.php" class="log-in" style="font-size:16px; color: #fbfbfb; font-weight: bold; font-family: Montserrat; list-style: none; text-decoration: none">Logout</a></ul>
                  </div>

                  <div style="float:left; margin-left:5mm; margin-top: 1mm;">
                      <a href="freelancerProfile.php"><h3 style="font-size: 14px; text-decoration: none; font-family: Montserrat; color:#43CC58 ; font-weight: bold;"> <?php echo $_SESSION['fName'] ?> &nbsp;<?php echo $_SESSION['lName'] ?> </h3></a>
                  </div>

              </div>   
          </ul>
          
      </nav>
    </div>

    <div style="position: fixed; height:90%; z-index: 1; margin-left: -2mm; margin-top:1cm; ">
      <div id="includedContent1"></div>
    </div>

    <div class="row" style="margin-top:-6mm">
      <div class="column">
      <div class="background">
  <div class="container">
    <div class="screen">
      <div class="screen-header">
      </div>
      <div class="screen-body">
        <div class="screen-body-item left">
          <div class="app-title">
            <span>YOU HAVE A COMPLAINT?</span>
            <br />
            <span style="margin-bottom: 8mm">WE'RE ALL EARS!</span>
          </div>

          
          <form >
            <div class="app-form" style="margin-top: 5mm">
              <div class="app-form-group">
                <?php
                  include_once('../../../model/freelancer/freelancerOrderRead.php');
                
                    echo "<div class='dropdown'>";
                      echo "<div>";

                        echo "<div style='float:left; width:40%'>";
                          echo "<label style='margin-right:1cm'; color: 'white'; > ORDER ID: </label>";
                        echo "</div>";

                        echo "<div style='float:left; width:60%; margin-top: -4mm'>";
                          echo "<div class='select'>";
                            echo "<select name='orderID' id='orderID' >";
                            while ($row = mysqli_fetch_array($result0)) {
                              echo "<option  value='".$row['orderID']."'>".$row['orderID']."</option>";
                            }
                            echo "</select>";
                          echo "</div>";
                        echo "</div>";
                      echo "</div>";
                      
                     echo "</div>";

                ?>
                
              </div>
              <div class="app-form-group">
                <input class="app-form-control"  type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"  required  name="complaintEmail" id="complaintEmail" placeholder="Reply email"  />
              </div>
              <div class="app-form-group">
                <input class="app-form-control" type="tel" pattern="[0-9]{10}" required name="complaintPhone" id="complaintPhone" placeholder="Contact No" />
              </div>
              <div class="app-form-group message">
                <input class="app-form-control" type="text" required name="complaintName" id="complaintName" placeholder="Complaint Name" />
              </div>
              <div class="app-form-group message">
                <input class="app-form-control" type="text" required name="complaintDescription" id="complaintDescription" placeholder="Description" />
              </div>
              <div class="app-form-group buttons">
                <button onclick="cancel()"  class="app-form-button">CANCEL</button>
                <!-- <button class="app-form-button" type="submit" name="save">SUBMIT</button> -->
                <input type="submit" class="app-form-button" name="select" id="select" value="SUBMIT" />

              </div>
            </div>
          </form>
          <div class="app-contact">CONTACT US DIRECTLY: +76 4567890</div>
        </div>
        <!-- <div class="screen-body-item">
         
        </div> -->
      </div>
    </div>
  </div>
</div>

      </div>
      <div class="column">
        <div style="margin-left: 2cm;">

        
        <?php

          include_once('../../../model/freelancer/freelancerComplaintRead.php');

          echo "<table>
          <thead>
            <tr>
              <th>Complaint ID</th>
              <th>Complaint Name</th>
              <th>Date</th> 
              <th>Description</th>
              <th>Related Order ID</th>
              <th>Action Taken</th>
              <th>Action Status</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>";

          while ($row = mysqli_fetch_array($result)) {
            
            echo "<tr>";
            echo "<td data-column='complaintId'>".$row['complaintID']."</td>";
            echo "<td data-column='complaintName'>".$row['complaintName']."</td>";
            echo "<td data-column='complaintDate'>".$row['complaintDate']."</td>";
            echo "<td data-column='complaintDescription'>".$row['complaintDescription']."</td>";
            echo "<td data-column='orderID'>".$row['orderID']."</td>";
           
            

            
            echo "<td data-column='actionsTaken'>".$row['actionsTaken']."</td>";


            echo "<td data-column='complaintActionStatus'>".$row['complaintActionStatus']."</td>";
            echo "

              <td>    
              <form method='post' action='../../../model/freelancer/freelancerComplaintDelete.php?complaintID=".$row['complaintID']."'>
                <button type='submit' name='delete' class='btn first' value='delete' style='margin:0px'>DELETE</button>
              </form>
              </td>";

            echo "</tr>";

          }

          echo "</tbody>
          </table>";

        ?>
          
              
            
        </div>
      </div>
    </div>

    <script type="text/javascript">
      
    var freelancerEmail = '<?php echo $_SESSION["freelancerEmail"]; ?>';
    var freelancerPhone = '<?php echo $_SESSION["phone"]; ?>';

    document.getElementById('complaintEmail').value = freelancerEmail;
    document.getElementById('complaintPhone').value = freelancerPhone;



      function cancel(){
        document.getElementById('complaintEmail').value=" ";
        document.getElementById('complaintPhone').value=" ";
        document.getElementById('complaintName').value=" ";
        document.getElementById('complaintDescription').value=" ";

      }
    </script>

<script>

    function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function validatePhone(phone) {
      const re = /^\d+$/;
      if (phone.length == 10 && re.test(phone) && phone[0] == 0) {
        return true;
      }
      return false;
    }

    $(function() {

      $('#select').click(function(e) {
        e.preventDefault();
        var orderID = document.getElementById('orderID').value;
        var complaintEmail = document.getElementById('complaintEmail').value;
        var complaintPhone = document.getElementById('complaintPhone').value;
        var complaintName = document.getElementById('complaintName').value;
        var complaintDescription = document.getElementById('complaintDescription').value;

        if(orderID==""){
          alert("You don't have any orders to select");
        }else if(complaintEmail==""||complaintPhone==""||complaintName==""||complaintDescription==""){
          alert("Fields cannot be empty");

        }
        else if(!validateEmail(complaintEmail)){
          alert("Please enter a valid email");
        }
        else if(!validatePhone(complaintPhone)){
          alert("Please enter a valid phone number");
        }
        else{
          $.ajax({
            url: '../../../model/freelancer/freelancerComplaintCreate.php',
            data: {
              orderID: orderID,
              complaintEmail: complaintEmail,
              complaintPhone: complaintPhone,
              complaintName: complaintName,
              complaintDescription: complaintDescription,
            },
            type: 'POST'
          }).done(function(resp) {
            if (resp=="inserted") {
              alert("Your complaint has been submitted successfully!");
              window.location.href = 'freelancerComplaint.php';
            } else if(resp=="deleted"){
              alert("Your complaint has been deleted successfully!");
            }else if(resp=="updated"){
              alert("Your complaint has been updated successfully");
            }else{
              alert("Error in complaint submission");
            }
          });
        }



      });
    });
  </script>
  </body>
</html>
