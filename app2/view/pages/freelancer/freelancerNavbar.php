<?php

session_start();

?>



<!DOCTYPE html>

<head>
    <title>NavBar</title>
    <link rel="stylesheet" href="../../assets/css/freelancer/freelancerNavbar.css">  <!--Copy and Paste this within Head Tag-->
    <link rel="stylesheet" href="/app/view/assets/css/freelancer/popupcard.css">
    <link rel="stylesheet" href="/app/view/assets/css/freelancer/hovercard.css">
</head>

<body>

    <!--copy this whole thing (nav and script) on top of your body-->
    <nav class="nav1">
        <div>
        <a href="../index.html"><img class="img1" src="https://i.ibb.co/n0613Lr/Ceylon-Gig-Logo.jpg" alt="CeylonGig Logo"></a>
        </div>
        <ul class="nav-links">

            <div>
                <div style="float:left; margin-left:13cm; margin-top: 5mm;">
                    <ul class="li1" style="font-weight: bold;"><a class="a1" style="color: black; list-style: none; font-weight: bold; font-family: Montserrat; display: flex; text-decoration: none" href="freelancerAboutUs.php">About</a></ul>
                </div>
               
                <div  style="float:left; margin-left:10mm; margin-top: 5mm;">
                    <ul class="li1"><a href="../../../model/freelancer/freelancerLogout.php" class="log-in" style="color: #fbfbfb; font-weight: bold; font-family: Montserrat; list-style: none; text-decoration: none">Logout</a></ul>
                </div>
                
                
                <div style="float:left; margin-left:5mm; margin-top: 2mm;">
                    <a href="freelancerProfile.php"><h3 style="font-size: 14px; text-decoration: none; font-family: Montserrat; color:#43CC58 ; font-weight: bold;"> <?php echo $_SESSION['fName'] ?> &nbsp;<?php echo $_SESSION['lName'] ?> </h3></a>
                </div>

            </div>   
        </ul>
        
    </nav>

    <script src="/app/view/assets/js/freelancer/navbar.js"></script>

   

</body>

</html>