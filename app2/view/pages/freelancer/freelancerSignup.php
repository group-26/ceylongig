<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/freelancer/freelancerSignup.css" />
    <link rel="stylesheet" href="../../assets/css/freelancer/navbar.css">
    <link rel="stylesheet" href="../../assets/css/freelancer/popupcard.css">
    <link rel="icon" href="../../assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="../../assets/css/freelancer/card.css" />
    <!-- <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'> -->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Raleway"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    
    <title>Signup</title>


    
    <script>
      $(function () {
        $("#includedContent").load("../../components/navbar.html");
      });
    </script>
    <script>
      $(function () {
        $("#includedContent2").load("../../components/card.html");
      });
    </script>
  </head>

  <body>
    <!-- multistep form -->
    <div id="includedContent"></div>

    <br> <br> <br>

    <form action="../../../model/freelancer/freelancerSignupCreate.php" method="POST" 
      id="msform"
      style="width: 20cm; margin: auto; width: 60%; padding: 10px"
    >
      <!-- progressbar -->
      <ul id="progressbar">
        <li class="active">Account Setup</li>
        <li>Personal Details</li>
        <li>Freelancer Category Selection</li>
        <li>Freelancer Category Information</li>
      </ul>

      <!-- fieldsets -->
      <fieldset style="width: 60%; margin: auto; padding: 20px">
        <h2 class="fs-title">Create your account</h2>
        <h3 class="fs-subtitle">This is the first step</h3>
        <input style="padding: 15px;" onkeyup="accountSetupValidationAway()" type="text" name="freelancerEmail" id="freelancerEmail" placeholder="Email" />
        <input style="padding: 15px;" onkeyup="accountSetupValidationAway()" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" type="password" name="freelancerPassword" id="freelancerPassword" placeholder="Password"/>
        <input style="padding: 15px;" onkeyup="accountSetupValidationAway()" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" type="password" name="freelancerConfirmPassword" id="freelancerConfirmPassword" placeholder="Confirm Password" />
        <input  onmouseup="accountSetupValidation()"
          type="button"
          name="next1"
          id="next1"
          class="next action-button"
          value="Next"
        />
      </fieldset>
      
      <fieldset style="width: 95%; margin: auto; padding: 10px">
        <h2 class="fs-title">Personal Details</h2>
        <h3 class="fs-subtitle" style="margin-bottom:2mm">Let us know more about you!</h3>

        <h3 class="fs-subtitle" style="color:red; margin-left:-17cm; margin-bottom:2mm; margin-top:2mm">* Required</h3>

        <div>
          <div style="float: left; width: 50%; padding-right: 6mm">
            <input type="text" onkeyup="personalDetailsValidationAway()" name="fName" id="fName" placeholder="First Name*" required />
            <input type="text" onkeyup="personalDetailsValidationAway()" name="freelancerNIC" id="freelancerNIC" placeholder="NIC*" required />
          </div>

          
          <div  style="float:left; width:50%; ">
            <input type="text" onkeyup="personalDetailsValidationAway()" name="lName" id="lName" placeholder="Last Name*" required />


            <div>
              <div style="float: left; width: 40%; margin-left: -8mm; margin-top:1mm">
                <label class="label2" for="doB"> Date of Birth* :</label>
              </div>
            
              <div style="float: left; width: 70%; margin-left: -5mm;">
              <input type="date" onkeyup="personalDetailsValidationAway()" name="doB" id="doB" placeholder="Date of Birth" required/>
              </div>
            </div>

            

          </div>

        </div>

        <div style="float:left; margin-left:-0.5cm">

          <div style="float:left; width:15%; margin-top:1mm">
            <label class="label2" for="gender">Gender* : </label>
          </div>

          <div class="radio-toolbar" style="float:left;width:60% ">
            <div style="float:left; width:15%; margin-right:5mm ">
              <input onkeyup="personalDetailsValidationAway()" type="radio" id="radio1" name="radios0" value="1" checked required>
              <label style="width:80px" for="radio1">Male</label>
            </div>

            <div style="float:left; width:15%; margin-right:5mm">
              <input onkeyup="personalDetailsValidationAway()" type="radio" id="radio2" name="radios0" value="2" required>
              <label style="width:80px" for="radio2">Female</label>
            </div>

            <div style="float:left; width:15%; margin-right:5mm">
              <input onkeyup="personalDetailsValidationAway()" type="radio" id="radio3" name="radios0" value="3" required>
              <label style="width:80px" for="radio3">Other</label>
            </div>

            <div style="float:left; width:30%;">
              <input onkeyup="personalDetailsValidationAway()" type="radio" id="radio4" name="radios0" value="4">
              <label style="width:150px" for="radio4">Prefer not to say</label required>
            </div>

          </div>

        </div>

            
        
        <div>
          <div style="float: left; width: 50%; padding-right: 6mm">
            <input type="tel" onkeyup="personalDetailsValidationAway()" name="phone" id="phone" placeholder="Phone*" required/>
          </div>
          <div style="float: left; width: 50%">
            <input onkeyup="personalDetailsValidationAway()"
              type="number"
              min="0"
              name="experience"
              id="experience"
              placeholder="Experience (in years)"
            />
          </div>
        </div>

        <div>
          <div style="float: left; padding-top: 1mm">
            <label class="label2" for="workingHours">Working Hours* :</label>
          </div>
          <div style="float: left; padding-top: 1mm">
            <label class="label2" for="workingHours_from"> From </label>
          </div>
          <div style="width: 30%; float: left; padding-top: -2mm">
            <input onkeyup="personalDetailsValidationAway()"  type="time" name="workingHours_from" id="workingHours_from" required />
          </div>
          <div style="float: left; padding-top: 1mm">
            <label class="label2" for="workingHours_to"> To </label>
          </div>
          <div style="width: 30%; float: left; padding-top: -2mm">
            <input onkeyup="personalDetailsValidationAway()" type="time" name="workingHours_to" id="workingHours_to" required/>
          </div>
        </div>


        
        <div style="width:100%">
          <div style="float: left; margin-right:7cm; margin-top:1mm">
            <label class="label2" for="workingDays"> Working Days* :</label>
          </div>
          <div class="weekDays-selector" style="float: left; width: 70%; margin-left:-9cm" required>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-mon" id="weekday-mon" class="weekday" />
            <label for="weekday-mon">M</label>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-tue" id="weekday-tue" class="weekday" />
            <label for="weekday-tue">T</label>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox"  name="weekDays-selector[]" value="weekday-wed" id="weekday-wed" class="weekday" />
            <label for="weekday-wed">W</label>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-thu" id="weekday-thu" class="weekday" />
            <label for="weekday-thu">T</label>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-fri"  id="weekday-fri" class="weekday" />
            <label for="weekday-fri">F</label>
            <input  onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-sat" id="weekday-sat" class="weekday" />
            <label for="weekday-sat">S</label>
            <input onkeyup="personalDetailsValidationAway()" type="checkbox" name="weekDays-selector[]" value="weekday-sun" id="weekday-sun" class="weekday" />
            <label for="weekday-sun">S</label>
          </div>
        </div>


        
        <textarea onkeyup="personalDetailsValidationAway()" name="address" id="address" placeholder="Address*"></textarea>
        <input
          type="button"
          name="previous"
          class="previous action-button"
          value="Previous"
        />
        <input onmouseup="personalDetailsValidation()"
          type="button"
          name="next2"
          id="next2"
          class="next action-button"
          value="Next"
        />
      </fieldset>


      <!-- //////////SECOND FIELDSET FOR CATEGORY CARDS  -->

      <fieldset
        style="margin: auto; width: fit-content; padding: 0px; right: 50%; padding-left: 8mm; padding-top: 10mm;"
      >
        <h2 class="fs-title">Freelancer Category Selection</h2>
        <h3 class="fs-subtitle">Select your services!</h3>
        <!-- <div id="includedContent2"></div> -->




        <!-- ----------------- -->
        <!-- BEAUTICIAN -->

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <button type="button" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <?xml version="1.0" encoding="iso-8859-1"?>
              <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
              <img src="../../assets/other/beautician.svg" alt="Beauticain">
            </div>
            <p>Beautician</p>
          </button>
          <label class="container">
            <input
              onclick="{

          var status = localStorage.getItem('Beautician');
          if(status==undefined){
            localStorage.setItem('Beautician',true);
          }else if(status=='true'){
              console.log('TRUE');
            
            localStorage.setItem('Beautician',false);
          }else if(status=='false'){
            console.log('FALSE');
                localStorage.setItem('Beautician',true);
          }

        }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- PLUMBER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
             <img src="../../assets/other/plumber.svg" alt="Plumber">
            </div>
            <p>Plumber</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Plumber');
              if(status==undefined){
                localStorage.setItem('Plumber',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Plumber',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Plumber',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- -------------------------------------- -->
        <!-- PAINTER -->

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/painter.svg" alt="Painter">
            </div>
            <p>Painter</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Painter');
              if(status==undefined){
                localStorage.setItem('Painter',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Painter',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Painter',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- COOK -->

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/cook.svg" alt="Cook">
            </div>
            <p>Cook</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Cook');
              if(status==undefined){
                localStorage.setItem('Cook',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Cook',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Cook',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>

        <!-- --------------------------------- -->
        <!-- TAILOR -->

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/tailor.svg" alt="Tailor">
            </div>
            <p>Tailor</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Tailor');
              if(status==undefined){
                localStorage.setItem('Tailor',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Tailor',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Tailor',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- EVENT ORGANIZER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/event-organizer.svg" alt="EventOrganizer">
            </div>
            <p>Event Organizer</p>
          </a>
          <label class="container">
            <input 
              onclick="{

              var status = localStorage.getItem('EventOrganizer');
              if(status==undefined){
                localStorage.setItem('EventOrganizer',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('EventOrganizer',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('EventOrganizer',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- HOMEFOOD MANUFACTURER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/homefood-manufacturer.svg" alt="HomefoodManufacturer">
            </div>
            <p>Homefood</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('HomefoodManufacturer');
              if(status==undefined){
                localStorage.setItem('HomefoodManufacturer',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('HomefoodManufacturer',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('HomefoodManufacturer',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- DRIVER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">

              <img src="../../assets/other/driver.svg" alt="Driver">
            </div>
            <p>Driver</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Driver');
              if(status==undefined){
                localStorage.setItem('Driver',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Driver',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Driver',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>

        <!-- --------------------------------- -->
        <!-- PROGRAMMER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/programmer.svg" alt="Programmer">
            </div>
            <p>Programmer</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Programmer');
              if(status==undefined){
                localStorage.setItem('Programmer',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Programmer',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Programmer',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- MASON -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/mason.svg" alt="Mason">
            </div>
            <p>Mason</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Mason');
              if(status==undefined){
                localStorage.setItem('Mason',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Mason',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Mason',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>



        <!-- --------------------------------- -->
        <!-- GRAPHIC DESIGNER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/graphic-designer.svg" alt="GraphicDesigner">
            </div>
            <p>Graphic Designer</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('GraphicDesigner');
              if(status==undefined){
                localStorage.setItem('GraphicDesigner',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('GraphicDesigner',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('GraphicDesigner',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>
        

        <!-- --------------------------------- -->
        <!-- MECHANIC -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/mechanic.svg" alt="Mechanic">
            </div>
            <p>Mechanic</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Mechanic');
              if(status==undefined){
                localStorage.setItem('Mechanic',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Mechanic',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Mechanic',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>



        <!-- --------------------------------- -->
        <!-- CONTENT WRITER -->
        

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/content-writer.svg" alt="ContentWriter">
            </div>
            <p>Content Writer</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('ContentWriter');
              if(status==undefined){
                localStorage.setItem('ContentWriter',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('ContentWriter',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('ContentWriter',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- HOUSEKEEPER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/housekeeper.svg" alt="Housekeeper">
            </div>
            <p>Housekeeper</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Housekeeper');
              if(status==undefined){
                localStorage.setItem('Housekeeper',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Housekeeper',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Housekeeper',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- ELDERLY CARETAKER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/elderly-caretaker.svg" alt="ElderlyCaretaker">
            </div>
            <p>Elderly Caretaker</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('ElderlyCaretaker');
              if(status==undefined){
                localStorage.setItem('ElderlyCaretaker',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('ElderlyCaretaker',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('ElderlyCaretaker',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- GARDENER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/gardener.svg" alt="Gardener">
            </div>
            <p>Gardener</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Gardener');
              if(status==undefined){
                localStorage.setItem('Gardener',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Gardener',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Gardener',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- PHOTOGRAPHER -->
       

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/photographer.svg" alt="Photographer">
            </div>
            <p>Photographer</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Photographer');
              if(status==undefined){
                localStorage.setItem('Photographer',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Photographer',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Photographer',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- CARPENTER -->

        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/carpenter.svg" alt="Carpenter">
            </div>
            <p>Carpenter</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Carpenter');
              if(status==undefined){
                localStorage.setItem('Carpenter',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Carpenter',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Carpenter',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <!-- --------------------------------- -->
        <!-- PET CARETAKER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/pet-caretaker.svg" alt="PetCaretaker">
            </div>
            <p>Pet Caretaker</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('PetCaretaker');
              if(status==undefined){
                localStorage.setItem('PetCaretaker',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('PetCaretaker',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('PetCaretaker',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>



        <!-- --------------------------------- -->
        <!-- BABYSITTER -->


        <div style="padding-bottom: 0.5cm; float: left; padding-right: 0.5cm">
          <a href="#" class="card category">
            <div class="overlay"></div>
            <div class="circle">
              <img src="../../assets/other/babysitter.svg" alt="Babysitter">
            </div>
            <p>Babysitter</p>
          </a>
          <label class="container">
            <input
              onclick="{

              var status = localStorage.getItem('Babysitter');
              if(status==undefined){
                localStorage.setItem('Babysitter',true);
              }else if(status=='true'){
                  console.log('TRUE');
                
                localStorage.setItem('Babysitter',false);
              }else if(status=='false'){
                console.log('FALSE');
                    localStorage.setItem('Babysitter',true);
              }
    
            }"
              type="checkbox"
            />
            <span class="checkmark"></span>
          </label>
        </div>


        <input
          type="button"
          name="previous"
          class="previous action-button"
          value="Previous"
        />
        <input
          type="button"
          onclick="ABC()"
          name="next"
          class="next action-button"
          value="Next"
        />
      </fieldset>

      <fieldset>
            
        <h2 style="color:red; margin-left:10px; font-size:14px; margin-left:-12cm">* Required</h2>
        <br>
        <hr><br>

        <div id="dynamicset1"></div>

        <div id="dynamicset2"></div>

        <div id="dynamicset3"></div>

        <div id="dynamicset4"></div>

        <div id="dynamicset5"></div>

        <div id="dynamicset6"></div>

        <div id="dynamicset7"></div>

        <div id="dynamicset8"></div>

        <div id="dynamicset9"></div>

        <div id="dynamicset10"></div>

        <div id="dynamicset11"></div>

        <div id="dynamicset12"></div>

        <div id="dynamicset13"></div>

        <div id="dynamicset14"></div>

        <div id="dynamicset15"></div>

        <div id="dynamicset16"></div>

        <div id="dynamicset17"></div>

        <div id="dynamicset18"></div>

        <div id="dynamicset19"></div>

        <div id="dynamicset20"></div>

      

      
        <div style="margin-left:4cm; width:45%">

          <input
            type="button"
            name="previous"
            class="previous action-button"
            value="Previous"
          />
          
          <input type="submit" class="submit action-button" name="select" id="select" value="Select" />
        
        </div>
       

      </fieldset>
    </form>
    
    <!-- ////////////////////////
    /////VALIDATIONS////////
    //////////////////////// -->

  <script type="text/javascript">
    //#############
    //first form
    //#############

    var VALIDATE=true;

    function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function validatePassword(password, confirmPassword) {
      if (password == confirmPassword) {
        return true;
      }
      return false;
    }

    function validateDate(dOB){
      var today = new Date().getFullYear();

      var dob = new Date(dOB).getFullYear();
      console.log(today);
      console.log(dob);

      var age =today-dob;
      if(age<18){
        return false;
      }else{
        return true; 
      }
    }
  


    function validateNIC(nic) {
      const re = /^\d+$/;
      if (nic.length == 12 && re.test(nic)) {
        return true;
      } else if (nic.length == 10 && (nic[9].toLowerCase() == "v" || nic[9].toLowerCase() == "x")) {
        return true;
      }
      return false;
    }

    function validatePhone(phone) {
      const re = /^\d+$/;
      if (phone.length == 10 && re.test(phone) && phone[0] == 0) {
        return true;
      }
      return false;
    }

    function validateNames(fName, lName) {
      const re = /^[a-zA-Z]+$/;

      if (re.test(fName) && re.test(lName)) {
        // alert("correct name");
        return true;
      }
      // alert("incorrect name");
      return false;
    }


    function accountSetupValidation() {


      var freelancerEmail = document.getElementById("freelancerEmail").value;
      var freelancerPassword = document.getElementById("freelancerPassword").value;
      var freelancerConfirmPassword = document.getElementById("freelancerConfirmPassword").value;



      if (freelancerEmail == "" || freelancerPassword == "" || freelancerConfirmPassword == "") {
        alert('Required fields cannot be empty');
        // console.log(freelancerEmail);
        // console.log(freelancerPassword);
        // console.log(freelancerConfirmPassword);
        document.getElementById("next1").disabled = VALIDATE;
        document.getElementById("next1").className = 'button_color';

      } else if (!validateEmail(freelancerEmail)) {

        alert('Please enter a valid email');
        document.getElementById("next1").disabled = VALIDATE;
        document.getElementById("next1").className = 'button_color';

      } else if (!validatePassword(freelancerPassword, freelancerConfirmPassword)) {
        alert('Passwords do not match');
        document.getElementById("next1").disabled = VALIDATE;
        document.getElementById("next1").className = 'button_color';
      } else {
        document.getElementById("next1").disabled = false;
        document.getElementById("next1").className = 'action-button';
      }
    }

    function accountSetupValidationAway() {
      document.getElementById("next1").disabled = false;
    }


    //#############
    //personal details
    //#############
    function personalDetailsValidation() {

      var fName = document.getElementById('fName').value;
      var lName = document.getElementById('lName').value;
      var freelancerNIC = document.getElementById('freelancerNIC').value;
      var doB = document.getElementById('doB').value;
      var phone = document.getElementById('phone').value;
      var address = document.getElementById('address').value;
      var workingHours_from = document.getElementById('workingHours_from').value;
      var workingHours_to = document.getElementById('workingHours_to').value;

      console.log(workingHours_from);
      console.log(workingHours_to);
      console.log(doB);

      if (fName == "" || lName == "" || freelancerNIC == "" || phone == "" || address == "" || workingHours_from == "" || workingHours_to == "" || doB == "") {
        alert('Fields cannot be empty');
        document.getElementById("next2").disabled = VALIDATE;
        document.getElementById("next2").className = 'button_color';

      } else if (!validateNames(fName, lName)) {
        alert('First Name and Last name should contain letters only');
        document.getElementById("next2").disabled = VALIDATE;
        document.getElementById("next2").className = 'button_color';

      } else if (!validateNIC(freelancerNIC)) {
        alert('Invalid NIC');
        document.getElementById("next2").disabled = VALIDATE;
        document.getElementById("next2").className = 'button_color';

      } else if (!validatePhone(phone)) {
        alert('Invalid Phone number');
        document.getElementById("next2").disabled = VALIDATE;
        document.getElementById("next2").className = 'button_color';

      } else if(!validateDate(doB)) {
        alert('You need to be at least 18 years to signup as a freelancer');
        document.getElementById("next2").disabled = VALIDATE;
        document.getElementById("next2").className = 'button_color';
      }
      
      else {

        document.getElementById("next2").disabled = false;
        document.getElementById("next2").className = 'action-button';
      }
    }

    function personalDetailsValidationAway() {

      // alert('away');
      document.getElementById("next2").disabled = false;
    }
    
    
  </script>


  <!-- ////////////////////////
    /////VALIDATIONS OVER!!!!!!!!
    //////////////////////// -->



    <script>
    $(function() {
      $('#select').click(function(e) {
        e.preventDefault();

        var beautician = window.localStorage.getItem("Beautician");
        var plumber = window.localStorage.getItem("Plumber");
        var painter = window.localStorage.getItem("Painter");
        var cook = window.localStorage.getItem("Cook");

        var tailor = window.localStorage.getItem("Tailor");
        var eventOrganizer = window.localStorage.getItem("EventOrganizer");
        var homefood = window.localStorage.getItem("HomefoodManufacturer");
        var driver = window.localStorage.getItem("Driver");

        var programmer = window.localStorage.getItem("Programmer");
        var mason = window.localStorage.getItem("Mason");
        var graphicDesigner = window.localStorage.getItem("GraphicDesigner");
        var mechanic = window.localStorage.getItem("Mechanic");

        var contentWriter = window.localStorage.getItem("ContentWriter");
        var housekeeper = window.localStorage.getItem("Housekeeper");
        var elderlyCaretaker = window.localStorage.getItem("ElderlyCaretaker");
        var gardener = window.localStorage.getItem("Gardener");

        var photographer = window.localStorage.getItem("Photographer");
        var carpenter = window.localStorage.getItem("Carpenter");
        var petCaretaker = window.localStorage.getItem("PetCaretaker");
        var babysitter = window.localStorage.getItem("Babysitter");



        
        var freelancerEmail = document.getElementById('freelancerEmail').value;
        var freelancerPassword = document.getElementById('freelancerPassword').value;
        var freelancerConfirmPassword = document.getElementById('freelancerConfirmPassword').value;
        var fName = document.getElementById('fName').value;
        var lName = document.getElementById('lName').value;
        var freelancerNIC = document.getElementById('freelancerNIC').value;
        var doB = document.getElementById('doB').value;
        var phone = document.getElementById('phone').value;
        var experience = document.getElementById('experience').value;
        var gender = document.querySelector('input[name="radios0"]:checked').value;
        var address = document.getElementById('address').value;
        var workingHours_from = document.getElementById('workingHours_from').value;
        var workingHours_to = document.getElementById('workingHours_to').value;
        
      
       

        //working days
        var monday = document.getElementById('weekday-mon').checked;
        var tuesday = document.getElementById('weekday-tue').checked;
        var wednesday = document.getElementById('weekday-wed').checked;
        var thursday = document.getElementById('weekday-thu').checked;
        var friday = document.getElementById('weekday-fri').checked;
        var saturday = document.getElementById('weekday-sat').checked;
        var sunday = document.getElementById('weekday-sun').checked;


        var temp = [monday, tuesday, wednesday, thursday, friday, saturday, sunday];
        var workingDays = [];
        for (i = 0; i < temp.length; i++) {
          if (temp[i] == true) {
            if (i == 0) {
              
              workingDays.push('mon');
            } else if (i == 1) {
              workingDays.push('tue');

            } else if (i == 2) {
              workingDays.push('wed');

            } else if (i == 3) {
              workingDays.push('thu');

            } else if (i == 4) {
              workingDays.push('fri');

            } else if (i == 5) {
              workingDays.push('sat');

            } else if (i == 6) {
              workingDays.push('sun');

            }
          }
        }
        workingDays = JSON.stringify(workingDays);

        console.log(workingDays);


        // var checkedValue = null;
        // var inputElements = document.getElementsByClassName('weekday');
        // for (var i = 0; inputElements[i]; ++i) {
        //   if (inputElements[i].checked) {
        //     checkedValue = inputElements[i].value;
        //     break;
        //   }
        // }

        var metaData = [];
        


        // ##############################
        // BEAUTICIAN
        // ##############################

        if (beautician == true || beautician != undefined) {

          metaData.push("beautician");

          var beauticianServiceType = document.querySelector('input[name="radios1"]:checked').value;
          var beauticianGenderType = document.querySelector('input[name="radios2"]:checked').value;

          var beauticianQualifications = document.getElementById('beauticianQualifications').value;
          var beauticianAdditionalServices = document.getElementById('beauticianAdditionalServices').value;
          

          var hairTreatments = document.getElementById('hairTreatments').checked;
          var hairColoring = document.getElementById('hairColoring').checked;
          var hairStyling = document.getElementById('hairStyling').checked;
          var bodyTreatments = document.getElementById('bodyTreatments').checked;
          var facialThreading = document.getElementById('facialThreading').checked;
          var facials = document.getElementById('facials').checked;
          var bodyWaxing = document.getElementById('bodyWaxing').checked;
          var manicure = document.getElementById('manicure').checked;
          var pedicure = document.getElementById('pedicure').checked;
          var fullDressing = document.getElementById('fullDressing').checked;
          var makeup = document.getElementById('makeup').checked;

          var beauticianTemp = [hairTreatments, hairColoring, hairStyling, bodyTreatments,facialThreading,facials,bodyWaxing,manicure,pedicure,fullDressing,makeup];
          
          var beauticianServices = [];
          for (i = 0; i < beauticianTemp.length; i++) {
            if (beauticianTemp[i] == true) {
              if (i == 0) {
                
                beauticianServices.push('hairTreatments');
              } else if (i == 1) {
                beauticianServices.push('hairColoring');

              } else if (i == 2) {
                beauticianServices.push('hairStyling');

              } else if (i == 3) {
                beauticianServices.push('bodyTreatments');

              } else if (i == 4) {
                beauticianServices.push('facialThreading');

              } else if (i == 5) {
                beauticianServices.push('facials');

              } else if (i == 6) {
                beauticianServices.push('bodyWaxing');

              }
              else if (i == 7) {
                beauticianServices.push('manicure');

              }
              else if (i == 8) {
                beauticianServices.push('pedicure');

              }
              else if (i == 9) {
                beauticianServices.push('fullDressing');

              }
              else if (i == 10) {
                beauticianServices.push('makeup');

              }
            }
          }
          beauticianServices = JSON.stringify(beauticianServices);
        }




        // ############################
        // PLUMBER
        // ############################


        if (plumber == true || plumber != undefined) {

          metaData.push("plumber");

          var plumberAdditionalServices = document.getElementById('plumberAdditionalServices').value;

          //checkbox ids 
          var leakRepair = document.getElementById('leakRepair').checked;
          var sewerRepair = document.getElementById('sewerRepair').checked;
          var toiletRepair = document.getElementById('toiletRepair').checked;
          var drainCleaning = document.getElementById('drainCleaning').checked;
          var waterHeaterInstallation = document.getElementById('waterHeaterInstallation').checked;
         

          var plumberTemp = [leakRepair,sewerRepair, toiletRepair,  drainCleaning, waterHeaterInstallation];

          var plumberServices = [];
          for (i = 0; i < plumberTemp.length; i++) {
            if (plumberTemp[i] == true) {
              if (i == 0) {
                
                plumberServices.push('leakRepair');
              } else if (i == 1) {
                plumberServices.push('sewerRepair');

              } else if (i == 2) {
                plumberServices.push('toiletRepair');

              } else if (i == 3) {
                plumberServices.push('drainCleaning');

              } else if (i == 4) {
                plumberServices.push('waterHeaterInstallation');

              } 
            }
          }
          plumberServices = JSON.stringify(plumberServices);
        }

        // ##############################
        // PAINTER
        // ##############################

        if (painter == true || painter != undefined) {

          metaData.push("painter");

          //optional fields
          var painterAdditionalServices = document.getElementById('painterAdditionalServices').value;

          //checkbox ids
          var furniturePainting = document.getElementById('furniturePainting').checked;
          var wallPainting = document.getElementById('wallPainting').checked;
          var ceilingPainting = document.getElementById('ceilingPainting').checked;
          

          var painterTemp = [furniturePainting, wallPainting, ceilingPainting];

          var painterServices = [];
          for (i = 0; i < painterTemp.length; i++) {
            if (painterTemp[i] == true) {
              if (i == 0) {
                
                painterServices.push('furniturePainting');
              } else if (i == 1) {
                painterServices.push('wallPainting');

              } else if (i == 2) {
                painterServices.push('ceilingPainting');

              } 
            }
          }
          painterServices = JSON.stringify(painterServices);
        }


        // ##############################
        // COOK
        // ##############################

        if (cook == true || cook != undefined) {

          metaData.push("cook");

          //radio button input
          var cookVisitType = document.querySelector('input[name="radios3"]:checked').value;
          
          //optional text fields
          var cookQualifications = document.getElementById('cookQualifications').value;
          var cookAdditionalCuisines = document.getElementById('cookAdditionalCuisines').value;

          //checkbox inputs 
          var sriLankan = document.getElementById('sriLankan').checked;
          var northIndian = document.getElementById('northIndian').checked;
          var southIndian = document.getElementById('southIndian').checked;
          var chinese = document.getElementById('chinese').checked;
          var arab = document.getElementById('arab').checked;
          var thai = document.getElementById('thai').checked;
          var italian = document.getElementById('italian').checked;
          var healthyFood = document.getElementById('healthyFood').checked;
       

          var cookTemp = [sriLankan, northIndian, southIndian, chinese, arab, thai, italian, healthyFood];

          var cookCuisines = [];
          for (i = 0; i < cookTemp.length; i++) {
            if (cookTemp[i] == true) {
              if (i == 0) {
                
                cookCuisines.push('sriLankan');
              } else if (i == 1) {
                cookCuisines.push('northIndian');

              } else if (i == 2) {
                cookCuisines.push('southIndian');

              } else if (i == 3) {
                cookCuisines.push('chinese');

              } else if (i == 4) {
                cookCuisines.push('arab');

              } else if (i == 5) {
                cookCuisines.push('thai');

              } else if (i == 6) {
                cookCuisines.push('italian');

              }
              else if (i == 7) {
                cookCuisines.push('healthyFood');

              }
              
            }
          }
          cookCuisines = JSON.stringify(cookCuisines);
        }

        // ##############################
        // TAILOR
        // ##############################

        if (tailor == true || tailor != undefined) {

          metaData.push("tailor");

          var tailorGenderType = document.querySelector('input[name="radios4"]:checked').value;
          var tailorAdditionalDressTypes = document.getElementById('tailorAdditionalDressTypes').value;


          var girlsSchoolUniform = document.getElementById('girlsSchoolUniform').checked;
          var boyschoolUniform = document.getElementById('boyschoolUniform').checked;
          var gentsSuits = document.getElementById('gentsSuits').checked;
          var sareeJackets = document.getElementById('sareeJackets').checked;
          var bridalDresses = document.getElementById('bridalDresses').checked;
          var ladiesDresses = document.getElementById('ladiesDresses').checked;
          var kidsDresses = document.getElementById('kidsDresses').checked;
         
          var tailorTemp = [girlsSchoolUniform, boyschoolUniform, gentsSuits, sareeJackets, bridalDresses, ladiesDresses, kidsDresses];

          var tailorDressTypes = [];
          for (i = 0; i < tailorTemp.length; i++) {
            if (tailorTemp[i] == true) {
              if (i == 0) {
                
                tailorDressTypes.push('girlsSchoolUniform');
              } else if (i == 1) {
                tailorDressTypes.push('boyschoolUniform');

              } else if (i == 2) {
                tailorDressTypes.push('gentsSuits');

              } else if (i == 3) {
                tailorDressTypes.push('sareeJackets');

              } else if (i == 4) {
                tailorDressTypes.push('bridalDresses');

              } else if (i == 5) {
                tailorDressTypes.push('ladiesDresses');

              } else if (i == 6) {
                tailorDressTypes.push('kidsDresses');

              }
              
            }
          }
          tailorDressTypes = JSON.stringify(tailorDressTypes);
        }

        // ##############################
        // EVENT ORGANIZER
        // ##############################

        if (eventOrganizer == true || eventOrganizer != undefined) {

          metaData.push("eventOrganizer");

          
          //optional fields
          var eventOrganizerAdditionalEvents = document.getElementById('eventOrganizerAdditionalEvents').value;
          var eventOrganizerAdditionalServices = document.getElementById('eventOrganizerAdditionalServices').value;
          var eventOrganizerQualifications = document.getElementById('eventOrganizerQualifications').value;

          //##############################
          //checkbox fields for event types 
          var birthdays = document.getElementById('birthdays').checked;
          var weddings = document.getElementById('weddings').checked;
          var parties = document.getElementById('parties').checked;
          var liveinConcerts = document.getElementById('liveinConcerts').checked;
          var bridalShowers = document.getElementById('bridalShowers').checked;
          var babyShowers = document.getElementById('babyShowers').checked;
          var corporateEvents = document.getElementById('corporateEvents').checked;
          

          var eventOrganizerEventTypesTemp = [ birthdays, weddings,parties,liveinConcerts,bridalShowers,babyShowers,corporateEvents];

          var eventOrganizerEventTypes = [];
          for (i = 0; i < eventOrganizerEventTypesTemp.length; i++) {
            if (eventOrganizerEventTypesTemp[i] == true) {
              if (i == 0) {
                
                eventOrganizerEventTypes.push('birthdays');
              } else if (i == 1) {
                eventOrganizerEventTypes.push('weddings');

              } else if (i == 2) {
                eventOrganizerEventTypes.push('parties');

              } else if (i == 3) {
                eventOrganizerEventTypes.push('liveinConcerts');

              } else if (i == 4) {
                eventOrganizerEventTypes.push('bridalShowers');

              } else if (i == 5) {
                eventOrganizerEventTypes.push('babyShowers');

              } else if (i == 6) {
                eventOrganizerEventTypes.push('corporateEvents');

              }
             
            }
          }
          eventOrganizerEventTypes = JSON.stringify(eventOrganizerEventTypes);



          // ###########################
          //checkbox fields for services 
          var foodAndBeverage = document.getElementById('foodAndBeverage').checked;
          var decor = document.getElementById('decor').checked;
          var transportationManagement = document.getElementById('transportationManagement').checked;
          var siteSelection = document.getElementById('siteSelection').checked;
          var budgetPlanning = document.getElementById('budgetPlanning').checked;
          var attendeeRegistration = document.getElementById('attendeeRegistration').checked;
          var speakerDelegateCoordination = document.getElementById('speakerDelegateCoordination').checked;
          

          var eventOrganizerServicesTemp = [ foodAndBeverage, decor, transportationManagement, siteSelection, budgetPlanning, attendeeRegistration, speakerDelegateCoordination];

          var eventOrganizerServices = [];
          for (i = 0; i < eventOrganizerServicesTemp.length; i++) {
            if (eventOrganizerServicesTemp[i] == true) {
              if (i == 0) {
                
                eventOrganizerServices.push('foodAndBeverage');
              } else if (i == 1) {
                eventOrganizerServices.push('decor');

              } else if (i == 2) {
                eventOrganizerServices.push('transportationManagement');

              } else if (i == 3) {
                eventOrganizerServices.push('siteSelection');

              } else if (i == 4) {
                eventOrganizerServices.push('budgetPlanning');

              } else if (i == 5) {
                eventOrganizerServices.push('attendeeRegistration');

              } else if (i == 6) {
                eventOrganizerServices.push('speakerDelegateCoordination');

              }
             
            }
          }
          eventOrganizerServices = JSON.stringify(eventOrganizerServices);


          
        }

        // ##############################
        // HOMEFOOD MANUFACTURER
        // ##############################

        if (homefood == true || homefood != undefined) {

          metaData.push("homefood");

          var homefoodAdditionalCuisines = document.getElementById('homefoodAdditionalCuisines').value;
          var homefoodQualifications = document.getElementById('homefoodQualifications').value;


          var homefoodSriLankan = document.getElementById('homefoodSriLankan').checked;
          var homefoodNorthIndian = document.getElementById('homefoodNorthIndian').checked;
          var homefoodSouthIndian = document.getElementById('homefoodSouthIndian').checked;
          var homefoodChinese = document.getElementById('homefoodChinese').checked;
          var homefoodArab = document.getElementById('homefoodArab').checked;
          var homefoodThai = document.getElementById('homefoodThai').checked;
          var homefoodItalian = document.getElementById('homefoodItalian').checked;
          var homefoodHealthyFood = document.getElementById('homefoodHealthyFood').checked;


          var homefoodTemp = [homefoodSriLankan, homefoodNorthIndian, homefoodSouthIndian, homefoodChinese, homefoodArab, homefoodThai, homefoodItalian, homefoodHealthyFood];

          var homefoodCuisines = [];
          for (i = 0; i < homefoodTemp.length; i++) {
            if (homefoodTemp[i] == true) {
              if (i == 0) {
                
                homefoodCuisines.push('homefoodSriLankan');
              } else if (i == 1) {
                homefoodCuisines.push('homefoodNorthIndian');

              } else if (i == 2) {
                homefoodCuisines.push('homefoodSouthIndian');

              } else if (i == 3) {
                homefoodCuisines.push('homefoodChinese');

              } else if (i == 4) {
                homefoodCuisines.push('homefoodArab');

              } else if (i == 5) {
                homefoodCuisines.push('homefoodThai');

              } else if (i == 6) {
                homefoodCuisines.push('homefoodItalian');

              }
              else if (i == 7) {
                homefoodCuisines.push('homefoodHealthyFood');

              }
              
            }
          }
          homefoodCuisines = JSON.stringify(homefoodCuisines);
        }

        // ##############################
        // DRIVER
        // ##############################

        if (driver == true || driver != undefined) {

          metaData.push("driver");

          //mandatorY fields

          var driverVehicleType = document.querySelector('input[name="radios5"]:checked').value;
          var drivingLicense = document.getElementById('drivingLicense').value;
         
        }

        // ##############################
        // PROGRAMMER
        // ##############################

        if (programmer == true || programmer != undefined) {

          metaData.push("programmer");


          //optional fields
          var programmerProjects = document.getElementById('programmerProjects').value;
          var programmerAdditionalLanguages = document.getElementById('programmerAdditionalLanguages').value;
          var programmerAdditionalFrameworks = document.getElementById('programmerAdditionalFrameworks').value;
          var programmerQualifications = document.getElementById('programmerQualifications').value;

          //##############################
          //checkbox fields for development types
          var webDeveloper = document.getElementById('webDeveloper').checked;
          var backEndDeveloper = document.getElementById('backEndDeveloper').checked;
          var mobileDeveloper = document.getElementById('mobileDeveloper').checked;
      


          var programmerDevelopmentTypeTemp = [ webDeveloper, backEndDeveloper, mobileDeveloper];

          var programmerDevelopmentType = [];
          for (i = 0; i < programmerDevelopmentTypeTemp.length; i++) {
            if (programmerDevelopmentTypeTemp[i] == true) {
              if (i == 0) {
                
                programmerDevelopmentType.push('webDeveloper');
              } else if (i == 1) {
                programmerDevelopmentType.push('backEndDeveloper');

              } else if (i == 2) {
                programmerDevelopmentType.push('mobileDeveloper');

              } 
            
            }
          }
          programmerDevelopmentType = JSON.stringify(programmerDevelopmentType);



          // ###########################
          //checkbox fields for programmer languages 
          var java = document.getElementById('java').checked;
          var cP = document.getElementById('c++').checked;
          var python = document.getElementById('python').checked;
          var cS = document.getElementById('c#').checked;
          var javascript = document.getElementById('javascript').checked;
          


          var programmerLanguagesTemp = [java, cP, python, cS, javascript];

          var programmerLanguages = [];
          for (i = 0; i < programmerLanguagesTemp.length; i++) {
            if (programmerLanguagesTemp[i] == true) {
              if (i == 0) {
                
                programmerLanguages.push('java');
              } else if (i == 1) {
                programmerLanguages.push('cP');

              } else if (i == 2) {
                programmerLanguages.push('python');

              } else if (i == 3) {
                programmerLanguages.push('cS');

              } else if (i == 4) {
                programmerLanguages.push('javascript');

              } 
            
            }
          }
          programmerLanguages = JSON.stringify(programmerLanguages);


           // ###########################
          //checkbox fields for programmer frameworks
          var angular = document.getElementById('angular').checked;
          var vue = document.getElementById('vue').checked;
          var laravel = document.getElementById('laravel').checked;
          var react = document.getElementById('react').checked;
          var reactNative = document.getElementById('reactNative').checked;
          var springboot = document.getElementById('springboot').checked;
          var flutter = document.getElementById('flutter').checked;
          


          var programmerFrameworksTemp = [angular, vue, laravel, react, reactNative, springboot, flutter];

          var programmerFrameworks = [];
          for (i = 0; i < programmerFrameworksTemp.length; i++) {
            if (programmerFrameworksTemp[i] == true) {
              if (i == 0) {
                
                programmerFrameworks.push('angular');
              } else if (i == 1) {
                programmerFrameworks.push('vue');

              } else if (i == 2) {
                programmerFrameworks.push('laravel');

              } else if (i == 3) {
                programmerFrameworks.push('react');

              } else if (i == 4) {
                programmerFrameworks.push('reactNative');

              }
              else if (i == 5) {
                programmerFrameworks.push('springboot');

              }
              else if (i == 6) {
                programmerFrameworks.push('flutter');

              }
            
            }
          }
          programmerFrameworks = JSON.stringify(programmerFrameworks);
        }

        // ##############################
        // MASON
        // ##############################

        if (mason == true || mason != undefined) {

          metaData.push("mason");

          //optional field
          
          var masonAdditionalServices = document.getElementById('masonAdditionalServices').value;

          //checkboxes for mason services 
          var houses = document.getElementById('houses').checked;
          var bathrooms = document.getElementById('bathrooms').checked;
          var corporateBuildings = document.getElementById('corporateBuildings').checked;
 

          var masonServicesTemp = [houses, bathrooms, corporateBuildings];

          var masonServices = [];
          for (i = 0; i < masonServicesTemp.length; i++) {
            if (masonServicesTemp[i] == true) {
              if (i == 0) {
                
                masonServices.push('houses');
              } else if (i == 1) {
                masonServices.push('bathrooms');

              } else if (i == 2) {
                masonServices.push('corporateBuildings');

              } 
            }
          }

          masonServices = JSON.stringify(masonServices);
        }

        // ##############################
        // GRAPHIC DESIGNER
        // ##############################

        if (graphicDesigner == true || graphicDesigner != undefined) {

          metaData.push("graphicDesigner");

          
          //optional fields 
          var graphicDesignerAditionalTypes = document.getElementById('graphicDesignerAditionalTypes').value;
          var graphicDesignerQualifications = document.getElementById('graphicDesignerQualifications').value;


          var bannerDesigning = document.getElementById('bannerDesigning').checked;
          var souvenirDesigning = document.getElementById('souvenirDesigning').checked;
          var tShirtDesigning = document.getElementById('tShirtDesigning').checked;
          var socialMediaPostDesigning = document.getElementById('socialMediaPostDesigning').checked;
          

          var graphicDesignerDesignTypesTemp = [bannerDesigning, souvenirDesigning, tShirtDesigning, socialMediaPostDesigning];

          var graphicDesignerDesignTypes = [];
          for (i = 0; i < graphicDesignerDesignTypesTemp.length; i++) {
            if (graphicDesignerDesignTypesTemp[i] == true) {
              if (i == 0) {
                
                graphicDesignerDesignTypes.push('bannerDesigning');
              } else if (i == 1) {
                graphicDesignerDesignTypes.push('souvenirDesigning');

              } else if (i == 2) {
                graphicDesignerDesignTypes.push('tShirtDesigning');

              } else if (i == 3) {
                graphicDesignerDesignTypes.push('socialMediaPostDesigning');

              } 
            }
          }
          graphicDesignerDesignTypes = JSON.stringify(graphicDesignerDesignTypes);
        }

        // ##############################
        // MECHANIC
        // ##############################

        if (mechanic == true || mechanic != undefined) {

          metaData.push("mechanic");

          //optional text fields 
          var mechanicAdditionalServices = document.getElementById('mechanicAdditionalServices').value;
          var mechanicAdditionalVehicleTypes = document.getElementById('mechanicAdditionalVehicleTypes').value;
          var mechanicAdditionalVehicleBrands = document.getElementById('mechanicAdditionalVehicleBrands').value;

          //checkboxes for service types 
          var brakeRepair = document.getElementById('brakeRepair').checked;
          var diagnostics = document.getElementById('diagnostics').checked;
          var tireRotation = document.getElementById('tireRotation').checked;
          var oilChange = document.getElementById('oilChange').checked;
          var tuneup = document.getElementById('tuneup').checked;

          var mechanicServicesTemp = [brakeRepair, diagnostics, tireRotation, oilChange, tuneup];

          var mechanicServices = [];
          for (i = 0; i < mechanicServicesTemp.length; i++) {
            if (mechanicServicesTemp[i] == true) {
              if (i == 0) {
                
                mechanicServices.push('brakeRepair');
              } else if (i == 1) {
                mechanicServices.push('diagnostics');

              } else if (i == 2) {
                mechanicServices.push('tireRotation');

              } else if (i == 3) {
                mechanicServices.push('oilChange');

              } else if (i == 4) {
                mechanicServices.push('tuneup');

              } 
            }
          }
          mechanicServices = JSON.stringify(mechanicServices);


          //checkboxes for vehicle types 
          var motorcycle = document.getElementById('motorcycle').checked;
          var car = document.getElementById('car').checked;
          var van = document.getElementById('van').checked;
          var bus = document.getElementById('bus').checked;
          var truck = document.getElementById('truck').checked;

          var mechanicVehicleTypeTemp = [motorcycle, car, van, bus, truck];

          var mechanicVehicleType = [];
          for (i = 0; i < mechanicVehicleTypeTemp.length; i++) {
            if (mechanicVehicleTypeTemp[i] == true) {
              if (i == 0) {
                
                mechanicVehicleType.push('motorcycle');
              } else if (i == 1) {
                mechanicVehicleType.push('car');

              } else if (i == 2) {
                mechanicVehicleType.push('van');

              } else if (i == 3) {
                mechanicVehicleType.push('bus');

              } else if (i == 4) {
                mechanicVehicleType.push('truck');

              } 
            }
          }
          mechanicVehicleType = JSON.stringify(mechanicVehicleType);


          //checkboxes for vehicle brands 
          var toyota = document.getElementById('toyota').checked;
          var honda = document.getElementById('honda').checked;
          var nissan = document.getElementById('nissan').checked;
          var suzuki = document.getElementById('suzuki').checked;
          var mercedes = document.getElementById('mercedes').checked;
          var tvs = document.getElementById('tvs').checked;
          var bajaj = document.getElementById('bajaj').checked;
          var yamaha = document.getElementById('yamaha').checked;
          var audi = document.getElementById('audi').checked;
          var mitsubishi = document.getElementById('mitsubishi').checked;

          var mechanicVehicleBrandsTemp = [toyota, honda, nissan, suzuki, mercedes, tvs, bajaj, yamaha, audi,mitsubishi];

          var mechanicVehicleBrands = [];
          for (i = 0; i < mechanicVehicleBrandsTemp.length; i++) {
            if (mechanicVehicleBrandsTemp[i] == true) {
              if (i == 0) {
                
                mechanicVehicleBrands.push('toyota');
              } else if (i == 1) {
                mechanicVehicleBrands.push('honda');

              } else if (i == 2) {
                mechanicVehicleBrands.push('nissan');

              } else if (i == 3) {
                mechanicVehicleBrands.push('suzuki');

              } else if (i == 4) {
                mechanicVehicleBrands.push('mercedes');

              } 
              else if (i == 5) {
                mechanicVehicleBrands.push('tvs');

              } 
              else if (i == 6) {
                mechanicVehicleBrands.push('bajaj');

              } 
              else if (i == 7) {
                mechanicVehicleBrands.push('yamaha');

              } 
              else if (i == 8) {
                mechanicVehicleBrands.push('audi');

              } 
              else if (i == 9) {
                mechanicVehicleBrands.push('mitsubishi');

              } 
            }
          }
          mechanicVehicleBrands = JSON.stringify(mechanicVehicleBrands);
        }

        // ##############################
        // CONTENT WRITER
        // ##############################

        if (contentWriter == true || contentWriter != undefined) {

          metaData.push("contentWriter");

          
          //optional fields
          var contentWriterPreviousWork = document.getElementById('contentWriterPreviousWork').value;
          var contentWriterAdditionalLanguages = document.getElementById('contentWriterAdditionalLanguages').value;
          var contentWriterQualifications = document.getElementById('contentWriterQualifications').value;

          //checkboxes for languages
          var sinhalese = document.getElementById('sinhalese').checked;
          var english = document.getElementById('english').checked;
          var tamil = document.getElementById('tamil').checked;
          var hindi = document.getElementById('hindi').checked;
          var japanese = document.getElementById('japanese').checked;
          var arabic = document.getElementById('arabic').checked;
          var german = document.getElementById('german').checked;
          var korean = document.getElementById('korean').checked;
          var french = document.getElementById('french').checked;

          var contentWriterLanguagesTemp = [sinhalese, english, tamil, hindi, japanese, arabic, german, korean, french];

          var contentWriterLanguages = [];
          for (i = 0; i < contentWriterLanguagesTemp.length; i++) {
            if (contentWriterLanguagesTemp[i] == true) {
              if (i == 0) {
                
                contentWriterLanguages.push('sinhalese');
              } else if (i == 1) {
                contentWriterLanguages.push('english');

              } else if (i == 2) {
                contentWriterLanguages.push('tamil');

              } else if (i == 3) {
                contentWriterLanguages.push('hindi');

              } else if (i == 4) {
                contentWriterLanguages.push('japanese');

              } else if (i == 5) {
                contentWriterLanguages.push('arabic');

              } else if (i == 6) {
                contentWriterLanguages.push('german');

              }
              else if (i == 7) {
                contentWriterLanguages.push('korean');

              }
              else if (i == 8) {
                contentWriterLanguages.push('french');

              }
              
            }
          }
          contentWriterLanguages = JSON.stringify(contentWriterLanguages);
        }

        // ##############################
        // HOUSEKEEPER
        // ##############################

        if (housekeeper == true || housekeeper != undefined) {

          metaData.push("housekeeper");

          //mandatory 
          var housekeeperVisitType = document.querySelector('input[name="radios6"]:checked').value;

          //optional
          var housekeeperComment = document.getElementById('housekeeperComment').value;

        }

        // ##############################
        // ELDERLY CARETAKER 
        // ##############################

        if (elderlyCaretaker == true || elderlyCaretaker != undefined) {

          metaData.push("elderlyCaretaker");

          //mandatory 
          var elderlyCaretakerVisitType = document.querySelector('input[name="radios7"]:checked').value;
          var elderlyCaretakerGenderType = document.querySelector('input[name="radios8"]:checked').value;
          var elderlyCaretakerAgeFrom = document.getElementById('elderlyCaretakerAgeFrom').value;
          var elderlyCaretakerAgeTo = document.getElementById('elderlyCaretakerAgeTo').value;

        }

        // ##############################
        // GARDENER 
        // ##############################

        if (gardener == true || gardener != undefined) {

          metaData.push("gardener");

          //mandatory
          var gardenerVisitType = document.querySelector('input[name="radios9"]:checked').value;

          //optional 
          var gardenerAdditionalServices = document.getElementById('gardenerAdditionalServices').value;
          var gardenerQualifications = document.getElementById('gardenerQualifications').value;


          var landscaping = document.getElementById('landscaping').checked;
          var hedgeTrimming = document.getElementById('hedgeTrimming').checked;
          var vegetableGrowing = document.getElementById('vegetableGrowing').checked;
          var weeding = document.getElementById('weeding').checked;
          var pruning = document.getElementById('pruning').checked;
          var renovationClearence = document.getElementById('renovationClearence').checked;
          var fertilizingPesticiding = document.getElementById('fertilizingPesticiding').checked;

          var gardenerServicesTemp = [landscaping, hedgeTrimming, vegetableGrowing, weeding, pruning, renovationClearence, fertilizingPesticiding];

          var gardenerServices = [];
          for (i = 0; i < gardenerServicesTemp.length; i++) {
            if (gardenerServicesTemp[i] == true) {
              if (i == 0) {
                
                gardenerServices.push('landscaping');
              } else if (i == 1) {
                gardenerServices.push('hedgeTrimming');

              } else if (i == 2) {
                gardenerServices.push('vegetableGrowing');

              } else if (i == 3) {
                gardenerServices.push('weeding');

              } else if (i == 4) {
                gardenerServices.push('pruning');

              } else if (i == 5) {
                gardenerServices.push('renovationClearence');

              } else if (i == 6) {
                gardenerServices.push('fertilizingPesticiding');

              }
              
            }
          }
          gardenerServices = JSON.stringify(gardenerServices);
        }


        // ##############################
        // PHOTOGRAPHER 
        // ##############################

        if (photographer == true || photographer != undefined) {

          metaData.push("photographer");

          //optional 
          
          var cameraModel = document.getElementById('cameraModel').value;
          var photographerAdditionalEvents = document.getElementById('photographerAdditionalEvents').value;
          var photographerQualifications = document.getElementById('photographerQualifications').value;


          var photographerBirthdays = document.getElementById('photographerBirthdays').checked;
          var photographerWeddings = document.getElementById('photographerWeddings').checked;
          var photographerParties = document.getElementById('photographerParties').checked;
          var photographerLiveinConcerts = document.getElementById('photographerLiveinConcerts').checked;
          var photographerBridalShowers = document.getElementById('photographerBridalShowers').checked;
          var photographerBabyShowers = document.getElementById('photographerBabyShowers').checked;
          var photographerCorporateEvents = document.getElementById('photographerCorporateEvents').checked;


          var photographerEventTypesTemp = [photographerBirthdays, photographerWeddings, photographerParties, photographerLiveinConcerts, photographerBridalShowers, photographerBabyShowers, photographerCorporateEvents];

          var photographerEventTypes = [];
          for (i = 0; i < photographerEventTypesTemp.length; i++) {
            if (photographerEventTypesTemp[i] == true) {
              if (i == 0) {
                
                photographerEventTypes.push('photographerBirthdays');
              } else if (i == 1) {
                photographerEventTypes.push('photographerWeddings');

              } else if (i == 2) {
                photographerEventTypes.push('photographerParties');

              } else if (i == 3) {
                photographerEventTypes.push('photographerLiveinConcerts');

              } else if (i == 4) {
                photographerEventTypes.push('photographerBridalShowers');

              } else if (i == 5) {
                photographerEventTypes.push('photographerBabyShowers');

              } else if (i == 6) {
                photographerEventTypes.push('photographerCorporateEvents');

              }
              
            }
          }
          photographerEventTypes = JSON.stringify(photographerEventTypes);
        }

        // ##############################
        // CARPENTER
        // ##############################

        if (carpenter == true || carpenter != undefined) {

          metaData.push("carpenter");

          //optional 
          var carpenterAdditionalServices = document.getElementById('carpenterAdditionalServices').value;


          var doors = document.getElementById('doors').checked;
          var windows = document.getElementById('windows').checked;
          var kitchenPantry = document.getElementById('kitchenPantry').checked;
          var cupboards = document.getElementById('cupboards').checked;
          var staircases = document.getElementById('staircases').checked;
          var fencing = document.getElementById('fencing').checked;
          var shelvingBookcases = document.getElementById('shelvingBookcases').checked;
     

          var carpenterServicesTemp = [doors,windows, kitchenPantry, cupboards, staircases, fencing, shelvingBookcases];

          var carpenterServices = [];
          for (i = 0; i < carpenterServicesTemp.length; i++) {
            if (carpenterServicesTemp[i] == true) {
              if (i == 0) {
                
                carpenterServices.push('doors');
              } else if (i == 1) {
                carpenterServices.push('windows');

              } else if (i == 2) {
                carpenterServices.push('kitchenPantry');

              } else if (i == 3) {
                carpenterServices.push('cupboards');

              } else if (i == 4) {
                carpenterServices.push('staircases');

              } else if (i == 5) {
                carpenterServices.push('fencing');

              } else if (i == 6) {
                carpenterServices.push('shelvingBookcases');

              }
              
              
            }
          }
          carpenterServices = JSON.stringify(carpenterServices);
        }


        // ##############################
        // PET CARETAKER 
        // ##############################

        if (petCaretaker == true || petCaretaker != undefined) {

          metaData.push("petCaretaker");

          //optional 

          var petCaretakerAdditionalPetTypes = document.getElementById('petCaretakerAdditionalPetTypes').value;
          var petCaretakerAdditionalServices = document.getElementById('petCaretakerAdditionalServices').value;

          //for pet types 
          var dog = document.getElementById('dog').checked;
          var cat = document.getElementById('cat').checked;
          var bird = document.getElementById('bird').checked;
          var rabbit = document.getElementById('rabbit').checked;
          var fish = document.getElementById('fish').checked;


          var petCaretakerPetTypesTemp = [dog, cat, bird, rabbit, fish];

          var petCaretakerPetTypes = [];
          for (i = 0; i < petCaretakerPetTypesTemp.length; i++) {
            if (petCaretakerPetTypesTemp[i] == true) {
              if (i == 0) {
                
                petCaretakerPetTypes.push('dog');
              } else if (i == 1) {
                petCaretakerPetTypes.push('cat');

              } else if (i == 2) {
                petCaretakerPetTypes.push('bird');

              } else if (i == 3) {
                petCaretakerPetTypes.push('rabbit');

              } else if (i == 4) {
                petCaretakerPetTypes.push('fish');

              } 
            }
          }
          petCaretakerPetTypes = JSON.stringify(petCaretakerPetTypes);


          //for services 

          var dailyWalk = document.getElementById('dailyWalk').checked;
          var vacationPetSitting = document.getElementById('vacationPetSitting').checked;
          var petTraining = document.getElementById('petTraining').checked;
          var petSittingByHour = document.getElementById('petSittingByHour').checked;
          var overnightPetSitting = document.getElementById('overnightPetSitting').checked;


          var petCaretakerServicesTemp = [dailyWalk, vacationPetSitting, petTraining, petSittingByHour, overnightPetSitting];

          var petCaretakerServices = [];
          for (i = 0; i < petCaretakerServicesTemp.length; i++) {
            if (petCaretakerServicesTemp[i] == true) {
              if (i == 0) {
                
                petCaretakerServices.push('dog');
              } else if (i == 1) {
                petCaretakerServices.push('cat');

              } else if (i == 2) {
                petCaretakerServices.push('bird');

              } else if (i == 3) {
                petCaretakerServices.push('rabbit');

              } else if (i == 4) {
                petCaretakerServices.push('fish');

              } 
            }
          }
          petCaretakerServices = JSON.stringify(petCaretakerServices);
        }

        // ##############################
        // BABYSITTER
        // ##############################

        if (babysitter == true || babysitter != undefined) {

          metaData.push("babysitter");

          //mandatory
          var babysitterVisitType = document.querySelector('input[name="radios10"]:checked').value;
          // var babysitterAgeFrom = document.getElementById('babysitterAgeFrom').value;
          // var babysitterAgeTo = document.getElementById('babysitterAgeTo').value;

          var babysitterAgeFrom = document.querySelector("[name='babysitterAgeFrom']").value;
          var babysitterAgeTo =  document.querySelector("[name='babysitterAgeTo']").value;

         
        }

        

        metaData = JSON.stringify(metaData);

        // alert(document.getElementById('myMulti1').value);
        // console.log($("#myMulti1").val())
        
       

        
        
        $.ajax({
          url: '../../../model/freelancer/freelancerSignupCreate.php',
          data: {
            freelancerNIC:freelancerNIC,
            freelancerEmail:freelancerEmail,
            freelancerPassword:freelancerPassword,
            fName:fName,
            lName:lName,
            gender:gender,
            phone:phone,
            experience:experience,
            address:address,
            doB:doB,
            workingHours_from:workingHours_from,
            workingHours_to:workingHours_to,
            
            workingDays: workingDays,

            metaData:metaData,

            // BEAUTICIAN

            beauticianServiceType:beauticianServiceType,
            beauticianGenderType:beauticianGenderType,
            beauticianQualifications:beauticianQualifications,
            beauticianAdditionalServices:beauticianAdditionalServices,          

            beauticianServices:beauticianServices,

            //PLUMBER

            plumberAdditionalServices:plumberAdditionalServices,
            plumberServices:plumberServices,

            //PAINTER
            painterAdditionalServices:painterAdditionalServices,
            painterServices:painterServices,

            //COOK
            cookVisitType:cookVisitType,
            cookQualifications:cookQualifications,
            cookAdditionalCuisines:cookAdditionalCuisines,          

            cookCuisines:cookCuisines,

            //TAILOR
            tailorGenderType:tailorGenderType,
            tailorAdditionalDressTypes:tailorAdditionalDressTypes,

            tailorDressTypes:tailorDressTypes,

            //EVENT ORGANIZER
            eventOrganizerAdditionalEvents:eventOrganizerAdditionalEvents,
            eventOrganizerAdditionalServices:eventOrganizerAdditionalServices,
            eventOrganizerQualifications:eventOrganizerQualifications,

            eventOrganizerEventTypes:eventOrganizerEventTypes,
            eventOrganizerServices:eventOrganizerServices,

            //HOMEFOOD MANUFACTURER
            homefoodAdditionalCuisines:homefoodAdditionalCuisines,
            homefoodQualifications:homefoodQualifications,

            homefoodCuisines:homefoodCuisines,

            //DRIVER 
            drivingLicense:drivingLicense,
            driverVehicleType:driverVehicleType,

            //PROGRAMMER
            programmerProjects:programmerProjects,
            programmerAdditionalLanguages:programmerAdditionalLanguages,
            programmerAdditionalFrameworks:programmerAdditionalFrameworks,
            programmerQualifications:programmerQualifications,

            programmerDevelopmentType:programmerDevelopmentType,
            programmerLanguages:programmerLanguages,
            programmerFrameworks:programmerFrameworks,

            //MASON
            masonAdditionalServices:masonAdditionalServices,

            masonServices:masonServices,

            //GRAPHIC DESIGNER 
            graphicDesignerAditionalTypes:graphicDesignerAditionalTypes,
            graphicDesignerQualifications:graphicDesignerQualifications,

            graphicDesignerDesignTypes:graphicDesignerDesignTypes,

            //MECHANIC 
            mechanicAdditionalServices:mechanicAdditionalServices,
            mechanicAdditionalVehicleTypes:mechanicAdditionalVehicleTypes,
            mechanicAdditionalVehicleBrands:mechanicAdditionalVehicleBrands,

            mechanicServices:mechanicServices,
            mechanicVehicleType:mechanicVehicleType,
            mechanicVehicleBrands:mechanicVehicleBrands,

            //CONTENT WRITER
            contentWriterPreviousWork:contentWriterPreviousWork,
            contentWriterAdditionalLanguages:contentWriterAdditionalLanguages,
            contentWriterQualifications:contentWriterQualifications,

            contentWriterLanguages:contentWriterLanguages,

            //HOUSEKEEPER 
            housekeeperVisitType:housekeeperVisitType,
            housekeeperComment:housekeeperComment,

            //ELDERLY CARETAKER 
            elderlyCaretakerVisitType:elderlyCaretakerVisitType,
            elderlyCaretakerGenderType:elderlyCaretakerGenderType,
            elderlyCaretakerAgeFrom:elderlyCaretakerAgeFrom,
            elderlyCaretakerAgeTo:elderlyCaretakerAgeTo,

            //GARDENER 
            gardenerVisitType:gardenerVisitType,
            gardenerAdditionalServices:gardenerAdditionalServices,
            gardenerQualifications:gardenerQualifications,

            gardenerServices:gardenerServices,

            //PHOTOGRAPHER 
            cameraModel:cameraModel,
            photographerAdditionalEvents:photographerAdditionalEvents,
            photographerQualifications:photographerQualifications,

            photographerEventTypes:photographerEventTypes,

            //CARPENTER
            carpenterAdditionalServices:carpenterAdditionalServices,
            carpenterServices:carpenterServices,

            //PET CARETAKER 
            petCaretakerAdditionalPetTypes:petCaretakerAdditionalPetTypes,
            petCaretakerAdditionalServices:petCaretakerAdditionalServices,

            petCaretakerPetTypes:petCaretakerPetTypes,
            petCaretakerServices:petCaretakerServices,

            //BABYSITTER 
            babysitterVisitType:babysitterVisitType,
            babysitterAgeFrom:babysitterAgeFrom,
            babysitterAgeTo:babysitterAgeTo

          },

          type: 'POST'
        }).done(function(resp) {
          // alert(resp);
          window.localStorage.clear();
    
          if (resp=='wrong_email') {
            alert("You already have an account with Ceylongig. Please try again with a new email");
            window.location.href = './freelancerSignup.php';
            
          }else if(resp=='wrong_nic'){
            alert("Your NIC already exists! Please use recover password option if you've forgotten your password" );
            window.location.href = './freelancerSignup.php';
          } 
          else {
            window.localStorage.setItem('freelancerPhone', phone);
            window.localStorage.setItem('freelancerEmail', freelancerEmail);

            alert("Thank you for signing up with Ceylongig! Please await the confirmation email to login!");

            window.location.href = '../login.php';
          }


        });
      });
    });
    
  </script>

    <script type="text/javascript">
      $(".previous").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li")
          .eq($("fieldset").index(current_fs))
          .removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate(
          { opacity: 0 },
          {
            step: function (now, mx) {
              //as the opacity of current_fs reduces to 0 - stored in "now"
              //1. scale previous_fs from 80% to 100%
              scale = 0.8 + (1 - now) * 0.2;
              //2. take current_fs to the right(50%) - from 0%
              left = (1 - now) * 50 + "%";
              //3. increase opacity of previous_fs to 1 as it moves in
              opacity = 1 - now;
              current_fs.css({ left: left });
              previous_fs.css({
                transform: "scale(" + scale + ")",
                opacity: opacity,
              });
            },
            duration: 800,
            complete: function () {
              current_fs.hide();
              animating = false;
            },
            //this comes from the custom easing plugin
            easing: "easeInOutBack",
          }
        );
      });

      function ABC() {
        // The following code will be enclosed within an anonymous function
        var foo = "Goodbye World!";
        console.log(foo);
        var script = document.createElement("script");
        script.onload = function () {
          //do stuff with the script
        };
        
        document.head.appendChild(script);
      
      /////////////////////////
      ////// Beautician //////
      ////////////////////////

      var BeauticianCode = ` <h2 class="fs-title">Beautician Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Service Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="beauticianHomeVisiting" name="radios1" value="HomeVisiting" checked required>
                <label for="beauticianHomeVisiting">Home-visiting Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="beauticianInSalon" name="radios1" value="InSalon">
                <label for="beauticianInSalon">In-Salon Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="beauticianVisitingEither" name="radios1" value="Either">
                <label for="beauticianVisitingEither">Either</label>
            </div> 
          </div>   
      </div>
      
      <textarea name="qualifications" id="beauticianQualifications" placeholder="Do you have any qualifications on Cosmetology and Beauty Culture? Type here!"></textarea>
      

    
      
      
        <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="hairTreatments" name="beauticianServices[]" value="hairTreatments" class="visitType" />
                <label for="hairTreatments">Hair Treatments</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="hairColoring" name="beauticianServices[]" value="hairColoring" class="visitType" />
                <label for="hairColoring">Hair Coloring</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="hairStyling" name="beauticianServices[]" value="hairStyling" class="visitType" />
                <label for="hairStyling">Hair Styling</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="bodyTreatments" name="beauticianServices[]" value="bodyTreatments" class="visitType" />
                <label for="bodyTreatments">Body Treatments</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="facialThreading" name="beauticianServices[]" value="facialThreading" class="visitType" />
                <label for="facialThreading">Facial Threading</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="facials" name="beauticianServices[]" value="facials" class="visitType" />
                <label for="facials">Facials</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="bodyWaxing" name="beauticianServices[]" value="bodyWaxing" class="visitType" />
                <label for="bodyWaxing">Body Waxing</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="manicure" name="beauticianServices[]" value="manicure" class="visitType" />
                <label for="manicure">Manicure</label>
              </div> 
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="pedicure" name="beauticianServices[]" value="pedicure" class="visitType" />
                <label for="pedicure">Pedicure</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="fullDressing" name="beauticianServices[]" value="fullDressing" class="visitType" />
                <label for="fullDressing">Full Dressing</label>
              </div> 
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="makeup" name="beauticianServices[]" value="makeup" class="visitType" />
                <label for="makeup">Makeup</label>
              </div> 
            </div>  
          </div>
      </div>
      
      <textarea name="additionalServices" id="beauticianAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2"  style="margin-bottom:-1cm" for="genderType">Gender Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="beauticianMale" name="radios2" value="Male" checked required>
                <label for="beauticianMale">Male Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="beauticianFemale" name="radios2" value="Female">
                <label for="beauticianFemale">Female Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="beauticianUnisex" name="radios2" value="Unisex">
                <label for="beauticianUnisex">Unisex</label>
            </div> 
          </div>   
      </div>
      <br>
      <br>
      <br>
      
        <hr>
    

      
      
      <br>`;

      ////////////////////
      /////// PLUMBER/////
      ////////////////////

      var PlumberCode = ` <h2 class="fs-title">Plumber Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
          
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="leakRepair" name="plumberServices[]" value="leakRepair" class="visitType" />
                <label for="leakRepair">Leak Repair</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="sewerRepair" name="plumberServices[]" value="sewerRepair" class="visitType" />
                <label for="sewerRepair">Sewer Repair</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="toiletRepair" name="plumberServices[]" value="toiletRepair" class="visitType" />
                <label for="toiletRepair">Toilet Repair</label>
              </div>
            </div>
                     
            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="drainCleaning" name="plumberServices[]" value="drainCleaning" class="visitType" />
                <label for="drainCleaning">Drain Cleaning</label>
              </div> 
              <div style="float: left; width: 43%; margin-right: 15mm;">
                <input type="checkbox" id="waterHeaterInstallation" name="plumberServices[]" value="waterHeaterInstallation" class="visitType" style="width:200px" />
                <label style="width:200px" for="waterHeaterInstallation">Water Heater Installation</label>
              </div> 
            </div>  
          </div>
      </div>

      <textarea name="comment" name="plumberAdditionalServices" id="plumberAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>
      <br><br>

      <hr><br>`; 


      /////////////////////////
      ////// Painter //////
      ////////////////////////

      var PainterCode = ` <h2 class="fs-title">Painter Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
       
      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="furniturePainting" name="painterServices[]" value="furniturePainting" class="visitType" />
                <label for="furniturePainting">Furniture Painting</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="wallPainting" name="painterServices[]" value="wallPainting" class="visitType" />
                <label for="wallPainting">Wall Painting</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="ceilingPainting" name="painterServices[]" value="ceilingPainting" class="visitType" />
                <label for="ceilingPainting">Ceiling Painting</label>
              </div>
            </div>
          </div>         
      </div>

      <textarea name="painterAdditionalServices" id="painterAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>
      <br><br>

      <hr><br>`;
      
      //////////////////
      ////// COOK //////
      //////////////////

      var CookCode = ` <h2 class="fs-title">Cook Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      
      <br><br>
      
     
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Cuisines:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="sriLankan" name="cookCuisines[]" value="sriLankan" class="visitType" />
                <label for="sriLankan">Sri Lankan</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="northIndian" name="cookCuisines[]" value="northIndian" class="visitType" />
                <label for="northIndian">North Indian</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="southIndian" name="cookCuisines[]" value="southIndian" class="visitType" />
                <label for="southIndian">South Indian</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="chinese" name="cookCuisines[]" value="chinese" class="visitType" />
                <label for="chinese">Chinese</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="arab" name="cookCuisines[]" value="arab" class="visitType" />
                <label for="arab">Arab</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="thai" name="cookCuisines[]" value="thai" class="visitType" />
                <label for="thai">Thai</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="italian" name="cookCuisines[]" value="italian" class="visitType" />
                <label for="italian">Italian</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="healthyFood" name="cookCuisines[]" value="healthyFood" class="visitType" />
                <label for="healthyFood">Healthy Food</label>
              </div> 
              
            </div>

            
          </div>
      </div>


      <textarea name="cookAdditionalCuisines"  id="cookAdditionalCuisines" placeholder="Haven't we listed the cuisines you offer? Type here!"></textarea>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Visit Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="cookHomeVisiting" name="radios3" value="HomeVisiting" checked required>
                <label for="cookHomeVisiting">Home-Visiting Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="cookPreCooked" name="radios3" value="PreCooked">
                <label for="cookPreCooked">Pre-Cooked Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="cookEither" name="radios3" value="Either">
                <label for="cookEither">Either</label>
            </div> 
          </div>   
      </div>

      <textarea name="cookQualifications" id="cookQualifications" rows="3" placeholder="Do you have any qualifications on Hospitality/Catering/Cookery/Culinary Arts? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ///////////////////
      //////TAILOR///////
      ///////////////////

      var TailorCode = ` <h2 class="fs-title">Tailor Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      
    
      
   
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Dress Types:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="girlsSchoolUniform" name="tailorDressTypes[]" value="girlsSchoolUniform" class="visitType"  style="width:140px"/>
                <label style="width:140px" for="girlsSchoolUniform">Girls' School Uniform</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="boyschoolUniform" name="tailorDressTypes[]" value="boyschoolUniform" class="visitType" style="width:140px"/>
                <label style="width:140px" for="boyschoolUniform">Boys' School Uniform</label>
              </div>
              <div style="float: left; width: 20%;">
                <input type="checkbox" id="gentsSuits" name="tailorDressTypes[]" value="gentsSuits" class="visitType" />
                <label for="gentsSuits">Gents' Suits</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="sareeJackets" name="tailorDressTypes[]" value="sareeJackets" class="visitType" />
                <label for="sareeJackets">Saree Jackets</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="bridalDresses" name="tailorDressTypes[]" value="bridalDresses" class="visitType" />
                <label for="bridalDresses">Bridal Dresses</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="ladiesDresses" name="tailorDressTypes[]" value="ladiesDresses" class="visitType" />
                <label for="ladiesDresses">Ladies' Dresses</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="kidsDresses" name="tailorDressTypes[]" value="kidsDresses" class="visitType" />
                <label for="kidsDresses">Kids' Dresses</label>
              </div> 
              
            </div>

            
          </div>
      </div>


      <textarea id="tailorAdditionalDressTypes" name="tailorAdditionalDressTypes" placeholder="Do you offer something we haven't listed? Type here!"></textarea>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" style="margin-bottom:-1cm" for="genderType">Gender Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="tailorMale" name="radios4" value="Male" checked>
                <label for="tailorMale">Male Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="tailorFemale" name="radios4" value="Female">
                <label for="tailorFemale">Female Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="tailorUnisex" name="radios4" value="Unisex">
                <label for="tailorUnisex">Unisex</label>
            </div> 
          </div>   
      </div>


      <br><br>

      <hr><br>`;


      //////////////////////////
      ///////EVENT ORGANIZER////
      //////////////////////////

      var EventOrganizerCode = ` <h2 class="fs-title">Event Organizer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
       
      
      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Types of Events:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="birthdays" name="eventOrganizerEventTypes[]" value="birthdays" class="visitType" />
                <label for="birthdays">Birthdays</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="weddings" name="eventOrganizerEventTypes[]" value="weddings" class="visitType" />
                <label for="weddings">Weddings</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="parties" name="eventOrganizerEventTypes[]" value="parties" class="visitType" />
                <label for="parties">Parties</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="liveinConcerts" name="eventOrganizerEventTypes[]" value="liveinConcerts" class="visitType" />
                <label for="liveinConcerts">Live-in Concerts</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="bridalShowers" name="eventOrganizerEventTypes[]" value="bridalShowers" class="visitType" />
                <label for="bridalShowers">Bridal Showers</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="babyShowers" name="eventOrganizerEventTypes[]" value="babyShowers" class="visitType" />
                <label for="babyShowers">Baby Showers</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="corporateEvents" name="eventOrganizerEventTypes[]" value="corporateEvents" class="visitType" />
                <label for="corporateEvents">Corporate Events</label>
              </div> 
             
            </div>

            
          </div>
      </div>

      <textarea name="eventOrganizerAdditionalEvents" id="eventOrganizerAdditionalEvents" placeholder="Have we missed any events you undertake? Type here!"></textarea>
      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 20%;margin-right: 15mm;">
                <input type="checkbox" id="foodAndBeverage" name="eventOrganizerServices[]" value="foodAndBeverage" class="visitType" style="width:133px"/>
                <label style="width:133px" for="foodAndBeverage">Food and Beverage</label>
              </div>
              <div style="float: left; width: 15%;margin-right: 15mm;">
                <input type="checkbox" id="decor" name="eventOrganizerServices[]" value="decor" class="visitType" style="width:70px"/>
                <label style="width:100px" for="decor">Décor</label>
              </div>
              
              <div style="float: left; width: 33%;">
                <input type="checkbox" id="transportationManagement" name="eventOrganizerServices[]" value="transportationManagement" class="visitType" style="width:200px" />
                <label style="width:200px" for="transportationManagement">Transportation Management</label>
              </div>

            </div>

            <div>
              <div style="float: left; width: 20%; margin-right:15mm">
                <input type="checkbox" id="siteSelection" name="eventOrganizerServices[]" value="siteSelection" class="visitType" style="width:120px"/>
                <label style="width:120px" for="siteSelection">Site Selection</label>
              </div> 
               
              <div style="float: left; width: 20%; margin-right: 15mm;">
                <input type="checkbox" id="budgetPlanning" name="beauticianServices[]" value="budgetPlanning" class="visitType" />
                <label for="budgetPlanning">Budget Planning</label>
              </div> 
              <div style="float: left; width: 28%;">
                <input type="checkbox" id="attendeeRegistration" name="eventOrganizerServices[]" value="attendeeRegistration" class="visitType" style="width:180px"/>
                <label style="width:180px" for="attendeeRegistration">Attendee Registration</label>
              </div>
              
            </div>

            <div>
              <div style="float: left; width: 43%;margin-right: 15mm;">
                  <input type="checkbox" id="speakerDelegateCoordination" name="eventOrganizerServices[]" value="speakerDelegateCoordination" class="visitType" style="width:220px"/>
                  <label style="width:220px" for="speakerDelegateCoordination">Speaker/Delegate Coordination</label>
              </div>
              
              
            </div>

            
          </div>
      </div>


      <textarea name="eventOrganizerAdditionalServices"  id="eventOrganizerAdditionalServices" placeholder="Have we missed any serives you offer? Type here!"></textarea>
      <textarea name="eventOrganizerQualifications" id="eventOrganizerQualifications" placeholder="Do you have any qualifications or experience in Event Planning? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      /////////////////////////
      /////////HOMEFOOD////////
      /////////////////////////

      var HomefoodCode = ` <h2 class="fs-title">Homefood Manufacturer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
          
      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Cuisines:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="homefoodSriLankan" name="homefoodCuisines[]" value="sriLankan" class="visitType" />
                <label for="homefoodSriLankan">Sri Lankan</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="homefoodNorthIndian" name="homefoodCuisines[]" value="northIndian" class="visitType" />
                <label for="homefoodNorthIndian">North Indian</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="homefoodSouthIndian" name="homefoodCuisines[]" value="southIndian" class="visitType" />
                <label for="homefoodSouthIndian">South Indian</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="homefoodChinese" name="homefoodCuisines[]" value="chinese" class="visitType" />
                <label for="homefoodChinese">Chinese</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="homefoodArab" name="homefoodCuisines[]" value="arab" class="visitType" />
                <label for="homefoodArab">Arab</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="homefoodThai" name="homefoodCuisines[]" value="thai" class="visitType" />
                <label for="homefoodThai">Thai</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="homefoodItalian" name="homefoodCuisines[]" value="italian" class="visitType" />
                <label for="homefoodItalian">Italian</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="homefoodHealthyFood" name="homefoodCuisines[]" value="healthyFood" class="visitType" />
                <label for="homefoodHealthyFood">Healthy Food</label>
              </div> 
              
            </div>

            
          </div>
      </div>


      <textarea name="homefoodAdditionalCuisines"  id="homefoodAdditionalCuisines" placeholder="Haven't we listed the cuisines you offer? Type here!"></textarea>
      <textarea name="homefoodQualifications" id="homefoodQualifications" rows="3" placeholder="Do you have any qualifications on Hospitality/Catering/Cookery/Culinary Arts? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ///////////////////////
      ////////DRIVER/////////
      ///////////////////////

      var DriverCode = ` <h2 class="fs-title">Driver Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <input type="text" name="drivingLicense"  id="drivingLicense" placeholder="Driving License Number* " required/>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Vehicle Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="driverHeavy" name="radios5" value="heavyVehicle" checked required>
                <label for="driverHeavy">Heavy Vehicle Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="driverLight" name="radios5" value="lightVehicle">
                <label for="driverLight">Light Vehicle Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="driverEither" name="radios5" value="either">
                <label for="driverEither">Either</label>
            </div> 
          </div>   
      </div>
      <br>
      <br><br>

      <hr><br>`;

      /////////////////////////
      ///////PROGRAMMER////////
      /////////////////////////

      var ProgrammerCode = ` <h2 class="fs-title">Programmer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      
      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Development Type* :</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 12mm;">
                <input type="checkbox" id="webDeveloper" name="programmerDevelopmentType[]" value="webDeveloper" class="visitType" />
                <label for="webDeveloper">Web Developer</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 12mm;">
                <input type="checkbox" id="backEndDeveloper" name="programmerDevelopmentType[]" value="hairCbackEndDeveloperoloring" class="visitType" />
                <label for="backEndDeveloper">Back-End Developer</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="mobileDeveloper" name="programmerDevelopmentType[]" value="mobileDeveloper" class="visitType" />
                <label for="mobileDeveloper">Mobile Developer</label>
              </div>
            </div>
          </div>
      </div>

      <br>  
      <textarea name="programmerProjects" id="programmerProjects" placeholder="Tell us about projects you've done, if any"></textarea>
     
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Languages:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">

            <div style="float:left">
              <div style="float: left;width: 13%; margin-right: 5mm;">
                <input type="checkbox" id="java" name="programmerLanguages[]" value="java" class="visitType" style="width:60px" />
                <label style="width:60px" for="java" >Java</label>
              </div>
              <div style="float: left; width: 13%;margin-right: 5mm;">
                <input type="checkbox" id="c++" name="programmerLanguages[]" value="c++" class="visitType" style="width:60px" />
                <label style="width:60px" for="c++">C++</label>
              </div>
              <div style="float: left;width: 13%; margin-right: 5mm;">
                <input type="checkbox" id="python" name="programmerLanguages[]" value="python" class="visitType" style="width:60px"/>
                <label style="width:60px" for="python">Python</label>
              </div>
              <div style="float: left;width: 13%;margin-right: 5mm; ">
                <input type="checkbox" id="c#" name="programmerLanguages[]" value="c#" class="visitType" style="width:60px" />
                <label style="width:60px" for="c#">C#</label>
              </div>
              <div style="float: left; width: 13%;margin-right: 5mm;">
                <input type="checkbox" id="javascript" name="programmerLanguages[]" value="javascript" class="visitType" style="width:90px" />
                <label style="width:90px" for="javascript">JavaScript </label>
              </div>
              
            </div>
            
          </div>
      </div>


      <textarea name="programmerAdditionalLanguages" id="programmerAdditionalLanguages" placeholder="Haven't we listed the languages you use? Type here!"></textarea>

      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Frameworks:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 15%;margin-right: 12mm;">
                <input type="checkbox" id="angular" name="programmerFrameworks[]" value="angular" class="visitType" style="width:100px"/>
                <label style="width:100px" for="angular">Angular</label>
              </div>
              <div style="float: left;width: 15%; margin-right: 12mm;">
                <input type="checkbox" id="vue" name="programmerFrameworks[]" value="vue" class="visitType" style="width:100px" />
                <label style="width:100px" for="vue">Vue.js</label>
              </div>
              <div style="float: left;width: 15%; margin-right: 12mm;">
                <input type="checkbox" id="laravel" name="programmerFrameworks[]" value="laravel" class="visitType" style="width:100px" />
                <label style="width:100px" for="laravel">Laravel</label>
              </div>
              <div style="float: left;width: 15%; ">
                <input type="checkbox" id="react" name="programmerFrameworks[]" value="react" class="visitType" style="width:100px"/>
                <label style="width:100px" for="react">React</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 15%; margin-right:15mm">
                <input type="checkbox" id="reactNative" name="programmerFrameworks[]" value="reactNative" class="visitType" />
                <label style="width:100px" for="reactNative">React Native</label>
              </div> 
              <div style="float: left; width: 15%; margin-right: 15mm;">
                <input type="checkbox" id="springboot" name="programmerFrameworks[]" value="springboot" class="visitType" />
                <label style="width:100px" for="springboot"> Springboot</label>
              </div> 
              <div style="float: left; width: 15%;">
                <input type="checkbox" id="flutter" name="programmerFrameworks[]" value="flutter" class="visitType" />
                <label style="width:100px"  for="flutter">Flutter</label>
              </div>
            </div>
          </div>
      </div>



      <textarea name="programmerAdditionalFrameworks" id="programmerAdditionalFrameworks" placeholder="Haven't we listed the frameworks you use? Type here!"></textarea>

      <textarea name="programmerQualifications" id="programmerQualifications" placeholder="Do you have any qualifications on Proramming and Development? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ////////////////////////////
      ////////MASON///////////////
      ////////////////////////////

      var MasonCode = ` <h2 class="fs-title">Mason Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      <br>  
      
      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="houses" name="masonServices[]" value="houses" class="visitType" />
                <label for="houses">Houses</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="bathrooms" name="masonServices[]" value="bathrooms" class="visitType" />
                <label for="bathrooms">Bathrooms</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="corporateBuildings" name="masonServices[]" value="corporateBuildings" class="visitType" style="width:140px"/>
                <label style="width:140px" for="corporateBuildings">Corporate Buildings</label>
              </div>
            </div>

             
          </div>
      </div>

      <textarea name="masonAdditionalServices" id="masonAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>
      
      <br><br>

      <hr><br>`;

      ///////////////////////////////////
      //////////GRAPHIC DESIGNER/////////
      ///////////////////////////////////

      var GraphicDesignerCode = ` <h2 class="fs-title">Graphic Designer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Types of Designing:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 13mm;">
                <input type="checkbox" id="bannerDesigning" name="graphicDesignerDesignTypes[]" value="bannerDesigning" class="visitType" />
                <label for="bannerDesigning">Banner Designing</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 13mm;">
                <input type="checkbox" id="souvenirDesigning" name="graphicDesignerDesignTypes[]" value="souvenirDesigning" class="visitType" />
                <label for="souvenirDesigning">Souvenir Designing</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="tShirtDesigning" name="graphicDesignerDesignTypes[]" value="tShirtDesigning" class="visitType" />
                <label for="tShirtDesigning">T-ShirtDesigning</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="socialMediaPostDesigning" name="graphicDesignerDesignTypes[]" value="socialMediaPostDesigning" class="visitType" style="width:200px"/>
                <label style="width:200px" for="socialMediaPostDesigning">Social Media Post Designing</label>
              </div> 
              
            </div>  
          </div>
      </div>

      <textarea name="graphicDesignerAditionalTypes" id="graphicDesignerAditionalTypes" placeholder="Haven't we listed what you offer? Type here!"></textarea>
      <textarea name="graphicDesignerQualifications" id="graphicDesignerQualifications" placeholder="Do you have any qualifications on Graphic Designing? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      //////////////////////////////
      /////////MECHANIC/////////////
      //////////////////////////////
      
      var MechanicCode = ` <h2 class="fs-title">Mechanic Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="brakeRepair" name="mechanicServices[]" value="brakeRepair" class="visitType" />
                <label for="brakeRepair">Brake Repair</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="diagnostics" name="mechanicServices[]" value="diagnostics" class="visitType" />
                <label for="diagnostics">Diagnostics</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="tireRotation" name="mechanicServices[]" value="tireRotation" class="visitType" />
                <label for="tireRotation">Tire Rotation</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="oilChange" name="mechanicServices[]" value="oilChange" class="visitType" />
                <label for="oilChange">Oil Change</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="tuneup" name="mechanicServices[]" value="tuneup" class="visitType" />
                <label for="tuneup">Tune-up</label>
              </div> 
              
            </div> 
          </div>
      </div>

      <textarea name="mechanicAdditionalServices" id="mechanicAdditionalServices" placeholder="Havne't we listed any service you offer? Type here!"></textarea>


      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Vehicle Type:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 13%;margin-right: 15mm;">
                <input type="checkbox" id="motorcycle" name="mechanicVehicleType[]" value="motorcycle" class="visitType" style="width:100px"/>
                <label style="width:100px" for="motorcycle">Motorcycle</label>
              </div>
              <div style="float: left; width: 10%;margin-right: 12mm;">
                <input type="checkbox" id="car" name="mechanicVehicleType[]" value="car" class="visitType" style="width:80px" />
                <label style="width:80px" for="car">Car</label>
              </div>
              <div style="float: left; width: 10%;margin-right: 12mm;">
                <input type="checkbox" id="van" name="mechanicVehicleType[]" value="van" class="visitType" style="width:80px" />
                <label style="width:80px" for="van">Van</label>
              </div>
              <div style="float: left; width: 10%; margin-right:12mm">
                <input type="checkbox" id="bus" name="mechanicVehicleType[]" value="bus" class="visitType" style="width:80px" />
                <label style="width:80px" for="bus">Bus</label>
              </div> 
              <div style="float: left; width: 13%;">
                <input type="checkbox" id="truck" name="mechanicVehicleType[]" value="truck" class="visitType" style="width:90px"/>
                <label style="width:90px" for="truck">Truck</label>
              </div> 
            </div>

          </div>
      </div>

      <textarea name="mechanicAdditionalVehicleTypes"  id="mechanicAdditionalVehicleTypes" placeholder="Do you provide service to any other types of vehicles? Type here!"></textarea>

      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Vehicle Brands:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="toyota" name="mechanicVehicleBrands[]" value="toyota" class="visitType" />
                <label for="toyota">Toyota</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="honda" name="mechanicVehicleBrands[]" value="honda" class="visitType" />
                <label for="honda">Honda</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="nissan" name="mechanicVehicleBrands[]" value="nissan" class="visitType" />
                <label for="nissan">Nissan</label>
              </div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="suzuki" name="mechanicVehicleBrands[]" value="suzuki" class="visitType" />
                <label for="suzuki">Suzuki</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="mercedes" name="mechanicVehicleBrands[]" value="mercedes" class="visitType" />
                <label for="mercedes">Mercedes</label>
              </div> 
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="tvs" name="mechanicVehicleBrands[]" value="tvs" class="visitType" />
                <label for="tvs">TVS</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="bajaj" name="mechanicVehicleBrands[]" value="bajaj" class="visitType" />
                <label for="bajaj">Bajaj</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="yamaha" name="mechanicVehicleBrands[]" value="yamaha" class="visitType" />
                <label for="yamaha">Yamaha</label>
              </div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="audi" name="mechanicVehicleBrands[]" value="audi" class="visitType" />
                <label for="audi">Audi</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="mitsubishi" name="mechanicVehicleBrands[]" value="mitsubishi" class="visitType" />
                <label for="mitsubishi">Mitsubishi</label>
              </div> 
            </div>
          </div>
      </div>


      <textarea name="mechanicAdditionalVehicleBrands" id="mechanicAdditionalVehicleBrands" placeholder="Do you provide service to vehicles of any other brands? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ///////////////////////////
      ////////CONTENT WRITER/////
      ///////////////////////////

      var ContentWriterCode = ` <h2 class="fs-title">Content Writer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <textarea name="contentWriterPreviousWork" id="contentWriterPreviousWork" placeholder="Tell us about any content writing tasks you've undertaken before"></textarea>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Languages:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="sinhalese" name="contentWriterLanguages[]" value="sinhalese" class="visitType" />
                <label for="sinhalese">Sinhalese</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="english" name="contentWriterLanguages[]" value="english" class="visitType" />
                <label for="english">English</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="tamil" name="contentWriterLanguages[]" value="tamil" class="visitType" />
                <label for="tamil">Tamil</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="hindi" name="contentWriterLanguages[]" value="hindi" class="visitType" />
                <label for="hindi">Hindi</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="japanese" name="contentWriterLanguages[]" value="japanese" class="visitType" />
                <label for="japanese">Japanese</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="arabic" name="contentWriterLanguages[]" value="arabic" class="visitType" />
                <label for="arabic">Arabic</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="german" name="contentWriterLanguages[]" value="german" class="visitType" />
                <label for="german">German</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="korean" name="contentWriterLanguages[]" value="korean" class="visitType" />
                <label for="korean">Korean</label>
              </div> 
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="french" name="contentWriterLanguages[]" value="french" class="visitType" />
                <label for="french">French</label>
              </div>
            </div>

            
          </div>
      </div>

      <textarea name="contentWriterAdditionalLanguages" id="contentWriterAdditionalLanguages" placeholder="Do you use any other languages? Type here!"></textarea>
      <textarea name="contentWriterQualifications" id="contentWriterQualifications" placeholder="Do you have any qualifications on Content Writing? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ////////////////////////////////////////
      /////////HOUSEKEEPER////////////////////
      ////////////////////////////////////////

      var HousekeeperCode = ` <h2 class="fs-title">Housekeeper Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Visit Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="housekeeperLiveIn" name="radios6" value="live-in" checked required>
                <label for="housekeeperLiveIn">Live-in Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="housekeeperDailyVisiting" name="radios6" value="daily-visiting">
                <label for="housekeeperDailyVisiting">Daily-Visiting Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="housekeeperEither" name="radios6" value="either">
                <label for="housekeeperEither">Either</label>
            </div> 
          </div>   
      </div>
      
      <textarea name="housekeeperComment" id="housekeeperComment" placeholder="Anything else you want to add to your profile? Type here!"></textarea>
      
      <br><br>

      <hr><br>`;

      ////////////////////////////////
      ///////////ELDERLY CARETAKER////
      ////////////////////////////////

      var ElderlyCaretakerCode = ` <h2 class="fs-title">Elderly Caretaker Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Visit Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="elderlyCaretakerLiveIn" name="radios7" value="live-in" checked required>
                <label for="elderlyCaretakerLiveIn">Live-in Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="elderlyCaretakerDailyVisiting" name="radios7" value="daily-visiting">
                <label for="elderlyCaretakerDailyVisiting">Daily-Visiting Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="elderlyCaretakerEither" name="radios7" value="either">
                <label for="elderlyCaretakerEither">Either</label>
            </div> 
          </div> 
      </div>


      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" style="margin-bottom:-1cm" for="genderType">Gender Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="elderlyCaretakerMale" name="radios8" value="male" checked required>
                <label for="elderlyCaretakerMale">Male Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="elderlyCaretakerFemale" name="radios8" value="female">
                <label for="elderlyCaretakerFemale">Female Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="elderlyCaretakerGenderEither" name="radios8" value="either">
                <label for="elderlyCaretakerGenderEither">Either</label>
            </div> 
          </div> 
      </div>


      <div style="width:100%; margin-bottom:3cm">
          <div style="float: left; padding-top: 6mm">
            <label class="label2"  style="margin-bottom:-1cm;" for="elderly-age">Age Range* :</label>
          </div>
          <div style="float: left; padding-top: 6mm">
            <label class="label2" style="margin-bottom:-1cm;" for="elderlyCaretakerAgeFrom"> From </label>
          </div>
          <div style="width: 30%; float: left; padding-top: 4mm">
            <input type="number" style="margin-bottom:-1cm;" id="elderlyCaretakerAgeFrom" name="elderlyCaretakerAgeFrom" min="40" max="100" step="5" required>
          </div>
          <div style="float: left; padding-top: 6mm">
            <label class="label2" style="margin-bottom:-1cm;" for="elderlyCaretakerAgeTo"> To </label>
          </div>
          <div style="width: 30%; float: left; padding-top: 4mm">
            <input type="number" style="margin-bottom:-1cm;" id="elderlyCaretakerAgeTo" name="elderlyCaretakerAgeTo" min="40" max="100" step="5" required>
          </div>
      </div> 
      
      
      <br><br>

      <hr>
      
      <br>`;

      //////////////////////////////
      /////GARDENER/////////////////
      //////////////////////////////

      var GardenerCode = ` <h2 class="fs-title">Gardener Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      
      
      

      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="landscaping" name="gardenerServices[]" value="landscaping" class="visitType" />
                <label for="landscaping">Landscaping</label>
              </div>
              
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="hedgeTrimming" name="gardenerServices[]" value="hedgeTrimming" class="visitType" />
                <label for="hedgeTrimming">Hedge Trimming</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="vegetableGrowing" name="gardenerServices[]" value="vegetableGrowing" class="visitType" />
                <label for="vegetableGrowing">Vegetable Growing</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 15%;margin-right: 12mm;">
                <input type="checkbox" id="weeding" name="gardenerServices[]" value="weeding" class="visitType" style="width:100px" />
                <label style="width:100px" for="weeding">Weeding</label>
              </div>
              
              <div style="float: left; width: 15%;margin-right: 12mm;">
                <input type="checkbox" id="pruning" name="gardenerServices[]" value="pruning" class="visitType" style="width:100px" />
                <label style="width:100px" for="pruning">Pruning</label>
              </div> 
              <div style="float: left; width: 33%;">
                <input type="checkbox" id="renovationClearence" name="gardenerServices[]" value="renovationClearence" class="visitType" style="width:220px"/>
                <label style="width:220px" for="renovationClearence">Renovation and Clearence</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 40%; margin-right: 15mm;">
                <input type="checkbox" id="fertilizingPesticiding" name="gardenerServices[]" value="fertilizingPesticiding" class="visitType" style="width:250px"/>
                <label style="width:250px" for="fertilizingPesticiding">Fertilizing and Applying Pesticides</label>
              </div> 
              
            </div>

           
      </div>


      <textarea name="gardenerAdditionalServices" id="gardenerAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>


      <div>
        <div style="float: left; margin-top: 1mm;">
          <label class="label2" for="serviceType">Visit Type* :</label>
        </div>
        <div class="radio-toolbar" style="float:left; width: 70%">
          <div style="float: left; width: 23%;margin-right: 15mm;">
            <input type="radio" id="gardenerLiveIn" name="radios9" value="live-in" checked required>
            <label for="gardenerLiveIn">Live-in Only</label>
          </div>
          <div style="float: left; width: 23%;margin-right: 15mm;">
            <input type="radio" id="gardenerDailyVisiting" name="radios9" value="daily-visiting">
            <label for="gardenerDailyVisiting">Daily-Visiting Only</label>
          </div>
          <div style="float: left; width: 23%;">
            <input type="radio" id="gardenerEither" name="radios9" value="either">
            <label for="gardenerEither">Either</label>
          </div> 
        </div>   
      </div>
      

      <textarea name="gardenerQualifications" id="gardenerQualifications" placeholder="Do you have any qualifications on Landscaping? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      

      ///////////////////////////////////////
      ///////PHOTOGRAPHER////////////////////
      ///////////////////////////////////////

      var PhotographerCode = ` <h2 class="fs-title">Photographer Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>

      <textarea name="cameraModel" id="cameraModel" placeholder="Camera Models"></textarea>



      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Types of Events:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="photographerBirthdays" name="photographerEventTypes[]" value="birthdays" class="visitType" />
                <label for="photographerBirthdays">Birthdays</label>
              </div>
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="photographerWeddings" name="photographerEventTypes[]" value="weddings" class="visitType" />
                <label for="photographerWeddings">Weddings</label>
              </div>
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="photographerParties" name="photographerEventTypes[]" value="parties" class="visitType" />
                <label for="photographerParties">Parties</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="photographerLiveinConcerts" name="photographerEventTypes[]" value="liveinConcerts" class="visitType" />
                <label for="photographerLiveinConcerts">Live-in Concerts</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="photographerBridalShowers" name="photographerEventTypes[]" value="bridalShowers" class="visitType" />
                <label for="photographerBridalShowers">Bridal Showers</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="photographerBabyShowers" name="photographerEventTypes[]" value="babyShowers" class="visitType" />
                <label for="photographerBabyShowers">Baby Showers</label>
              </div>
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="photographerCorporateEvents" name="photographerEventTypes[]" value="corporateEvents" class="visitType" />
                <label for="photographerCorporateEvents">Corporate Events</label>
              </div> 
             
            </div>

            
          </div>
      </div>


      <textarea name="photographerAdditionalEvents" id="photographerAdditionalEvents" placeholder="Haven't we listed what you the events you cover? Type here!"></textarea>
      <textarea name="photographerQualifications" id="photographerQualifications" placeholder="Do you have any qualifications on Photography? Type here!"></textarea>
      <br><br>

      <hr><br>`;

      ///////////////////////////////////
      ////////////CARPENTER//////////////
      ///////////////////////////////////


      var CarpenterCode = ` <h2 class="fs-title">Carpenter Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
       
      
      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              
              <div style="float: left; width: 17%;margin-right: 15mm;">
                <input type="checkbox" id="doors" name="carpenterServices[]" value="doors" class="visitType" style="width:100px"/>
                <label style="width:100px" for="doors">Doors</label>
              </div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="windows" name="carpenterServices[]" value="windows" class="visitType" />
                <label for="windows">Windows</label>
              </div>
              <div style="float: left; width: 23%; margin-right: 15mm;">
                <input type="checkbox" id="kitchenPantry" name="carpenterServices[]" value="kitchenPantry" class="visitType" />
                <label for="kitchenPantry">Kitchen Pantry</label>
              </div> 
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="cupboards" name="carpenterServices[]" value="cupboards" class="visitType" />
                <label for="cupboards">Cupboards</label>
              </div> 
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="staircases" name="carpenterServices[]" value="staircases" class="visitType" />
                <label for="staircases">Staircases</label>
              </div> 
              <div style="float: left; width: 23%;">
                <input type="checkbox" id="fencing" name="carpenterServices[]" value="fencing" class="visitType" />
                <label for="fencing">Fencing</label>
              </div>
            </div>

            <div>
              
              <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="checkbox" id="shelvingBookcases" name="carpenterServices[]" value="ShelvingandBookcases" class="visitType" style="width:180px" />
                <label style="width:180px" for="shelvingBookcases">Shelving and Bookcases</label>
              </div>
             
            </div>

            
          </div>
      </div>


      <textarea name="carpenterAdditionalServices" id="carpenterAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>

      <br><br>

      <hr><br>`;

      //////////////////////////////////
      //////////PET CARETAKER///////////
      //////////////////////////////////

      var PetCaretakerCode = ` <h2 class="fs-title">Pet Caretaker Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Pet Type:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float: left; width: 23%;margin-right: 15mm;">
              <input type="checkbox" id="dog" class="visitType" name=petCaretakerPetTypes[] value="dog" style="width:100px"/>
              <label style="width:100px" for="dog">Dog</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
              <input type="checkbox" id="cat" class="visitType" name=petCaretakerPetTypes[] value="cat" style="width:100px"/>
              <label style="width:100px" for="cat">Cat</label>
            </div>
            <div style="float: left; width: 23%;">
              <input type="checkbox" id="bird" class="visitType" name=petCaretakerPetTypes[] value="bird" style="width:100px" />
              <label style="width:100px" for="bird">Bird</label>
            </div> 
            <div style="float: left; width: 23%; margin-right:1.5cm">
              <input type="checkbox" id="rabbit" class="visitType" name=petCaretakerPetTypes[] value="rabbit" style="width:100px"/>
              <label style="width:100px" for="rabbit">Rabbit</label>
            </div> 
            <div style="float: left; width: 23%;">
              <input type="checkbox" id="fish" class="visitType" name=petCaretakerPetTypes[] value="fish" style="width:100px"/>
              <label style="width:100px" for="fish">Fish</label>
            </div> 
          </div>
      </div>

      <textarea name="petCaretakerAdditionalPetTypes" id="petCaretakerAdditionalPetTypes" placeholder="Haven't we the types of pets you take care of? Type here!"></textarea>

      
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" for="serviceType">Services:</label>
          </div>
          <div class="visitType-selector" style="float: left; width: 70%;">
            <div style="float:left">
              <div style="float: left; width: 15%;margin-right: 15mm;">
                <input type="checkbox" id="dailyWalk" name="petCaretakerServices[]" value="dailyWalk" class="visitType" style="width:100px" />
                <label style="width:100px" for="dailyWalk">Daily Walk</label>
              </div>
              <div style="float: left; width: 30%; margin-right: 15mm;">
                <input type="checkbox" id="vacationPetSitting" name="petCaretakerServices[]" value="vacationPetSitting" class="visitType" style="width:150px" />
                <label style="width:150px" for="vacationPetSitting">Vacation Pet Sitting</label>
              </div>
              
              <div style="float: left; width: 15%;margin-right: 15mm;">
                <input type="checkbox" id="petTraining" name="petCaretakerServices[]" value="petTraining" class="visitType" />
                <label for="petTraining">Pet Training</label>
              </div> 
            </div>

            <div>
              <div style="float: left; width: 23%; margin-right:15mm">
                <input type="checkbox" id="petSittingByHour" name="petCaretakerServices[]" value="petSittingByHour" class="visitType" />
                <label for="petSittingByHour">Pet Sitting by hour</label>
              </div> 
              <div style="float: left; width: 33%;">
                <input type="checkbox" id="overnightPetSitting" name="petCaretakerServices[]" value="overnightPetSitting" class="visitType" style="width:150px" />
                <label style="width:150px" for="overnightPetSitting">Overnight Pet Sitting</label>
              </div>
              
              
            </div>

           
            
          </div>
      </div>

      <textarea name="petCaretakerAdditionalServices" id="petCaretakerAdditionalServices" placeholder="Haven't we listed what you offer? Type here!"></textarea>

      <br><br>

      <hr><br>`;

      /////////////////////////////////////
      ///////BABYSITTER////////////////////
      /////////////////////////////////////

      var BabysitterCode = ` <h2 class="fs-title">Babysitter Category Information</h2>
      <h3 class="fs-subtitle">Let us know more about your services!</h3>
      <div>
          <div style="float: left; margin-top: 1mm;">
            <label class="label2" style="margin-bottom:-1cm" for="serviceType">Service Type* :</label>
          </div>
          <div class="radio-toolbar" style="float:left; width: 70%">
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="babysitterLiveIn" name="radios10" value="live-in" checked>
                <label for="babysitterLiveIn">Live-in Only</label>
            </div>
            <div style="float: left; width: 23%;margin-right: 15mm;">
                <input type="radio" id="babysitterDailyVisiting" name="radios10" value="daily-visiting">
                <label for="babysitterDailyVisiting">Daily-Visiting Only</label>
            </div>
            <div style="float: left; width: 23%;">
                <input type="radio" id="babysitterEither" name="radios10" value="either">
                <label for="babysitterEither">Either</label>
            </div> 
          </div>
      </div>

      <div style="width:100%; margin-bottom:3cm">
          <div style="float: left; padding-top: 6mm">
            <label class="label2" style="margin-bottom:-1cm; for="babysitter-age">Age Range* :</label>
          </div>
          <div style="float: left; padding-top: 6mm">
            <label class="label2" style="margin-bottom:-1cm; for="babysitter-from"> From </label>
          </div>
          <div style=" float: left; padding-top: 4mm">
            <input type="number" style="margin-bottom:-1cm; id="babysitterAgeFrom" name="babysitterAgeFrom" min="0" max="10" step="0.5" required>
          </div>
          <div style="float: left; padding-top: 6mm">
            <label class="label2" style="margin-bottom:-1cm;  for="babysitter-to"> To </label>
          </div>
          <div style="width: 30%; float: left; padding-top: 4mm">
            <input type="number" style="margin-bottom:-1cm;  id="babysitterAgeTo" name="babysitterAgeTo" min="0" max="10" step="0.5" required>
          </div>
      </div>

      <br>
      
      <br><br>

      <hr>

      <br>`;

      

        // console.log(document.getElementById("dynamicset1"));

        if (localStorage.getItem("Beautician") == "true") {
          document.getElementById("dynamicset1").innerHTML = BeauticianCode;
        } else {
          document.getElementById("dynamicset1").innerHTML = "";
        }

        if (localStorage.getItem("Plumber") == "true") {
          // console.log(document.getElementById("dynamicset"));
          document.getElementById("dynamicset2").innerHTML = PlumberCode;
        } else {
          document.getElementById("dynamicset2").innerHTML = "";
        }

        if (localStorage.getItem("Painter") == "true") {
          document.getElementById("dynamicset3").innerHTML = PainterCode;
        } else {
          document.getElementById("dynamicset3").innerHTML = "";
        }

        if (localStorage.getItem("Cook") == "true") {
          document.getElementById("dynamicset4").innerHTML = CookCode;
        } else {
          document.getElementById("dynamicset4").innerHTML = "";
        }

        if (localStorage.getItem("Tailor") == "true") {
          document.getElementById("dynamicset5").innerHTML = TailorCode;
        } else {
          document.getElementById("dynamicset5").innerHTML = "";
        }

        if (localStorage.getItem("EventOrganizer") == "true") {
          document.getElementById("dynamicset6").innerHTML = EventOrganizerCode;
        } else {
          document.getElementById("dynamicset6").innerHTML = "";
        }

        if (localStorage.getItem("HomefoodManufacturer") == "true") {
          document.getElementById("dynamicset7").innerHTML = HomefoodCode;
        } else {
          document.getElementById("dynamicset7").innerHTML = "";
        }

        if (localStorage.getItem("Driver") == "true") {
          document.getElementById("dynamicset8").innerHTML = DriverCode;
        } else {
          document.getElementById("dynamicset8").innerHTML = "";
        }

        if (localStorage.getItem("Programmer") == "true") {
          document.getElementById("dynamicset9").innerHTML = ProgrammerCode;
        } else {
          document.getElementById("dynamicset9").innerHTML = "";
        }

        if (localStorage.getItem("Mason") == "true") {
          document.getElementById("dynamicset10").innerHTML = MasonCode;
        } else {
          document.getElementById("dynamicset10").innerHTML = "";
        }

        if (localStorage.getItem("GraphicDesigner") == "true") {
          document.getElementById(
            "dynamicset11"
          ).innerHTML = GraphicDesignerCode;
        } else {
          document.getElementById("dynamicset11").innerHTML = "";
        }

        if (localStorage.getItem("Mechanic") == "true") {
          document.getElementById("dynamicset12").innerHTML = MechanicCode;
        } else {
          document.getElementById("dynamicset12").innerHTML = "";
        }

        if (localStorage.getItem("ContentWriter") == "true") {
          document.getElementById("dynamicset13").innerHTML = ContentWriterCode;
        } else {
          document.getElementById("dynamicset13").innerHTML = "";
        }

        if (localStorage.getItem("Housekeeper") == "true") {
          document.getElementById("dynamicset14").innerHTML = HousekeeperCode;
        } else {
          document.getElementById("dynamicset14").innerHTML = "";
        }

        if (localStorage.getItem("ElderlyCaretaker") == "true") {
          document.getElementById(
            "dynamicset15"
          ).innerHTML = ElderlyCaretakerCode;
        } else {
          document.getElementById("dynamicset15").innerHTML = "";
        }

        if (localStorage.getItem("Gardener") == "true") {
          document.getElementById("dynamicset16").innerHTML = GardenerCode;
        } else {
          document.getElementById("dynamicset16").innerHTML = "";
        }

  

        if (localStorage.getItem("Photographer") == "true") {
          document.getElementById("dynamicset17").innerHTML = PhotographerCode;
        } else {
          document.getElementById("dynamicset17").innerHTML = "";
        }

        if (localStorage.getItem("Carpenter") == "true") {
          document.getElementById("dynamicset18").innerHTML = CarpenterCode;
        } else {
          document.getElementById("dynamicset18").innerHTML = "";
        }

        if (localStorage.getItem("PetCaretaker") == "true") {
          document.getElementById("dynamicset19").innerHTML = PetCaretakerCode;
        } else {
          document.getElementById("dynamicset19").innerHTML = "";
        }

        if (localStorage.getItem("Babysitter") == "true") {
          document.getElementById("dynamicset20").innerHTML = BabysitterCode;
        } else {
          document.getElementById("dynamicset20").innerHTML = "";
        }

      

        selected = false;

        // var originalSetItem = localStorage.setItem;
        // localStorage.setItem = function () {
        //   document.createEvent('Event').initEvent('itemInserted', true, true);

        //   originalSetItem.apply(this, arguments);
        //   selected = true;
        //   console.log(selected)

        // }

        // if (true) {
        //   document.write("<h2 class='fs-title' >Freelancer Category Information</h2>");
        //   document.write("<h3 class='fs-subtitle'>Tell us what you offer!</h3>");
        //   document.write("<input type='text' name='fname' placeholder='First Name' />");
        //   document.write("<input type='text' name='lname' placeholder='Last Name' />");
        //   document.write("<input type='text' name='phone' placeholder='Phone' />");
        //   document.write("<textarea name='address' placeholder='Address'></textarea>");

        // } else {
        //   document.writeln("<p>FUCK</p>")
        // }

        // document.write("<p>Inside our anonymous function foo means '" + foo + '".</p>');
      } // We call our anonymous function immediately
    </script>

    <script src="../../assets/js/freelancer/freelancerSignup.js"></script>
    <script src="../../assets/js/freelancer/navbar.js"></script>
    
  </body>
</html>
