<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../assets/css/freelancer/freelancerNavbar.css" />
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/freelancer/freelancerProfile.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="../../assets/css/freelancer/freelancerSidebar.css"
    />
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/md5.js"></script>

    <title>Profile Management</title>

    <style>
      .column {
        float: left;
        width: 50%;
      }

      /* Clear floats after the columns */
      .row:after {
        display: table;
        clear: both;
      
      }
    </style>

    <script>
      $(function () {
        $("#includedContent1").load("./freelancerSidebar.php");
      });
    </script>

    <script>
      $(function () {
        $("#includedContent").load("./freelancerNavbar.php");
      });
    </script>

  </head>

  <body>
    <div>
      <nav class="nav1">
          <div>
          <a href="../index.html"><img class="img1" src="https://i.ibb.co/n0613Lr/Ceylon-Gig-Logo.jpg" alt="CeylonGig Logo"></a>
          </div>
          <ul class="nav-links">

              <div>
                  <div style="float:left; margin-left:11cm; margin-top: 5mm;">
                      <ul class="li1" style="font-weight: bold;"><a class="a1" style="color: black; list-style: none; font-weight: bold; font-family: Montserrat; display: flex; text-decoration: none" href="freelancerAboutUs.php">About</a></ul>
                  </div>
                  
                  <div  style="float:left; margin-left:10mm; margin-top: 5mm;">
                      <ul class="li1"><a href="../../../model/freelancer/freelancerLogout.php" class="log-in" style="color: #fbfbfb; font-weight: bold; font-family: Montserrat; list-style: none; text-decoration: none">Logout</a></ul>
                  </div>
                  
                  

                  <div style="float:left; margin-left:5mm; margin-top: 6mm;">
                      <a href="freelancerProfile.php" style="text-decoration:none" ><h3 style="font-size: 14px; text-decoration: none; font-family: Montserrat; color:#43CC58 ; font-weight: bold;"> <?php echo $_SESSION['fName'] ?> &nbsp;<?php echo $_SESSION['lName'] ?> </h3></a>
                  </div>

              </div>   
          </ul>
          
      </nav>
    </div>


    <div class="row" style="margin-top:-1mm;">
      <div class="column">
        <!-- importing sidebar -->

        <div style="position: fixed; height:90%; z-index: 1; margin-top: 10mm;">
          <div id="includedContent1"></div>
        </div>

        <!-- profile card -->

        <div class="container" style="margin-top:-3mm">
          <div class="card">
            <h3 class="title">My Profile</h3>
            <div class="bar">
              <div class="emptybar"></div>
              <div class="filledbar"></div>
            </div>

            <div class="profile-detail">

              <?php

                include_once('../../../model/freelancer/freelancerGetDetail.php');

                $row = mysqli_fetch_array($result);
               
                
                echo "<div>
                  <div style='float: left; width: 30%'>";

                  echo '<img src="data:image/jpeg;base64,'.$row['profilePicture'].'"/>';
                  echo "</div>";
                  echo "<div style='float: left; width: 70%'>";
                  echo "<h2 class='name'>".$row['fName']." ".$row['lName']."</h2>";

                  echo "<h3 class='post-title' style='margin-top:32%; margin-left:57%'> My Categories: </h3>";

                  echo "<div style='margin-top:3%; margin-left:87%'>";

                    while($row_categories=mysqli_fetch_array($result_categories)){

                      echo "<h3 class='post-title'> ".$row_categories['categoryName']."</h3><br/>";
                      

                    }
                  echo "</div>";
                  
                  echo "</div>"; 
                echo "</div>";
              

                echo "<p class='post-description' style='font-size:12px'>";
                  
                  echo "Email: ".$row['freelancerEmail']."
                  <br>";
                  // echo "Age : ".$row['freelancerEmail']."
                  // <br>";
                  echo "NIC : ".$row['freelancerNIC']."
                  <br>";
                  echo "Gender : ".$row['gender']."
                  <br>";
                  echo "Experience : ".$row['experience']." years
                  <br>";

                  echo "Working Days: ";
                  
                  while ($row2 = mysqli_fetch_array($result2)){
                    echo "{$row2['workingDays']} &nbsp";
                  }
                  
                  echo "<br>";
                  echo "Working hours : from - ".$row['workingHours_from']." to - ".$row['workingHours_to']."
                  <br>";
                  echo "Phone : ".$row['phone']."
                  <br>";
                  echo "Address : ".$row['address']."";

                echo "</p>";

              ?>

              

              <div
                style="
                  margin: 0 auto;
                  max-width: 350px;
                  padding: 0px;
                  font-size: 12px;
                  margin-top: 76%;
                "
              >

              <?php

                $star1=0;
                $star2=0;
                $star3=0;
                $star4=0;
                $star5=0;
                $total=0;

                while($row3 = mysqli_fetch_array($result3)){

                  $freelancerRating= $row3['freelancerRating'];

                  if($freelancerRating!=0){
                   

                    if($freelancerRating==1){
                      $star1++;
                    }
                    else if($freelancerRating==2){
                      $star2++;
                    }
                    else if($freelancerRating==3){
                      $star3++;
                    }
                    else if($freelancerRating==4){
                      $star4++;
                    }
                    else if($freelancerRating==5){
                      $star5++;
                    }                  
                  }                 
                }

                $row4 = mysqli_fetch_array($result4);

                echo "Average Rating: ".$row4['average']." rating based on ".$row4['total']." reviews" ;

                echo "<hr style='border: 2px solid #b4b538; margin-bottom: 10px; margin-top: 7px; width:305px' />";

                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 380px; left: 80px; border-radius: 0%' 
                src='../../assets/other/star.svg'>";

                echo "<p style='margin-left:200px'>".$star1."</p>";
                echo "<br>";

                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 410px; left: 80px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 410px; left: 110px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";

                echo "<p style='margin-left:200px; margin-top: -7px;'>".$star2."</p>";

                echo "<br>";

                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 440px; left: 80px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 440px; left: 110px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 440px; left: 140px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";

                echo "<p style='margin-left:200px; margin-top: -6px;'>".$star3."</p>";

                echo "<br>";


                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 470px; left: 80px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 470px; left: 110px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 470px; left: 140px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 470px; left: 170px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";

                echo "<p style='margin-left:200px; margin-top: -5px;'>".$star4."</p>";

                echo "<br>";


                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 500px; left: 80px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 500px; left: 110px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 500px; left: 140px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 500px; left: 170px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";
                echo "<img style=' width: 20px; height: 20px; position: absolute; top: 500px; left: 200px; border-radius: 0%;' 
                src='../../assets/other/star.svg'>";

                echo "<p style='margin-left:200px; margin-top: -2px;'>".$star5."</p>";

                echo "<br>";



                

              ?>
                
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- profile edit form -->


      <div class="column" style="margin-left: 630px">
        <div class="card1">




          <!-- ***********************************************
          PASSWORD RESET
          *********************************************** -->

          <form enctype="multipart/form-data" >

            <h1>Password Reset</h1>
            <fieldset>
              <legend>
                <span class='number'></span> <span class='number'></span>
                <span class='number'></span><span class='number'></span
                ><span class='number'></span>Reset Your Password
              </legend>
              <div>
                <div style='float: left; width: 15%'>
                  
                  <label style="width:28cm" for='old-password'>Old Password:</label>
                  <label for='new-password'>New Password:</label>
                  <label for='confirm-password'>Confirm Password:</label>
                </div>
                <div style='float: left; width: 85%'>
                  <input style="width:50%" value="" type='password' id='freelancerOldPassword' name='freelancerOldPassword' />
                  <input  style="width:50%" value="" type='password' id='freelancerNewPassword' name='freelancerPassword' />
                  <input style="width:50%" type='password' id='freelancerConfirmPassword' name='freelancerConfirmPassword'/>
                </div>
              </div>
            </fieldset> 

            <button onclick="passwordReset(`<?php echo $row['freelancerPassword'] ?>`)" type="button" name="reset_password" id="reset_password">Reset Password</button>

          </form>

          <!-- 
          ***********************************************
          SECONDARY DETAIL UPDATE
          ***********************************************  -->

          <form action="../../../model/freelancer/freelancerUpdateProfile.php" method="POST" enctype="multipart/form-data" >

            <h1>Secondary Deatail Update</h1>
            
            <fieldset>
              <legend>
                <span class="number"></span><span class="number"></span
                ><span class="number"></span><span class="number"></span
                ><span class="number"></span> Update Secondary Information
              </legend>

         
              <div>
                <div style="float: left; width: 15%">
                  <label style="width:5cm" for="first-name">First Name* :</label>
                  <label style="width:5cm" for="last-name">Last Name* :</label>
                </div>
                <div style="float: left; width: 85%">
                  <input style="margin-left:2cm; width:11.5cm" value="<?php echo $row['fName'] ?>"  type="text" id="fName" name="fName" required />
                  <input style="margin-left:2cm; width:11.5cm" value="<?php echo $row['lName'] ?>" type="text" id="lName" name="lName" required />
                </div>
              </div>

              <!-- PROFILE PICTURE SECTION----blob -->

              <div>
                <div style="float: left; width: 30%">
                  <label class="label1"  for="img">Profile Picture:</label>
                </div>
                <div style="float: left; width: 70%">
                  <input type="file" id="profilePicture" name="profilePicture" accept="image/*" />
                </div>
              </div>


                  
              <div>
                <div style="float: left; width: 15%;">
                  <label for="phone">Phone* : </label>
                </div>
                <div style="float: left; width: 85%;">
                  <input value="<?php echo $row['phone'] ?>" type="text" id="phone" name="phone" required/>
                </div>
              </div>
  
              

              <div>
                <div style="float: left; padding-top: 3mm">
                  <label class="label1" for="workingHours">Working Hours* :</label>
                </div>
                <div style="float: left; padding-top: 3mm; margin-left: 4mm;">
                  <label class="label1" for="workingHours_from"> From </label>
                </div>
                <div style="width: 2cm; float: left; padding-top: 3mm;margin-left: 4mm;">
                  <input value="<?php echo  $row['workingHours_from']  ?>" type="time" id="workingHours_from" name="workingHours_from" required />
                </div>
                <div style="float: left; padding-top: 3mm;margin-left: 4mm;">
                  <label class="label1" for="workingHours_to"> To </label>
                </div>
                <div style="width: 2cm; float: left; padding-top: 3mm;margin-left: 4mm;">
                  <input  value="<?php echo  $row['workingHours_to']  ?>" type="time" id="workingHours_to" name="workingHours_to" />
                </div>
              </div>

              <div>
                <div style="float: left; width: 3cm; margin-right:16%">
                  <label for="address">Address* :  </label>
                </div>
                <div style="float: left; width: 56%;">
                  <textarea id="address" name="address"  required> <?php echo $row['address'] ?> </textarea>
                </div>
              </div>
            </fieldset>

            <button  type="submit" name="submit" id="submit">Update</button>

          </form>
        </div>
      </div>
    </div>



  <script type="text/javascript">

    function validatePassword(freelancerNewPassword, freelancerConfirmPassword) {

      if (freelancerNewPassword == freelancerConfirmPassword) {
        return true;
      }
      return false;
    }


    function passwordReset(old_pass_hash){

      // alert(old_pass_hash);

      var freelancerOldPassword = document.getElementById("freelancerOldPassword").value;
      var freelancerNewPassword = document.getElementById("freelancerNewPassword").value;
      var freelancerConfirmPassword = document.getElementById("freelancerConfirmPassword").value;

      //taking MD5 hash of the entered password

      var passhash = CryptoJS.MD5(freelancerOldPassword).toString();


      //check if all fields are entered
      if ( freelancerOldPassword == "" || freelancerConfirmPassword == "" || freelancerNewPassword=="") {
        
        alert("Required fields cannot be empty");
        
      } 

      //then, check new password matches with confirm password
      
      else if (!validatePassword(freelancerNewPassword, freelancerConfirmPassword)) {
        
        alert("Passwords do not match");
        
      }

      //check if entered password matches with what's in database

      else if(passhash!=old_pass_hash){
        alert("Your old password is wrong!");
      }

      else{

        $.ajax({   

          url: '../../../model/freelancer/freelancerPasswordReset.php',
          data: {  
            freelancerNewPassword:freelancerNewPassword
          },

          type: 'POST'
          }).done(function(resp) {

          if (resp) {

            // alert(resp);

            alert ("Password reset successfully!");
            window.location.href = '../../pages/freelancer/freelancerProfile.php';

          } else {
            alert("Error in password reset");
          }
        });

        
      }

    }

   



    // function validatePhone(phone) {
    //   const re = /^\d+$/;
    //   if (phone.length == 10 && re.test(phone) && phone[0] == 0) {
    //     return true;
    //   }
    //   return false;
    // }

    // function validateNames(fName, lName) {
    //   const re = /^[a-zA-Z]+$/;

    //   if (re.test(fName) && re.test(lName)) {
    //     // alert("correct name");
    //     return true;
    //   }
    //   // alert("incorrect name");
    //   return false;
    // }


    // function validation() {

    //   var validate=true;
     

    //   var fName = document.getElementById('fName').value;
    //   var lName = document.getElementById('lName').value;
    //   var phone = document.getElementById('phone').value;
    //   var address = document.getElementById('address').value;
    //   var workingHours_from = document.getElementById('workingHours_from').value;
    //   var workingHours_to = document.getElementById('workingHours_to').value;
    //   var profilePicture = document.getElementById('profilePicture').value;

    //   console.log(profilePicture);

    //   if ( fName == "" || lName == "" || phone == "" || address == "" || workingHours_from == "" || workingHours_to == "") {
    //     var validate=false;
    //     alert('Required fields cannot be empty');
        
        

    //   } else if (!validateNames(fName, lName)) {
    //     var validate=false;
    //     alert('First Name and Last name should contain letters only');
        

    //   }  else if (!validatePhone(phone)) {
    //     var validate=false;
    //     alert('Invalid Phone number');
    //   } 


    //   var file = $('#profilePicture').get(0).files.item(0); // instance of File

    //   if(validate==true){

        
    //     $.ajax({


    //       url: '../../../model/freelancer/freelancerUpdateProfile.php',
    //       data: {
                
    //         fName:fName,
    //         lName:lName,
    //         phone:phone,
    //         address:address,
    //         workingHours_from:workingHours_from,
    //         workingHours_to:workingHours_to,
    //         profilePicture:file
            
    //         // workingDays: workingDays,


    //       },

    //       type: 'POST',
    //       contenType: 'application/my-binary-type',,
    //       processData:false
          
    //     }).done(function(resp) {

    //       if (resp==true) {
    //         alert("Profile Updated Successfully");
    //         window.location.href = '../../pages/freelancer/freelancerProfile.php';
    //       } else {
    //         alert("Error in profile update");
    //       }
    //     });
    //   }
    // }

  </script>


  </body>
</html>
