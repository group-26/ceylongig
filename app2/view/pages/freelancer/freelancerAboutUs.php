<?php 
	// session_start();

    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

	if(!isset($_SESSION['freelancerID']) == true ) {

		header("Location:../../pages/login.php");
    }
?>





<html>

    <head>
            <title>About us</title>
            <link rel="stylesheet" type="text/css" href="../../assets/css/freelancer/aboutus.css">
            <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
            <link rel="stylesheet" href="../../assets/css/freelancer/freelancerNavbar.css" />
            <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
            <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
            <!-- <script> 
                $(function(){
                $("#includedContent").load("header.php"); 
                });
            </script>  -->

    </head>

    <body>

        <header>

        <div style="margin-left:-2mm; margin-top:-2mm">
      <nav class="nav1">
          <div>
              <a href="../index.html"><img class="img1" src="https://i.ibb.co/n0613Lr/Ceylon-Gig-Logo.jpg" alt="CeylonGig Logo"></a>
          </div>
          <ul class="nav-links">

              <div>
                  <div style="float:left; margin-left:13cm; margin-top: 5mm;">
                      <ul class="li1" style="font-weight: bold;"><a class="a1" style="font-size:16px; color: black; list-style: none; font-weight: bold; font-family: Montserrat; display: flex; text-decoration: none" href="freelancerAboutUs.php">About</a></ul>
                  </div>
                  
                  <div  style="float:left; margin-left:10mm; margin-top: 5mm;">
                      <ul class="li1"><a href="../../../model/freelancer/freelancerLogout.php" class="log-in" style="font-size:16px; color: #fbfbfb; font-weight: bold; font-family: Montserrat; list-style: none; text-decoration: none">Logout</a></ul>
                  </div>

                  <div style="float:left; margin-left:5mm; margin-top: 1mm;">
                      <a href="freelancerProfile.php"><h3 style="font-size: 14px; text-decoration: none; font-family: Montserrat; color:#43CC58 ; font-weight: bold;"> <?php echo $_SESSION['fName'] ?> &nbsp;<?php echo $_SESSION['lName'] ?> </h3></a>
                  </div>

              </div>   
          </ul>
          
      </nav>
    </div>

        </header>
            <div class="container">
                <div class="about">
                    <div class="text">
                        <h1>About us</h1>
                        <div class="line"></div>
                    </div>
                </div>
                <div class="content">
                    <p> <b>CeylonGig</b> is established with a motive of creating a web-based platform for all self-employed workers, also known as freelancers,
                        to deliver their service efficiently to the customers who are seeking them.
                        <b>CeylonGig</b> will be targeting to assist freelancers such as home-food makers, tailors, beauticians, plumbers, mechanics and also other online freelancers to get connected with their customers.
                    <br> It also acts as a platform for the customers to post a job which needs to be done for them and the system will help them find the freelancers
                        who are capable of offering them goods/services required.<br>
                        The ultimate goal of the project is to deliver a safe and novel platform for Sri Lankan freelancers to find a job and for the customers who live in Sri Lanka to get their jobs done.
                    </p>
                </div>
            </div>

            
       
    </body>

</html>