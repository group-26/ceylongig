<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 include('../../../model/Advertiser/AdvertProfile.php');
 
 ?>
<!DOCTYPE html>

<head>
    <title>Profile-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/advertProfile.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="heading">
                <h1>Profile</h1>
            </div>
            <div id="detailsleft">
                <label for="orgID">Organisation ID: &nbsp&nbsp </label>
                <label name="orgID" for="orgID"><b><?php echo $orgID; ?></b></label><br><br><br>
                <label for="orgName">Organisation Name:&nbsp&nbsp</label>
                <label name="orgName" for="orgName"><b><?php echo $orgName; ?></b></label><br><br><br>
                <label for="orgAddress">Address:&nbsp&nbsp</label>
                <label name="orgAddress" for="orgAddress"><b><?php echo $orgAddress; ?></b></label><br><br><br>
                <label for="orgEmail">Email:&nbsp&nbsp</label>
                <label name="orgEmail" for="orgEmail"><b><?php echo $orgEmail; ?></b></label><br><br><br>
                <label for="orgContact">Contact No:&nbsp&nbsp</label>
                <label name="orgContact" for="orgContact"><b><?php echo $orgContact; ?></b></label><br><br><br>

                
            </div>
            <div id="detailsright">
                <img id="profileimg" name="profileimg" src='data:image/jpeg;base64,<?php echo "$profImg"; ?>' alt=""><br>
                <label for="orgID">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspProfile Image</label><br><br>
                
            </div>
            <div id="editBtnCon">
                <input type="submit" value="Edit Details" id="editBtn" onclick="window.location.href='advertEditProfile.php'">
            </div>
        </div>

    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>
</body>