
<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');

 ?>

<!DOCTYPE html>

<head>
    <title>Contact Us - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertContact.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>Contact Us</h1>
            </div>
                <div id="paymentBlock1" class="paymentBlocks">
                    <h3>Team Ceylon GIG</h3><br><br>
                    <label for="adName1">Address :<b>nbsp&nbspNo.157, Galle Road Colombo 21&nbsp&nbsp</b></label>
                    <br><br>
                    <label for="reqName1">Contact :<b>&nbsp&nbsp +94117636374</b></label>
                    <br><br>
                    <label for="clickDest1">Email:&nbsp&nbsp<b>ceylonggig@gmail.com</b></label><br><br><br>
                    <h3>Ceylon GIG - Advertiser </h3><br><br>
                    <label for="pacakge1">Contact :<b>&nbsp&nbsp +94117636375</b></label>
                    <br><br>
                    <label for="stratDate">Email:&nbsp&nbsp<b>ceylonggigadvert@gmail.com</b></label>
                    <br><br>
                    <label for="contactNo">For Complains please reach us through clicking <a href="AdvertComplain.php">here</a></label>
                    <br><br>

                </div>
        </div>
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>

        
</body>

