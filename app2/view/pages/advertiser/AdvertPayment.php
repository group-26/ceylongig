<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 include('../../../model/Advertiser/AdvertPayment.php');

 ?>

<!DOCTYPE html>

<head>
    <title>Payments - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertPayment.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>Payment Details</h1>
            </div>
                <div id="paymentBlock1" class="paymentBlocks">
                    <h3>Please make sure everything is correct</h3><br><br>
                    <label for="adName1"><b>Advertisment Name:&nbsp&nbsp</b></label>
                    <lable id='adName1' ></lable><br><br>
                    <label for="reqName1"><b>Requester Name:&nbsp&nbsp</b></label>
                    <lable id='reqName1' ></lable><br><br>
                    <label for="clickDest1"><b>Click Destination:&nbsp&nbsp</b></label>
                    <lable id='clickDest1' ></lable><br><br>
                    <label for="pacakge1"><b>Package:&nbsp&nbsp</b></label>
                    <lable id='package1' ></lable><br><br>
                    <label for="stratDate"><b>Starting Date:&nbsp&nbsp</b></label>
                    <lable id='startDate1' ></lable><br><br>
                    <label for="contactNo"><b>Contact:&nbsp&nbsp</b></label>
                    <lable id='contactNo1' ></lable><br><br>
                    <p style="font-size: x-small;">Please note that if your advertisment does not follow our ethical guidelines it will be rejected and your payment will be fully refunded. </p><br><br>

                    <form method="post" action="https://sandbox.payhere.lk/pay/checkout"> 
                        <div style="text-align:center;"> 
                        <input type="submit" value="Pay" id="payBtn" name="payBtn">
                        </div>  
                        <input type="hidden" name="merchant_id" value="1217031">
                        <input type="hidden" name="return_url" value="http://localhost/ceylongig/app2/view/pages/advertiser/AdvertPaymentConfirmation.php">
                        <input type="hidden" name="cancel_url" value="http://localhost/ceylongig/app2/view/pages/advertiser/AdvertnewAd.php?id=2">
                        <input type="hidden" name="notify_url" value="http://localhost/ceylongig/app2/view/pages/advertiser/AdvertDashboard.php">  
                        
                        <input type="hidden" name="order_id" value="1">
                        <input type="hidden" id="adName" name="items"><br>
                        <input type="hidden" name="currency" value="LKR">
                        <input type="hidden" id="amount" name="amount">  
                    
                        <input type="hidden" name="first_name" id="reqName">
                        <input type="hidden" name="last_name" value="<?php echo $orgLoggedName ?>"><br>
                        <input type="hidden" name="email" value="<?php echo $email ?>">
                        <input type="hidden" name="phone" id="contactNo"><br>
                        <input type="hidden" name="address" value="<?php echo $companyAddress ?>">
                        <input type="hidden" name="city" value="#">
                        <input type="hidden" name="country" value="Sri Lanka"><br><br> 
                    
                    </form>
                </div>
                <div id="paymentBlock1" class="imgBlock">
                        <label for="img">Advertisment Image</label><br><br>
                        <img id='previewImg' >
                </div>
                        <script>
                            document.getElementById('previewImg').src=localStorage.getItem('img');
                            document.getElementById('adName').value= localStorage.getItem('adName');
                            document.getElementById('reqName').value= localStorage.getItem('reqname');
                            document.getElementById('contactNo').value= localStorage.getItem('contactNo');

                            if(localStorage.getItem('package')==30){
                                document.getElementById('amount').value=15000;
                                document.getElementById('package1').innerHTML="1 Month";
                            }else if(localStorage.getItem('package')==7){
                                document.getElementById('amount').value=4000;
                                document.getElementById('package1').innerHTML="1 Week";
                            }else if(localStorage.getItem('package')==1){
                                document.getElementById('amount').value=650;
                                document.getElementById('package1').innerHTML="1 Day";
                            }
                            else{
                                document.getElementById('amount').value==NULL;
                            }

                            document.getElementById('adName1').innerHTML= localStorage.getItem('adName');
                            document.getElementById('reqName1').innerHTML= localStorage.getItem('reqname');
                            document.getElementById('clickDest1').innerHTML= localStorage.getItem('website');
                            document.getElementById('startDate1').innerHTML= localStorage.getItem('startdate');
                            document.getElementById('contactNo1').innerHTML= localStorage.getItem('contactNo');
                        </script>
                        <br><br>   
                
                <script src="../../assets/js/advertiser/adImgPreview.js"></script>                
            
            
        </div>
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>

        
</body>

