<?php
 include('../../../model/Advertiser/connection.php');
 ?>

<!DOCTYPE html>

<head>
    <title>SignUp - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertSignup.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <div id="content">
            <div id="pgeHeading">
                <h1>Advertiser SignUp</h1>
            </div>
                
            <form name="SignUp" action="../../../model/Advertiser/AdvertSignup.php" method="POST" enctype="multipart/form-data">
                <div class="block" id="rightBlock" >
                        
                    <label for="orgName">Organisation Name:</label><br>
                    <input type="text" id="orgName" name="orgName" value="" class="txtField" required><br><br>
                    <label for="orgEmail">Organisation Email:</label><br>
                    <input type="email" id="orgEmail" name="orgEmail" value="" class="txtField" required><br><br>
                    <label for="pwd">Password</label><br>
                    <input type="Password" id="pwd" minlength="7" maxlength="20" name="pwd" value="" class="txtField" required><br><br>
                    <label for="conPwd">Confirm Password</label><br>
                    <input type="Password" id="conPwd" name="conPwd" value="" class="txtField" required><br><br>
                    <label for="orgAddress">Address:</label><br>
                    <input type="text" id="orgAddress" name="orgAddress" value="" class="txtField" required><br><br>
                    <label for="orgContact">Contact No:</label><br>
                    <input type="text" id="orgContact" minlength="9" maxlength="12" name="orgContact" value="" class="txtField" required><br><br>
                </div>

                <div class="block" id="leftBlock" >
                        
                    <input type="file" id="profImg" name="profImg" accept="image/*" onchange="loadFile(event)" ><br>
                    <img id="previewImg"/><br><br>
                    <label for="orgID">Organistation Profile Image</label><br>
                    <h3>Registerer Information</h3>
                    <label for="regName">Name:</label><br>
                    <input type="text" id="regName" name="regName" value="" class="txtField" required><br><br>
                    <label for="NIC">NIC:</label><br>
                    <input type="text" id="regNIC" minlength="10" maxlength="12" name="regNIC" value="" class="txtField" required><br><br>
                </div>
                <script src="../../assets/js/advertiser/adImgPreview.js"></script>
                <div id="signupBlock">
                    <input type="submit" value="SignUp" id="signupBtn" name="signupBtn">
                    <input type="submit" value="Cancel" id="signupBtn" onclick="window.location.href='AdvertLogin.php'">
                    <p style="font-size: small;">Already Have a Account? <a href="AdvertLogin.php"> Click here to Login </a></p>
                 </div>
            </form>
            
        </div>
    </div>
</body>  