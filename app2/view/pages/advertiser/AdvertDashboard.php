<?php
include('../../../model/Advertiser/Session.php');
?>
<!DOCTYPE html>

    <head>
        <title>Dashboard-Advertiser</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
        <link rel="stylesheet" href="../../assets/css/advert/AdvertDashboard.css">
        <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
        <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
        <script src="https://kit.fontawesome.com/ca362f7c3e.js" crossorigin="anonymous"></script>
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>

        
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        
    </head>

    <body>
        <div id="container" >
            <nav>
                <?php 
                    include('AdvertNav.php');
                    include('popupAdvertisement.php');
                ?>
                <script src="../../assets/js/popup.js"></script>

            </nav>
            
            <div id="leftAd">
                <a href="#"><img id="AdImg" src="../../assets/img/Advert1.png" alt="RightAd"></a>
                <p>Your ad may look like this</p>
            </div>
            <div id="rightAd">
                <a href="#"><img id="AdImg" src="../../assets/img/Advert2.png" alt="RightAd"></a>
                <p>Your ad may look like this</p>
            </div>
            

            <div id="content">
                <a href="AdvertPackages.php">
                    <div class="blocks">
                        <i class="fas fa-4x fa-ad"></i>
                        <p><b>New Ad</b></p>
                    </div>
                </a>
                <a href="AdvertStat.php">
                    <div class="blocks">
                        <i class="fas fa-4x fa-chart-line"></i>
                        <p><b>Statistics</b></p>
                    </div>
                </a>
                <a href="AdvertLog.php">
                    <div class="blocks">
                        <i class="fas fa-4x fa-clipboard-list"></i>
                        <p><b>Log</b></p>
                    </div>
                </a>
                <a href="AdvertProfile.php">
                    <div class="blocks">
                        <i class="fas fa-4x fa-user-alt"></i>
                        <p><b>Profile</b></p>
                    </div>
                </a>
                <a href="AdvertComplain.php">
                    <div class="blocks">
                        <i class="fas fa-4x fa-mail-bulk"></i>
                        <p><b>Complain</b></p>
                    </div>
                </a>
            </div>
            
            <div id="topcon">
                <div class="slideImg fade">
                    <img  id="contentImg" src="../../assets/img/TopCont-01.png" alt="TopContent">
                </div>
                <div class="slideImg fade">
                    <img  id="contentImg" src="../../assets/img/TopContent-02.png" alt="TopContent">
                </div>
                <div class="slideImg fade">
                    <img  id="contentImg" src="../../assets/img/TopContent-03.png" alt="TopContent">
                </div>
                <div class="slideImg fade">
                    <img  id="contentImg" src="../../assets/img/TopContent-04.png" alt="TopContent">
                </div>
            </div>
            <script src="../../assets/js/advertiser/advertDashboard.js"></script>
            
        </div>
        
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>
    </body>
</html>