<?php
 include('../../../model/Advertiser/connection.php');
 ?>

<!DOCTYPE html>

<head>
    <title>Login - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertResetPwdEmail.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <div id="content">
            <div id="pgeHeading">
                <h2>Please enter your email</h2>
            </div>
                
            <form name="SignUp" action="../../../model/Advertiser/AdvertLogin.php" method="POST">
                <div id="sendMailBlock">
                    <label for="orgEmail">Email</label><br>
                    <input type="email" id="orgEmail" name="orgEmail" value="" class="txtField" required><br><br><br>
                    <input type="submit" value="Send OTP" id="sendBtn" name="sendBtn"><br>
                </div>
            </form>
            
        </div>
    </div>
</body>