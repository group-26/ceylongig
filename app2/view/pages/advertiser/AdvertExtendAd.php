<?php
include('../../../model/Advertiser/Session.php');
?>
<!DOCTYPE html>

<head>
    <title>Extend Ad-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertExtendAd.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="heading">
            <h1>Extend Progressing Advertisment Request</h1>
        </div>
        <form>
            <div id="Content">
                <div id="detailsleft">
                    <H2>Current Details</H2><br><br>
                    <label for="adID">Advertisment ID:</label><br>
                    <label for="adID">#</label><br><br>
                    <label for="adOwnerName">Advertisor Name:</label><br>
                    <label for="AdOwnerName">#</label><br><br>
                    <label for="adOwnerNIC">Advertisor NIC:</label><br>
                    <label for="adOwnerNIC">#</label><br><br>
                    <label for="adOrgName">Organisation:</label><br>
                    <label for="adOrgName">#</label><br><br>
                    <label for="adStartDate">Started Date:</label><br>
                    <label for="adStartDate">#</label><br><br>
                    <label for="adEndDate">Current Ending Date:</label><br>
                    <label for="adEndDate">#</label><br><br>
                    <label for="adPackage">Current Package:</label><br>
                    <label for="adPackage">#</label><br><br>

                </div>
                <div id="detailsright">
                    <H2>Extension Details</H2><br><br>
                    <p>Extension Pacakage:</p><br>
                    <input type="radio" id="weekly" name="package" value="weekly" required>
                    <label for="weekly">Weekly Extension</label><br>
                    <input type="radio" id="monthly" name="package" value="monthly" required>
                    <label for="monthly">Monthly Extension</label><br><br>
                    <p><b>*NOTE: </b><br><br>- Request Should be Submitted prior to two Days <br><br>- Extension not Avalible for Daily Advetisment Package.<br><br>- You will be Conatcted Regading extension and payment</p><br><br><br><br>
                    <input type="submit" value="Request" id="extendBtn">
                    <input type="submit" value="Cancel" id="extendBtn" onclick="window.location.href='AdvertDashboard.php'">        
                </div>
                <div id="adImg">
                    <img src="#" alt="" id="adImgProp">
                    <p>Advetisment Image</p>

                </div>
                
            </div>
        </form>

    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>
</body>