<?php
include('../../../model/Advertiser/Session.php');
?>
<!DOCTYPE html>

<head>
    <title>Packages-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertPackages.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
       
        <div id="leftAd">
            <a href="#"><img id="AdImg" src="../../assets/img/FreelancerAd-02.png" alt="RightAd"></a>
            <p>Your ad may look like this</p>
        </div>
        <div id="rightAd">
            <a href="#"><img id="AdImg" src="../../assets/img/FreelancerAd-01.png" alt="RightAd"></a>
            <p>Your ad may look like this</p>
        </div>

        <div id="heading">
            <h1>Please Choose your Package</h1>
        </div>

        <div id="allPackages">
            <a href="AdvertnewAd.php?id=1">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 15000.png" alt="price">
                    </div>
                    <h2>Monthly<br>Package</h2>
                    <p><br>This pacakage will be valid for 30 days</p>
                </div>
            </a>
            <a href="AdvertnewAd.php?id=2">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 4000.png" alt="price">
                    </div>
                    <h2>Weekly<br>Package</h2>
                    <p><br>This pacakage will be valid for 07 days</p>
                </div>
            </a>
            <a href="AdvertnewAd.php?id=3">
                <div class="package">
                    <div class=priceBlock>
                        <img src="../../assets/img/LKR 650.png" alt="price">
                    </div>
                    <h2>Daily<br>Package</h2>
                    <p><br>This pacakage will be valid for 24 hours</p>
                </div>
            </a>

        </div>
        
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>
</body>