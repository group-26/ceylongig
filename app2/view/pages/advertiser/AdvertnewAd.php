<?php
include('../../../model/Advertiser/Session.php');
include('../../../model/Advertiser/AdvertnewAd.php');

?>
<!DOCTYPE html>

<head>
    <title>Request Ad-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertnewAd.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <script src="../../assets/js/advertiser/AdtempStore.js"></script>

        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>New  Advertsiment</h1>
            </div>
            <form name="newAD" onclick="AdvertPayments.php" method="POST" enctype="multipart/form-data"> 
                <div id="formCont">
                
                    <div class="block" id="rightBlock" >
                                                
                        <label for="name">Requester Name:</label><br>
                        <input class="field" type="text" id="reqName" name="reqName" value="" required><br><br>
                        <label for="nic" >NIC:</label><br>
                        <input class="field" type="text" id="nic" name="nic" value="" minlength="10" maxlength="12" required><br><br>
                        <label for="adName">Advertisment Name:</label><br>
                        <input class="field" type="text" id="adName" name="adName" value="" required><br><br>
                        <label for="contactNo">Contact No:</label><br>
                        <input class="field"  minlength="9" maxlength="12" type="text" id="contactNo" name="contactNo" value="" required><br><br>
                        <label for="startdate">Select Advertisment Start date:<br><b><span style="font-size: x-small;">*Date should be three after to todays date</span></b></label><br>
                        <input class="field" type="date" id="startdate" name="startdate" min="<?php echo date('Y-m-d',$mindate);?>" max="<?php echo date('Y-m-d',$maxdate);?>"><br><br>
                        <p>Package:</p>
                        <?php 
                        if(isset($_GET['id'])){
                            $id=$_GET['id'];
                        }
                        if($id==3){
                            echo "<input type=\"radio\" id=\"daily\" name=\"package\" value=\"1\" required checked>
                            <label for=\"daily\">Daily Package</label><br>
                            <input type=\"radio\" id=\"weekly\" name=\"package\" value=\"7\" required>
                            <label for=\"weekly\">Weekly Package</label><br>
                            <input type=\"radio\" id=\"monthly\" name=\"package\" value=\"30\" required>
                            <label for=\"monthly\">Monthly Package</label><br>";
                        }elseif($id==2){
                            echo "<input type=\"radio\" id=\"daily\" name=\"package\" value=\"1\" required >
                            <label for=\"daily\">Daily Package</label><br>
                            <input type=\"radio\" id=\"weekly\" name=\"package\" value=\"7\" required checked>
                            <label for=\"weekly\">Weekly Package</label><br>
                            <input type=\"radio\" id=\"monthly\" name=\"package\" value=\"30\" required>
                            <label for=\"monthly\">Monthly Package</label><br>";
                        }else{
                            echo "<input type=\"radio\" id=\"daily\" name=\"package\" value=\"1\" required >
                            <label for=\"daily\">Daily Package</label><br>
                            <input type=\"radio\" id=\"weekly\" name=\"package\" value=\"7\" required>
                            <label for=\"weekly\">Weekly Package</label><br>
                            <input type=\"radio\" id=\"monthly\" name=\"package\" value=\"30\" required checked>
                            <label for=\"monthly\">Monthly Package</label><br>";
                        }
                        ?>
                        <a href="AdvertPackages.php" style="font-size: x-small;">Click Here for package Details</a>
                       
                    </div>
                    <div class="block" id="leftBlock">
                     <label for="website">Click Destination:</label><br>
                        <input class="field" type="text" id="website" name="website" value="" required><br><br>
                        <label for="advImg">Select Advertisment Image:</label><br>
                        <input type="file" id="advImg" name="advImg" accept="image/*" onchange="loadFile(event)" required><br>
                       
                        <img id="previewImg"/>
                        <p style="font-size: x-small;">Preferd Image size is 200x300 pixels</p>
                    </div>
                    <script src="../../assets/js/advertiser/adImgPreview.js"></script>
                
                </div>
                    <div id="btnBlock">
                        <input type="submit" value="Submit" id="submitBtn" name="submitBtn" onclick="setDetails()">
                    </div>
            </form>
        </div>
    </div>
        <footer>
        </footer>

        <script>
        document.getElementById("advImg").addEventListener("change", readFile);
        </script>
</body>