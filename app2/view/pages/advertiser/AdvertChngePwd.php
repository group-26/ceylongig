<?php
include('../../../model/Advertiser/Session.php');
include('../../../model/Advertiser/AdvertChngePwd.php');
?>

<!DOCTYPE html>

<head>
    <title>Change Password - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertChngPwd.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container">
        <nav>
                <?php include('AdvertNav.php') ?>
            </nav>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>Change Password</h1>
            </div>
                
            <form name="chngePwd" action="" method="POST">
                <div id="ChngePwdBlock">
                    <label for="crntPwd">Current Password: </label><br>
                    <input type="Password" id="crntPwd" name="crntPwd" value="" class="txtField" required><br><br><br>
                    <label for="newPwd">New Password: </label><br>
                    <input type="Password" id="newPwd" name="newPwd" value="" class="txtField" minlength="7" maxlength="20" required><br><br><br>
                    <label for="conPwd">Confirm New Password: </label><br>
                    <input type="Password" id="conPwd" name="conPwd" value="" class="txtField" required><br><br><br>
                    <div style="text-align: center;">
                        <input type="submit" value="Change Password" id="chngeBtn" name="chngeBtn">
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    <footer>
        <?php include('AdvertFooter.php');?>    
    </footer>
</body>