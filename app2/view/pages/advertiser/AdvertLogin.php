<?php
 include('../../../model/Advertiser/connection.php');
 ?>

<!DOCTYPE html>

<head>
    <title>Login - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertLogin.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <div id="content">
            <div id="pgeHeading">
                <h1>Advertiser Login</h1>
            </div>
                
            <form name="SignUp" action="../../../model/Advertiser/AdvertLogin.php" method="POST">
                <div id="loginBlock">
                    <label for="orgEmail">Email</label><br>
                    <input type="email" id="orgEmail" name="orgEmail" value="" class="txtField" required><br><br><br>
                    <label for="pwd">Password</label><br>
                    <input type="Password" id="pwd" name="pwd" value="" class="txtField" required><br>                    <br><br>
                    <input type="submit" value="Login" id="loginBtn" name="loginBtn"><br>
                    <p style="font-size: small;"><a href="AdvertResetPwdEmail.php">Forgotten Password?</a></p><br>
                    <p style="font-size: small;"><br>Don't Have an Account? <a href="AdvertSignup.php"> Click here to Signup. </a></p>
                </div>
            </form>
            
        </div>
    </div>
</body>