<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 include('../../../model/Advertiser/AdvertEditProfile.php');
 
 ?>
<!DOCTYPE html>

<head>
    <title>Edit Profile-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertEditProfile.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="heading">
                <h1>Profile</h1>
            </div>
            <form name="Edit" action="" method="POST" enctype="multipart/form-data">
                <div id="detailsleft">
                    <label for="orgID">Organisation ID:</label><br>
                    <label for="orgID"><?php echo $orgID; ?></label><br><br>
                    <label for="orgEmail">Email:</label><br>
                    <label for="orgEmail"><?php echo $orgEmail; ?></label><br><br>
                    <label for="orgName">Organisation Name:</label><br>
                    <input type="text" id="orgName" name="orgName" value="<?php echo $orgName; ?>" class="txtField" required><br><br>
                    <label for="orgAddress">Address:</label><br>
                    <input type="text" id="orgAddress" name="orgAddress" value="<?php echo $orgAddress; ?>" class="txtField" required><br><br>

                    <label for="orgContact">Contact No:</label><br>
                    <input type="text" id="orgContact"  minlength="9" maxlength="12" name="orgContact" value="<?php echo $orgContact; ?>" class="txtField" required><br><br>

                    
                </div>
                <div id="detailsright">
                    <div id="allImg">
                        <img id="profileimg" name="profileimg" src='data:image/jpeg;base64,<?php echo "$profImg1"; ?>' alt="">
                        <img id="previewImg"/><br>
                    </div>
                    <input type="file" id="profImg" value='<?php echo "$profImg1"; ?>' name="profImg" accept="image/*" onchange="loadFile(event)" ><br>
                    <label>Change Profile Image</label>
                    <input style="width: 9cm; margin-top:1em;" type="submit" value="Change Password" id="editBtn" name="chngPwd"><br>
                    <p style="font-size: small;"><b>*Note:- </b>Please contact our support team to<br>change <b>E-mail</b> or <b>Delete</b> your Account</p>
                </div>
                <script src="../../assets/js/advertiser/adImgPreview.js"></script>
                <div id="editBtnCon">
                    <input type="submit" value="Save" id="editBtn" name="editBtn">
                    <input type="submit" value="Cancel" id="editBtn"  name="cancelBtn">
                </div>
            </form>
            
        </div>
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>
    

</body>