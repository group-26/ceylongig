<?php
include('../../../model/Advertiser/Session.php');
$orgLogged=$_SESSION['orgID'];
$orgLoggedName=$_SESSION['orgName'];
?>
<!DOCTYPE html>

<head>
    <title>Log-Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertLog.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="heading">
            <h1>Advertisement Log</h1><br>
            <h2>Organisation Name:  <?php echo $orgLoggedName;?></h2><br><br>
            
            
        </div>
        <div>
          <button id="downloadBtn" name="downloadBtn" value="Download" onclick="demoFromHTML()">Download</button>
        </div>

       
        <div id="logTable">
            <table class="tg">
                <thead>
                  <tr>
                    <th class="tg-frgj">Advertisement ID</th>
                    <th class="tg-frgj">Advertisement Name</th>
                    <th class="tg-frgj">Requested By<br></th>
                    <th class="tg-frgj">Requester NIC</th>
                    <th class="tg-frgj">Start Date</th>
                    <th class="tg-frgj">End Date</th>
                    <th class="tg-frgj">clickDestination</th>
                    <th class="tg-frgj">Status</th>
                    <th class="tg-frgj">Price</th>
                    <th class="tg-frgj">Report</th>
                  </tr>
                </thead>
                <tbody>
                  <?php include('../../../model/Advertiser/AdvertLog.php');?>
                </tbody>
            </table>
            
    </div>
    <footer>
      <?php include('AdvertFooter.php');?>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
    <script>
    function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#logTable')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function(element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 0,
        left: 200,
        width: 1500
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, {// y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },
    function(dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('AdvertisementLogReport.pdf');
    }
    , margins);
}
    </script>

</body>