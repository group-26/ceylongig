<?php
 include('../../../model/Advertiser/connection.php');
 ?>

<!DOCTYPE html>

<head>
    <title>Login - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertResetPwd.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <div id="content">
            <div id="pgeHeading">
                <h1>Reset Password</h1>
            </div>
                
            <form name="SignUp" action="../../../model/Advertiser/AdvertLogin.php" method="POST">
                <div id="loginBlock">
                    <label for="newPassword">New Password</label><br>
                    <input type="Password" id="newpwd" name="newpwd" value="" class="txtField" required><br><br><br>
                    <label for="pwd">Confirm New Password</label><br>
                    <input type="Password" id="Conpwd" name="Conpwd" value="" class="txtField" required><br><br><br>
                    <input type="submit" value="Reset" id="loginBtn" name="oginBtn"><br>
                </div>
            </form>
            
        </div>
    </div>
</body>