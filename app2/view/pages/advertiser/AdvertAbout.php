
<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 ?>

<!DOCTYPE html>

<head>
    <title>About Us - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertAbout.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h1>About Us</h1>
            </div>
                <div id="paymentBlock1" class="paymentBlocks">
                    <h3>Ceylon GIG</h3><br><br>
                    <p>CeylonGig is established with a motive of creating a web-based platform for all self-employed workers, also known as freelancers, to deliver their service efficiently to the customers who are seeking them.</p><br><br><br>
                    <h3>Ceylon GIG - Advertiser </h3><br><br>
                    <p>Ceylon GIG advertiser portal gives the opprunity to Organisation like your to join with us and advertise your products. It is a platform with all legal and ethical constraints regarding Advertising. Make your brand by advertising with us.</p><br><br><br>

                </div>
        </div>
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>

        
</body>

