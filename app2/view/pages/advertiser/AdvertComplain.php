<?php
 include('../../../model/Advertiser/connection.php');
 include('../../../model/Advertiser/Session.php');
 include('../../../model/Advertiser/AdvertComplain.php');

 ?>

<!DOCTYPE html>

<head>
    <title>Complain - Advertiser</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/ceylongig/app/view/assets/img/icon_circle.png" type="image/png">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertComplain.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertNavbar.css">
    <link rel="stylesheet" href="../../assets/css/advert/AdvertFooter.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        
</head>

<body>
    <div id="container" >
        <nav>
            <?php include('AdvertNav.php') ?>
        </nav>
        <div id="content">
            <div id="pgeHeading">
                <h2>What Went Wrong? Please feel free to Share with Us</h2>
            </div>
                <form name="newCompalint" action="" method="POST">
                    <div id="contentBlock1" class="contentBlocks">
                        <h2>Complaint Details</h2><br><br>
                        <label for="cmpAbout"><b>Complaint title:&nbsp&nbsp</b></label><br>
                        <input name='cmpAbout' id='cmpAbout' class="field"><br><br>
                        <label for="cmpContent"><b>Complaint Description:&nbsp&nbsp</b></label><br>
                        <textarea type="text" name='cmpContent' id='cmpContent' class="field" cols="20" rows="10" ></textarea><br><br>
            
                        <p style="font-size: small;">Please note that your complaint will be submited from your current account and our staff memebrs will get in touch with you via E-Mail or Phone</p><br><br>
                        <div style="text-align:center;"> 
                            <input type="submit" value="Submit" id="submitBtn" name="submitBtn">
                        </div> 
                    </div>
                </form>
                
        </div>
    </div>
        <footer>
            <?php include('AdvertFooter.php');?>
        </footer>

        
</body>

